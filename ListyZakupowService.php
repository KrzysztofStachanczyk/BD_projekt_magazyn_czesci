<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 02.01.17
 * Time: 21:41
 */

require_once "DBConnector.php";

class ListyZakupowService{

    /**
     * Pobiera szczegóły wybranej listy zakupów
     * @param $id_uzytkownika
     * @param $nazwa_listy
     * @return array|null - lista o indeskach identyfikator_listy_zakupow,nazwa_listy_zakupow,czas_utworzenia,termin_realizacji
     * @throws Exception - błąd komunikatji z BD (KOD 200)
     */
    public static function pobierzListeZakupow($id_uzytkownika,$nazwa_listy){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenia,"select identyfikator_listy_zakupow,nazwa_listy_zakupow,czas_utworzenia,termin_realizacji from listy_zakupow WHERE id_uzytkownika=$1 AND nazwa_listy_zakupow=$2",array($id_uzytkownika,$nazwa_listy));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $listaZakupow=null;

        if ($row=pg_fetch_row($wynik_zapytania)){
            $listaZakupow=array("identyfikator_listy_zakupow"=> $row[0], "nazwa_listy_zakupow" => $row[1],"czas_utworzenia" => $row[2],"termin_realizacji" => $row[3]);
        }

        DBConnector::closeConnection();
        return $listaZakupow;
    }


    /**
     * Pobiera listy zakupów użytkownika
     * @param $id_uzytkownika
     * @return array - lista zawierająca wiersze o indeksach "identyfikator_listy_zakupow" "nazwa_listy_zakupow" "czas_utworzenia" "termin_realizacji"
     * @throws Exception -błąd komunikacji z BD (KOD 200)
     */
    public static  function  pobierzListyZakupowUzytkownika($id_uzytkownika){
        $polaczenie=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenie,"select identyfikator_listy_zakupow,nazwa_listy_zakupow,czas_utworzenia,termin_realizacji from listy_zakupow WHERE id_uzytkownika=$1",array($id_uzytkownika));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $listyZakupow=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $listyZakupow[]=array(
                "identyfikator_listy_zakupow" => $row[0],
                "nazwa_listy_zakupow" => $row[1],
                "czas_utworzenia" => $row[2],
                "termin_realizacji" => $row[3]);
        }

        DBConnector::closeConnection();
        return $listyZakupow;
    }

    /**
     * Dodaje listę zakupów o podanej nazwie
     * @param $id_uzytkownika
     * @param $nazwa_listy_zakupow
     * @throws Exception - błąd pochodzący z bazy danych (KOD 202)
     */
    public static function dodajListeZakupow($id_uzytkownika,$nazwa_listy_zakupow){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_liste_zakupow($1, $2);',array($id_uzytkownika,$nazwa_listy_zakupow));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa liste zakupow o podanym ID
     * @param $id_uzytkownika
     * @param $id_listy_zakupow
     * @throws Exception - błąd pochodzący z bazy danych (KOD 202)
     */
    public static function usunListeZakupow($id_uzytkownika,$id_listy_zakupow){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select usun_liste_zakupow($1, $2);',array($id_uzytkownika,$id_listy_zakupow));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";

            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Pobiera koszt wskazanej listy zakupów użytkownika
     * @param $id_uzytkownika
     * @param $lista_zakupow_id
     * @return
     * @throws
     */
    public static function pobierzKosztZakupu($id_uzytkownika, $lista_zakupow_id){
        $connection=DBConnector::connect();
        $resultSet=pg_query_params($connection,"select pobierzKosztZakupu($1,$2);",array($id_uzytkownika,$lista_zakupow_id));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $koszt=null;

        if ($row=pg_fetch_row($resultSet)){
            $koszt=$row[0];
        }

        DBConnector::closeConnection();
        return $koszt;
    }


    /**
     * Wprowadza listę zakupów do magazynu
     * @param $id_uzytkownika
     * @param $id_listy_zakupow
     * @throws Exception
     */
    public static function wprowadzListeZakupow($id_uzytkownika,$id_listy_zakupow){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select akceptuj_liste_zakupow($1, $2);',array($id_uzytkownika,$id_listy_zakupow));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            $exceptionMessage= pg_result_error($result);
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Tworzy listę zakupów na podstawie schemau
     * @param $id_uzytkownika
     * @param $id_schematu
     * @param $nazwa_listy
     * @throws Exception błędy pochodzące z bazy danych
     */
    public static function utworzListeZakupowNaPodstawieSchematu($id_uzytkownika,$id_schematu,$nazwa_listy){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select utworz_liste_zakupow_na_podstawie_schematu($1, $2,$3);',array($id_uzytkownika,$id_schematu,$nazwa_listy));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            $exceptionMessage= pg_result_error($result);
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }


}