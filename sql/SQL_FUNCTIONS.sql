
----------------------------------------------------------------------------------------------------
--
--                              funkcje wewnetrzne
--
----------------------------------------------------------------------------------------------------

-- sprawdza czy uzytkownik o zadanym ID posiada uprawnienie o nazwie "nazwa_uprawnienia_attr"
-- zwraca True/False
CREATE OR REPLACE FUNCTION sprawdz_uprawnienie(ident_uzytkownika integer, nazwa_uprawnienia_attr varchar(100)) RETURNS boolean AS $$
    DECLARE
        ilosc_zgodnych integer;

    BEGIN
        SELECT COUNT(*) FROM view_uzytkownicy_uprawnienia where id_uzytkownika=ident_uzytkownika AND nazwa_uprawnienia=nazwa_uprawnienia_attr  INTO ilosc_zgodnych;

        IF ilosc_zgodnych=1 THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------
--
--                              dokumentacje service 
--
----------------------------------------------------------------------------------------------------

-- dodaje nowa dokumentacje do czesci o podanej nazwie, rzuca wyjatki jesli
-- - czesc nie istnieje
-- - nazwa dokumentacji zawiera niedozwolone znaki
-- - nazwa dokumentacji ma zla dlugosc
-- - brak uprawnien do zmian w dokumentacjach
CREATE OR REPLACE FUNCTION dodaj_dokumentacje(id_uzytkownika integer, nazwa_czesci varchar(100),nazwa_nowej_dokumentacji varchar(50),link_do_dokumentacji varchar(250)) RETURNS VOID AS $$
    DECLARE
        id_elementu_dla_dokumentacji integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_DOKUMENTACJI') THEN
            IF length(nazwa_nowej_dokumentacji)<5 OR length(nazwa_nowej_dokumentacji)>50 THEN
                RAISE 'Nazwa dokumentacji powinna miec dlugosc (5-50) znakow';
            END IF;

            IF nazwa_nowej_dokumentacji  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
                RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
            END IF;

            SELECT identyfikator_czesci FROM czesci_elektroniczne  WHERE nazwa_elementu=nazwa_czesci INTO id_elementu_dla_dokumentacji;

            IF id_elementu_dla_dokumentacji is not null THEN
                INSERT INTO dokumentacje(identyfikator_czesci,nazwa_dokumentacji,link) VALUES (id_elementu_dla_dokumentacji,nazwa_nowej_dokumentacji,link_do_dokumentacji);
            ELSE
                RAISE 'Wskazana czesc nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;

        EXCEPTION 
            WHEN unique_violation THEN
                RAISE 'Dokumentacja o podanej nazwie już istnieje';
    END;
$$ LANGUAGE plpgsql;

-- usuwa dokumentacje o podanym id, rzuca wyjatki jesli
-- - brak uprawnien do zmian w dokumentacjach
-- - wskazana dokumentacja nie istnieje
CREATE OR REPLACE FUNCTION usun_dokumentacje(id_uzytkownika integer,id_dokumentacji integer) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_DOKUMENTACJI') THEN
            WITH rows AS(
                DELETE from dokumentacje WHERE identyfikator_dokumentacji=id_dokumentacji
                RETURNING 1
            )
            SELECT COUNT(*) FROM rows INTO ilosc_usunietych_wierszy;

            IF ilosc_usunietych_wierszy=0 THEN
                RAISE 'Dokumentacja o podanym ID nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------
--
--                              noty service 
--
----------------------------------------------------------------------------------------------------

-- dodaje note dokumentacje do czesci o podanej nazwie, rzuca wyjatki jesli
-- - czesc nie istnieje
-- - nazwa noty zawiera niedozwolone znaki
-- - nazwa noty ma zla dlugosc
-- - brak uprawnien do zmian w notach aplikacyjnych
CREATE OR REPLACE FUNCTION dodaj_note(id_uzytkownika integer, nazwa_czesci varchar(100),nazwa_nowej_noty varchar(50),link_do_noty varchar(250)) RETURNS VOID AS $$
    DECLARE
        id_elementu_dla_noty integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_NOTY') THEN
            IF length(nazwa_nowej_noty)<10 OR length(nazwa_nowej_noty)>50 THEN
                RAISE 'Nazwa noty powinna miec dlugosc (10-50) znakow';
            END IF;

            IF nazwa_nowej_noty  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
                RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
            END IF;

            SELECT identyfikator_czesci FROM czesci_elektroniczne WHERE nazwa_elementu=nazwa_czesci INTO id_elementu_dla_noty ;

            IF id_elementu_dla_noty is not null THEN
                INSERT INTO noty_aplikacyjne(identyfikator_czesci,nazwa_noty,link) VALUES (id_elementu_dla_noty,nazwa_nowej_noty,link_do_noty);
            ELSE
                RAISE 'Wskazana czesc nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;

        EXCEPTION 
            WHEN unique_violation THEN
                RAISE 'Nota o podanej nazwie już istnieje';
    END;
$$ LANGUAGE plpgsql;

-- usuwa note o podanym id, rzuca wyjatki jesli
-- - brak uprawnien do zmian w notach
-- - wskazana nota nie istnieje
CREATE OR REPLACE FUNCTION usun_note(id_uzytkownika integer,id_noty integer) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_NOTY') THEN
            WITH rows AS(
                DELETE from noty_aplikacyjne WHERE identyfikator_noty=id_noty
                RETURNING 1
            )
            SELECT COUNT(*) FROM rows INTO ilosc_usunietych_wierszy;

            IF ilosc_usunietych_wierszy=0 THEN
                RAISE 'Nota o podanym ID nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              czesci magazyn service 
--
----------------------------------------------------------------------------------------------------

-- usuwa czesc z magazynu, rzuca wyjatki jesli:
-- - czesc o podanej nazwie nie istnieje 
-- - w magazynie nie ma juz takiej czesci
CREATE OR REPLACE FUNCTION usun_czesc_z_magazynu(id_uzytkownika_attr integer,nazwa_czesci_do_usuniecia varchar(100)) RETURNS VOID AS $$
    DECLARE
        id_czesci_do_usuniecia integer;
        ilosc_usunietych_wierszy integer;
    BEGIN
      
            SELECT identyfikator_czesci INTO id_czesci_do_usuniecia FROM czesci_elektroniczne WHERE nazwa_elementu=nazwa_czesci_do_usuniecia;

            IF id_czesci_do_usuniecia is not null THEN
                WITH rows AS(
                DELETE FROM czesci_elektroniczne_magazyn WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_czesci=id_czesci_do_usuniecia
                RETURNING 1
                ) 
                SELECT COUNT(*) FROM rows INTO ilosc_usunietych_wierszy;

                IF ilosc_usunietych_wierszy=0 THEN
                    RAISE 'Tej części już nie ma w magazynie';
                END IF;

            ELSE
                RAISE 'Czesc o podanej nazwie nie istnieje';
            END IF;
    END;
$$ LANGUAGE plpgsql;


-- dodaje czesc do magazynu , rzuca wyjatki jesli:
-- - uzytkownik nie ma uprawnien do dodawania czesci do swojego magazynu
-- - czesc jest zablokowana
-- - czesc nie istnieje
-- - czesc jest juz w magazynie
CREATE OR REPLACE FUNCTION dodaj_czesc_do_magazynu(id_uzytkownika_attr integer,nazwa_przypisywanej_czesci varchar(100)) RETURNS VOID AS $$
    DECLARE
        id_przypisanej_czesci integer;
        czesc_zablokowana boolean;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika_attr,'PRZYPISYWANIE_CZESCI_DO_MAGAZYNU') THEN
            SELECT identyfikator_czesci INTO id_przypisanej_czesci FROM czesci_elektroniczne WHERE nazwa_elementu=nazwa_przypisywanej_czesci;
            SELECT zablokowana INTO czesc_zablokowana FROM czesci_elektroniczne WHERE identyfikator_czesci = id_przypisanej_czesci;

            IF  czesc_zablokowana THEN
                RAISE 'Czesc jest zablokowana i nie moze byc dodawana do magazynow';
            END IF;

            IF id_przypisanej_czesci is not null THEN
                INSERT INTO czesci_elektroniczne_magazyn(identyfikator_czesci,id_uzytkownika,ilosc_sztuk) VALUES (id_przypisanej_czesci,id_uzytkownika_attr,0);
            ELSE
                RAISE 'Czesc o podanej nazwie nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;

    EXCEPTION
        WHEN unique_violation THEN
            RAISE 'Wskazana cześć jest już w twoim magazynie';

    END;
$$ LANGUAGE plpgsql;

-- usuwa zadana czesc z magazynu rzuca wyjatki jesli:
-- - czesc nie istnieje 
CREATE OR REPLACE FUNCTION usun_czesc_z_magazynu_by_id(id_uzytkownika_attr integer,id_czesci_attr integer) RETURNS VOID AS $$
    DECLARE 
        ilosc_usunietych_wierszy integer;
    BEGIN
        WITH rows AS (
            DELETE FROM czesci_elektroniczne_magazyn WHERE id_uzytkownika = id_uzytkownika_attr AND identyfikator_czesci = id_czesci_attr 
            RETURNING 1 
            )
        SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

        IF ilosc_usunietych_wierszy=0 THEN 
            RAISE 'Wskazana czesc nie znajduje sie w magazynie';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- aktualizuje ilosc czesci w magazynie rzuca wyjatek jesli:
-- - podana ilosc < 0 
-- - nie znaleziono takiej czesci
CREATE OR REPLACE FUNCTION zmien_ilosc_czesci(id_uzytkownika_attr integer, id_czesci_attr integer, nowa_ilosc integer) RETURNS VOID AS $$
    DECLARE
        ilosc_zmian integer;
    BEGIN
        IF nowa_ilosc<0 THEN
            RAISE 'Ilosc czesci nie moze byc ujemna';
        ELSE
            WITH rows AS (
            UPDATE czesci_elektroniczne_magazyn SET ilosc_sztuk=nowa_ilosc WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_czesci = id_czesci_attr
            RETURNING 1
            ) 
            SELECT COUNT(*) INTO ilosc_zmian FROM rows;

            IF ilosc_zmian = 0 THEN 
                RAISE 'Wskazana czesc nie istnieje';
            END IF;
        END IF;

    END ;

$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              czesci service 
--
----------------------------------------------------------------------------------------------------

-- usuwa czesc o podanej nazwie , rzuca wyjatki jesli:
-- - brak uprawnien do usuwania czesci
-- - czesc znajduje sie w magazynach
-- - czesc nie istnieje
CREATE OR REPLACE FUNCTION usun_czesc(id_uzytkownika integer,nazwa_czesci_do_usuniecia varchar(100)) RETURNS VOID AS $$
    DECLARE 
        ilosc_usunietych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'USUWANIE_CZESCI') THEN

            WITH rows AS(
                DELETE FROM czesci_elektroniczne WHERE nazwa_elementu=nazwa_czesci_do_usuniecia
                RETURNING 1
            )
            SELECT COUNT(*) FROM rows INTO ilosc_usunietych_wierszy;

            IF ilosc_usunietych_wierszy=0 THEN
                RAISE 'Czesc o podanej nazwie nie istnieje';    
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    EXCEPTION
        WHEN  foreign_key_violation THEN
            RAISE 'Wskazana część znajduje się w magazynach';
    END;
$$ LANGUAGE plpgsql;

-- blokuje czesc przez co nie mozna jej dodac do nowych magazynow, rzuca wyjatki jesli:
-- - brak uprawnien do modyfikacji czesci
-- - czesc nie istnieje
CREATE OR REPLACE FUNCTION zablokuj_czesc(id_uzytkownika integer,nazwa_czesci_do_zablokowania varchar(100)) RETURNS VOID AS $$
    DECLARE
        ilosc_zmodyfikowanych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_CZESCI') THEN

            WITH rows AS(
                UPDATE czesci_elektroniczne SET zablokowana=TRUE WHERE nazwa_elementu = nazwa_czesci_do_zablokowania
                RETURNING 1
            )
            SELECT COUNT(*) FROM rows INTO ilosc_zmodyfikowanych_wierszy;
            
            IF ilosc_zmodyfikowanych_wierszy=0 THEN
                RAISE 'Czesc o podanej nazwie nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;


-- odblokowuje czesc, rzuca wyjatki jesli:
-- - brak uprawnien do modyfikacji czesci
-- - czesc nie istnieje
CREATE OR REPLACE FUNCTION odblokuj_czesc(id_uzytkownika integer,nazwa_czesci_do_odblokowania varchar(100)) RETURNS VOID AS $$
    DECLARE
        ilosc_zmodyfikowanych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'MODYFIKACJA_CZESCI') THEN

            WITH rows AS(
                UPDATE czesci_elektroniczne SET zablokowana=FALSE WHERE nazwa_elementu = nazwa_czesci_do_odblokowania
                RETURNING 1
            )
            SELECT COUNT(*) FROM rows INTO ilosc_zmodyfikowanych_wierszy;
            
            IF ilosc_zmodyfikowanych_wierszy=0 THEN
                RAISE 'Czesc o podanej nazwie nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- dodaje nowa czesc do magazynu , rzuca wyjatki jesli:
-- - brak uprawnien do dodawania czesci
-- - kategoria o podanej nazwie nie istnieje
-- - dlugosc nazwy nie jest w dozwolonym zakresie
-- - nazwa ma zly format
-- - czesc o podanej nazwie juz istnieje
CREATE OR REPLACE FUNCTION dodaj_czesz(id_uzytkownika integer, nazwa_nowej_czesci varchar(100),opis_nowej_czesci varchar(500),nazwa_przypisanej_kategori varchar(50)) RETURNS VOID AS $$
    DECLARE
        id_przypisanej_kategori integer;
    BEGIN
        IF length(nazwa_nowej_czesci)<5 or length(nazwa_nowej_czesci)>100 THEN
            RAISE 'Bledna dlugosc nazwy czesci (dozwolony zakres 5-100)';
        END IF;

        IF nazwa_nowej_czesci  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF;

        IF sprawdz_uprawnienie(id_uzytkownika,'DODAWANIE_CZESCI') THEN
            SELECT id_kategori FROM kategorie_czesci  WHERE nazwa_kategori=nazwa_przypisanej_kategori INTO id_przypisanej_kategori;

            IF id_przypisanej_kategori is not null THEN
                INSERT INTO czesci_elektroniczne(id_kategori,nazwa_elementu,opis,zablokowana) VALUES (id_przypisanej_kategori,nazwa_nowej_czesci,opis_nowej_czesci,false);
            ELSE
                RAISE 'Nazwa kategorii nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;

    EXCEPTION 
        WHEN unique_violation THEN
            RAISE 'Cześć o podanej nazwie już istnieje';
    END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              kategorie service 
--
----------------------------------------------------------------------------------------------------

-- usuwa wskazana kategorie rzuca wyjatki jesli:
--  - kategoria nie istnieje
--  - brak uprawnien
--  - do kategorii przypisane sa jakies czesci
CREATE OR REPLACE FUNCTION usun_kategorie(id_uzytkownika integer, nazwa_kategori_do_usuniecia varchar(50)) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_KATEGORI') THEN
            WITH rows AS(
            DELETE FROM kategorie_czesci WHERE nazwa_kategori=nazwa_kategori_do_usuniecia
            RETURNING 1
            ) 
            SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

            IF ilosc_usunietych_wierszy=0 THEN
                RAISE 'Wskazana kategoria nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;

    EXCEPTION
        WHEN  foreign_key_violation THEN
            RAISE 'Wskazana kategoria ma przypisane przedmioty';
    END;
$$ LANGUAGE plpgsql;



-- zmienia nazwe kategorii , rzuca wyjatki jesli:
-- - uzytkownik nie ma uprawnien do edycji kategorii
-- - kategoria nie istnieje
-- - nowa nazwa kategorii ma zla dlugosc 
-- - nowa nazwa nie jest zgodna z formatem
CREATE OR REPLACE FUNCTION zmien_nazwe_kategori(id_uzytkownika integer, stara_nazwa_kategori varchar(50),nowa_nazwa_kategori varchar(50)) RETURNS VOID AS $$
    DECLARE
        id_zmienianej_kategori integer;
    BEGIN
        IF length(nowa_nazwa_kategori)<5 or length(nowa_nazwa_kategori)>50 THEN
            RAISE 'Bledna dlugosc nazwy kategorii (dozwolony zakres 5-50)';
        END IF;

        IF nowa_nazwa_kategori  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF;

        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_KATEGORI') THEN
            SELECT id_kategori FROM kategorie_czesci  WHERE nazwa_kategori=stara_nazwa_kategori INTO id_zmienianej_kategori;

            IF id_zmienianej_kategori is not null THEN
                update kategorie_czesci set nazwa_kategori = nowa_nazwa_kategori where id_kategori = id_zmienianej_kategori;
            ELSE
                RAISE 'Kategoria nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- dodaje nowa kategorie , rzuca wyjatki jesli:
-- - uzytkownik nie ma uprawnien do edycji kategorii
-- - kategoria o tej nazwie juz istnieje
-- - nazwa kategorii ma zla dlugosc 
-- - nazwa nie jest zgodna z formatem
CREATE OR REPLACE FUNCTION dodaj_kategorie(id_uzytkownika integer, nazwa_nowej_kategori varchar(50)) RETURNS VOID AS $$
    BEGIN
        IF length(nazwa_nowej_kategori)<5 or length(nazwa_nowej_kategori)>50 THEN
            RAISE 'Bledna dlugosc nazwy kategorii (dozwolony zakres 5-50)';
        END IF;

        IF nazwa_nowej_kategori  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF;

        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_KATEGORI') THEN
            INSERT INTO kategorie_czesci(nazwa_kategori) VALUES (nazwa_nowej_kategori);
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    EXCEPTION
        WHEN unique_violation THEN
            RAISE  'Kategoria o podanej nazwie istnieje';
    END;
$$ LANGUAGE plpgsql;



----------------------------------------------------------------------------------------------------
--
--                              uprawnienia service 
--
----------------------------------------------------------------------------------------------------


-- usuwa wskazane uprawnienie z grupy, rzuca wyjatki jesli:
-- - uzytkownik nie ma uprawnien do edycji grup
-- - grupa o wskazanej nazwie nie istnieje
CREATE OR REPLACE FUNCTION usun_uprawnienie(id_uzytkownika integer, nazwa_grupy_attr varchar(50), nazwa_uprawnienia_attr varchar(50)) RETURNS VOID AS $$
    DECLARE
        id_grupy_val integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_GRUP_UPRAWNIEN') THEN
            select identyfikator_grupy from grupy_uprawnien   where nazwa_grupy=nazwa_grupy_attr into id_grupy_val;
            
            IF id_grupy_val is not null THEN
                delete from definicje_uprawnien where identyfikator_grupy=id_grupy_val  and nazwa_uprawnienia=nazwa_uprawnienia_attr;
            ELSE
                RAISE 'Wskazana grupa nie istnieje';
            END IF;

        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;


-- dodaje uprawnienie do grupy, rzuca wyjatki jesli:
-- - uprawnienie nie istnieje
-- - grupa o podanej nazwie nie istnieje
-- - uzytkownik nie ma uprawnien do edycji grup
CREATE OR REPLACE FUNCTION dodaj_uprawnienie(id_uzytkownika integer, nazwa_grupy_attr varchar(50), nazwa_uprawnienia_attr varchar(50)) RETURNS VOID AS $$
    DECLARE
        id_grupy_val integer;
        czy_uprawnienie_istnieje integer;
        czy_nazwa_uprawnienia_poprawnia integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_GRUP_UPRAWNIEN') THEN
            select identyfikator_grupy from grupy_uprawnien  where nazwa_grupy=nazwa_grupy_attr into id_grupy_val ;
            select count(*) from uprawnienia   where nazwa_uprawnienia=nazwa_uprawnienia_attr into czy_nazwa_uprawnienia_poprawnia ;

            IF czy_nazwa_uprawnienia_poprawnia=0 THEN
                RAISE 'Wskazane uprawnienie nie istnieje';
            END IF;

            IF id_grupy_val is not null THEN
                select count(*) from definicje_uprawnien where identyfikator_grupy=id_grupy_val and nazwa_uprawnienia=nazwa_uprawnienia_attr into czy_uprawnienie_istnieje;

                IF czy_uprawnienie_istnieje=0 THEN
                    INSERT into definicje_uprawnien VALUES (nazwa_uprawnienia_attr,id_grupy_val);
                END IF;

            ELSE
                RAISE 'Wskazana grupa nie istnieje';
            END IF;

        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------
--
--                              grupy uprawnien service 
--
----------------------------------------------------------------------------------------------------


-- przypisuje uzytkownika do grupy, rzuca wyjatki jesli:
-- - grupa nie istnieje
-- - nie udalo sie przypisac uzytkownika do grupy
-- - uzytkownik nie ma uprawnien do modyfikacji uprawnien innych osob
CREATE OR REPLACE FUNCTION przypisz_uzytkownika_do_grupy(id_uzytkownika integer, login_attr varchar(20), nazwa_grupy_attr varchar(50)) RETURNS VOID AS $$
    DECLARE
        id_grupy integer;
        ilosc_zmodyfikowanych_wierszy integer;
    BEGIN

        IF sprawdz_uprawnienie(id_uzytkownika,'PRZYPISYWANIE_DO_UPRAWNIEN') THEN
            select identyfikator_grupy from grupy_uprawnien where nazwa_grupy=nazwa_grupy_attr into id_grupy ;

            IF id_grupy is not null THEN
                WITH rows AS(
                    update uzytkownicy set identyfikator_grupy = id_grupy WHERE login=login_attr
                    RETURNING 1
                )
                SELECT COUNT(*)  FROM rows INTO ilosc_zmodyfikowanych_wierszy;

                IF ilosc_zmodyfikowanych_wierszy=0 THEN
                    RAISE 'Nie udało się wykonać zmian';
                END IF; 
            ELSE
                RAISE 'Wskazana grupa nie istnieje';
            END IF;

        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- dodaje nowa grupe uprawnien rzuca wyjatki jesli:
-- - uzytkownik nie ma uprawnien do tworzenia grup
-- - dlugosc nazwy grupy poza dopuszczalnym zakresem 3-100 znakow
-- - grupa o podanej nazwie juz istnieje
-- - nazwa zawiera niedozwolone znaki
CREATE OR REPLACE FUNCTION dodaj_grupe_uprawnien(id_uzytkownika integer, nazwa_nowej_grupy varchar(50)) RETURNS VOID AS $$
    BEGIN
        IF length(nazwa_nowej_grupy)<3 or length(nazwa_nowej_grupy)>50 THEN
            RAISE 'Bledna dlugosc nazwy grupy (dozwolony zakres 3-50)';
        END IF;

        IF nazwa_nowej_grupy  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF; 

        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_GRUP_UPRAWNIEN') THEN
            INSERT INTO grupy_uprawnien(nazwa_grupy) VALUES(nazwa_nowej_grupy); 
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    EXCEPTION
        WHEN unique_violation THEN
            RAISE  'Grupa uprawnien o podanej nazwie istnieje';
    END;
$$ LANGUAGE plpgsql;

-- usuwa wskazana grupe uprawnien, rzuca wyjatki jesli:
-- - uzytkownika nie ma uprawnien do usuwania grup
-- - grupa o podanej nazwie nie istnieje
-- - grupa ma przypisanych uzytkownikow
CREATE OR REPLACE FUNCTION usun_grupe_uprawnien(id_uzytkownika integer, nazwa_grupy_do_usuniecia varchar(50)) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych_wierszy integer;
    BEGIN

        IF sprawdz_uprawnienie(id_uzytkownika,'EDYCJA_GRUP_UPRAWNIEN') THEN
            WITH rows AS (
                delete from grupy_uprawnien where nazwa_grupy=nazwa_grupy_do_usuniecia
                RETURNING 1
            )
            SELECT COUNT(*) INTO ilosc_usunietych_wierszy;

            IF ilosc_usunietych_wierszy=0 THEN
                RAISE 'Wskazana grupa nie istnieje';
            END IF;
        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    EXCEPTION
        WHEN  foreign_key_violation THEN
            RAISE 'Wskazana grupa ma przypisanych użytkowników';
    END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              uzytkownicy service 
--
----------------------------------------------------------------------------------------------------

-- tworzy nowego uzytkownika i zwraca jego id, rzuca wyjatek jesli
-- - dlugosc loginu poza przedzialem [3-50]
-- - domyslna grupa uprawnien nie istnieje
-- - jest juz uzytkownik o takim loginie
-- - login ma zly format 
CREATE OR REPLACE FUNCTION utworz_uzytkownika(nowy_login varchar(20),haslo_attr varchar(20)) RETURNS integer AS $$
    DECLARE
        group_id integer;
        id_nowego_uzytkownika integer;
    BEGIN
        IF length(nowy_login)<3 or length(nowy_login)>20 THEN
            RAISE 'Bledna dlugosc login-u';
        END IF;

        IF nowy_login  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*' THEN
            RAISE 'Błedny format loginu dopuszczalne są tylko litery (polski i angielski alfabet) i cyfry';
        END IF; 

        IF length(haslo_attr)<5 or length(haslo_attr)>20 THEN
            RAISE 'Błędna długosc hasła powinno zawierać od 5 do 20 znaków';
        END IF;

        IF haslo_attr  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*' THEN
            RAISE 'Błedny format hasła dopuszczalne są tylko litery (polski i angielski alfabet) i cyfry';
        END IF;

        SELECT identyfikator_grupy FROM grupy_uprawnien WHERE nazwa_grupy='domyslna' INTO group_id ;

        IF group_id is not null THEN
            INSERT INTO uzytkownicy(login,haslo,identyfikator_grupy) VALUES(nowy_login,MD5(haslo_attr),group_id);
            SELECT id_uzytkownika FROM uzytkownicy WHERE uzytkownicy.login = nowy_login INTO id_nowego_uzytkownika;
        ELSE
            RAISE 'Domyslna grupa nie istnieje - skontaktuj sie z administratorem';
        END IF;

        RETURN id_nowego_uzytkownika;

    EXCEPTION
        WHEN unique_violation THEN
            RAISE 'Uzytkownik o zadanym loginie jest juz w systemie';
    END;
$$ LANGUAGE plpgsql;


-- usuwa wskazanego uzytkownika (po sprawdzeniu czy osoba zadjaca tego ma uprawnienia) rzuca wyjatki jesli:
--  - uzytkownika do usuniecia nie istnieje
--  - brak uprawnien do wykonania operacji
CREATE OR REPLACE FUNCTION usun_uzytkownika(id_uzytkownika integer,login_uzytkownika_do_usuniecia varchar(20)) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych_wierszy integer;
    BEGIN
        IF sprawdz_uprawnienie(id_uzytkownika,'USUWANIE_UZYTKOWNIKOW') THEN
            WITH rows AS(
            delete from uzytkownicy where login=login_uzytkownika_do_usuniecia
            RETURNING 1
            )
            select count(*) from rows into ilosc_usunietych_wierszy;

            IF ilosc_usunietych_wierszy =0 THEN    
                RAISE 'Wskazany uzytkownik nie istnieje';
            END IF;

        ELSE
            RAISE 'Brak uprawnien';
        END IF;
    END;
$$ LANGUAGE plpgsql;


-- przeprowadza procedure logowania zwraca id_uzytkownika lub null 
CREATE OR REPLACE FUNCTION login(nazwa_uzytkownika varchar(20),haslo_attr varchar(20)) RETURNS integer AS $$
    DECLARE
        id_zalogowanego_uzytkownika integer;
    BEGIN
        SELECT id_uzytkownika FROM uzytkownicy  WHERE login=nazwa_uzytkownika AND haslo=MD5(haslo_attr) INTO id_zalogowanego_uzytkownika;
        RETURN id_zalogowanego_uzytkownika ;
    END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              tag service 
--
----------------------------------------------------------------------------------------------------

-- usuwa tag o zadanych parametrach jesli nie istnieje rzucany jest wyjatek
CREATE OR REPLACE FUNCTION usun_tag(id_uzytkownika_attr integer, id_czesci_attr integer, nazwa_tagu_attr varchar(20)) RETURNS VOID AS $$
    DECLARE
        ilosc_usunietych integer;
        ilosc_przypisan_do_tagu integer;
    BEGIN
        WITH rows AS (
                DELETE FROM przypisanie_tagu WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_czesci=id_czesci_attr AND nazwa_tagu = nazwa_tagu_attr
                RETURNING 1
            ) 
        SELECT COUNT(*) INTO ilosc_usunietych FROM rows;

        IF ilosc_usunietych=0 THEN
            RAISE 'Tag nie istnieje';
        END IF;

        SELECT COUNT(*) INTO ilosc_przypisan_do_tagu FROM przypisanie_tagu WHERE nazwa_tagu=nazwa_tagu_attr AND id_uzytkownika=id_uzytkownika_attr;

        IF ilosc_przypisan_do_tagu=0 THEN
            DELETE FROM tagi WHERE id_uzytkownika=id_uzytkownika_attr AND nazwa_tagu=nazwa_tagu_attr;
        END IF;
    END;
$$ LANGUAGE plpgsql;


-- dodaje nowy tag rzuca wyjatek jesli:
-- - dlugosc tagu nie jest w zakresie [3,250] 
-- - tag juz  istnieje
CREATE OR REPLACE FUNCTION dodaj_tag(id_uzytkownika_attr integer, id_czesci_attr integer, nazwa_tagu_attr varchar(20)) RETURNS VOID AS $$
    DECLARE
        czy_tag_istnieje INTEGER;
    BEGIN

        IF length(nazwa_tagu_attr)<3 OR length(nazwa_tagu_attr)>20 THEN
            RAISE 'Nazwa tagu ma zla dlugosc (wymagane 3-20 znakow)';
        END IF;

        SELECT count(*)  INTO czy_tag_istnieje FROM tagi  WHERE id_uzytkownika=id_uzytkownika_attr AND nazwa_tagu=nazwa_tagu_attr;
        IF czy_tag_istnieje=0 THEN
            INSERT INTO tagi(nazwa_tagu,id_uzytkownika) VALUES (nazwa_tagu_attr,id_uzytkownika_attr);
        END IF;

        IF nazwa_tagu_attr  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF; 
            
        INSERT INTO przypisanie_tagu(id_uzytkownika,identyfikator_czesci,nazwa_tagu) VALUES(id_uzytkownika_attr,id_czesci_attr,nazwa_tagu_attr);

    EXCEPTION
        WHEN unique_violation THEN
            RAISE 'Podany tag juz istnieje';
    END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              listy zakupow service 
--
----------------------------------------------------------------------------------------------------



-- wprowadza listę zakupów do magazynu
-- - rzuca wyjatej jesli lista nie istnieje
CREATE OR REPLACE FUNCTION akceptuj_liste_zakupow(id_uzytkownika_attr INTEGER,id_listy_zakupow_attr INTEGER) RETURNS VOID AS $$
DECLARE
    czy_lista_istnieje INTEGER;
    wiersz RECORD;
BEGIN
    SELECT COUNT(*) INTO czy_lista_istnieje FROM listy_zakupow WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_listy_zakupow=id_listy_zakupow_attr;

    IF czy_lista_istnieje=0 THEN
        RAISE 'Wskazana lista nie istnieje';
    END IF;

    FOR wiersz IN
    SELECT identyfikator_czesci,ilosc_do_zamowienia FROM definicje_elementow_listy_zakupow WHERE identyfikator_listy_zakupow=id_listy_zakupow_attr

    LOOP
        UPDATE czesci_elektroniczne_magazyn SET ilosc_sztuk=ilosc_sztuk+wiersz.ilosc_do_zamowienia WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_czesci=wiersz.identyfikator_czesci;
    END LOOP;

    DELETE FROM listy_zakupow WHERE identyfikator_listy_zakupow=id_listy_zakupow_attr AND id_uzytkownika=id_uzytkownika_attr;

END
$$ LANGUAGE plpgsql;


-- dodaje nową liste zakupow rzuca wyjatek jesli:
-- - dlugosc nazwy nie jest w zakresie [5,250]
-- - nazwa zawiera niedozwolone znaki
-- - lista o podanej nazwie juz istnieje
CREATE OR REPLACE FUNCTION dodaj_liste_zakupow(id_uzytkownika_attr integer, nazwa_listy_attr varchar(50)) RETURNS VOID AS $$
    BEGIN
        IF length(nazwa_listy_attr)<5 OR length(nazwa_listy_attr)>50 THEN
            RAISE 'Nazwa listy zakupow powinna miec dlugosc 5-50 znakow';
        END IF;

        IF nazwa_listy_attr  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF; 

        INSERT INTO listy_zakupow(nazwa_listy_zakupow,id_uzytkownika,czas_utworzenia) VALUES (nazwa_listy_attr,id_uzytkownika_attr,NOW());

    EXCEPTION
        WHEN unique_violation THEN
            RAISE 'Lista o podanej nazwie juz istnieje';
    END;
$$ LANGUAGE plpgsql;


-- usuwa liste zakupow z magazynu rzuca wyjatki jesli:
-- - lista zakupow nie istnieje
CREATE OR REPLACE FUNCTION usun_liste_zakupow(id_uzytkownika_attr integer,id_listy_zakupow_attr integer) RETURNS VOID AS $$
    DECLARE 
        ilosc_usunietych_wierszy integer;
    BEGIN
        WITH rows AS (
            DELETE FROM listy_zakupow WHERE id_uzytkownika = id_uzytkownika_attr AND identyfikator_listy_zakupow = id_listy_zakupow_attr 
            RETURNING 1 
            )
        SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

        IF ilosc_usunietych_wierszy=0 THEN 
            RAISE 'Wskazana lista zakupow nie istnieje';
        END IF;
    END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION utworz_liste_zakupow_na_podstawie_schematu(id_uzytkownika_attr INTEGER, id_schematu_attr INTEGER,nazwa_nowej_listy VARCHAR(50)) RETURNS VOID AS $$
DECLARE
    nazwa_schematu_val VARCHAR(100);
    czy_lista_zakupow_juz_wygenerowana INTEGER;
    element_schematu RECORD;
    id_listy_zakupow INTEGER;
BEGIN
    SELECT nazwa_schematu INTO nazwa_schematu_val FROM schematy where identyfikator_schematu=id_schematu_attr;

    IF nazwa_schematu_val is NULL THEN
        RAISE 'Wskazany schemat nie istnieje';
    END IF;


    SELECT COUNT(*) INTO czy_lista_zakupow_juz_wygenerowana FROM listy_zakupow WHERE nazwa_listy_zakupow=nazwa_schematu_val AND id_uzytkownika=id_uzytkownika_attr;

    IF czy_lista_zakupow_juz_wygenerowana!=0 THEN
        RAISE 'Lista zakupów na podstawie tego schematu już istnieje';
    END IF;

    FOR element_schematu IN
    SELECT identyfikator_czesci,wymagana_ilosc FROM przypisanie_czesci_do_schematu WHERE identyfikator_schematu=id_schematu_attr
    LOOP
        BEGIN
            INSERT into czesci_elektroniczne_magazyn(identyfikator_czesci, id_uzytkownika, ilosc_sztuk) VALUES(element_schematu.identyfikator_czesci,id_uzytkownika_attr,0);
            EXCEPTION WHEN UNIQUE_VIOLATION THEN
            -- do nothing part exist :D
        END;
    END LOOP;

    PERFORM dodaj_liste_zakupow(id_uzytkownika_attr,nazwa_nowej_listy);

    SELECT identyfikator_listy_zakupow INTO id_listy_zakupow FROM listy_zakupow  WHERE  nazwa_listy_zakupow=nazwa_nowej_listy AND id_uzytkownika=id_uzytkownika_attr;

    IF id_listy_zakupow is NULL THEN
        RAISE 'Tworzenie listy zakupów nie powiodło się';
    END IF;

    FOR element_schematu IN
    SELECT identyfikator_czesci,wymagana_ilosc FROM przypisanie_czesci_do_schematu WHERE identyfikator_schematu=id_schematu_attr

    LOOP
        INSERT INTO definicje_elementow_listy_zakupow(identyfikator_listy_zakupow, identyfikator_czesci, id_uzytkownika, ilosc_do_zamowienia, koszt,czy_koszt_jednostkowy) VALUES(id_listy_zakupow,element_schematu.identyfikator_czesci,id_uzytkownika_attr,element_schematu.wymagana_ilosc,0,FALSE);
    END LOOP;
END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------
--
--                              listy zakupow definicje service 
--
----------------------------------------------------------------------------------------------------


-- modyfikuje element  listy zakupow rzuca wyjatki jesli:
-- - koszt < 0
-- - ilosc do zamowienia <=0
-- - wskazany element nie istnieje
CREATE OR REPLACE FUNCTION zmien_na_liscie_zakupow(id_uzytkownika_attr integer,id_listy_zakupow_attr integer,id_czesci_attr integer, ilosc_do_zamowienia_attr integer, koszt_attr decimal, czy_koszt_jednostkowy_attr boolean) RETURNS VOID AS $$
    DECLARE 
        ilosc_zmodyfikowanych_wierszy integer;
    BEGIN
        IF koszt_attr<0 THEN
            RAISE 'Koszt nie moze byc < 0 ';
        END IF;

        IF ilosc_do_zamowienia_attr<= 0 THEN
            RAISE 'Ilosc czesci do zamowienia nie moze byc <= 0 ';
        END IF;

        WITH rows AS (
        UPDATE definicje_elementow_listy_zakupow SET ilosc_do_zamowienia=ilosc_do_zamowienia_attr, koszt=koszt_attr, czy_koszt_jednostkowy=czy_koszt_jednostkowy_attr WHERE  id_uzytkownika = id_uzytkownika_attr AND identyfikator_listy_zakupow = id_listy_zakupow_attr AND identyfikator_czesci=id_czesci_attr
        RETURNING 1
        )
        SELECT COUNT(*) INTO ilosc_zmodyfikowanych_wierszy FROM rows;

        IF ilosc_zmodyfikowanych_wierszy=0 THEN 
            RAISE 'Wskazany element nie istnieje na liscie zakupow';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- usuwa element z listy zakupow  rzuca wyjatki jesli:
-- - zazadano usuniecia nieistniejacego elementu
 CREATE OR REPLACE FUNCTION usun_element_z_listy_zakupow(id_uzytkownika_attr integer,id_listy_zakupow_attr integer,id_czesci_attr integer) RETURNS VOID AS $$
    DECLARE 
        ilosc_usunietych_wierszy integer;
    BEGIN
        WITH rows AS (
            DELETE FROM definicje_elementow_listy_zakupow WHERE id_uzytkownika = id_uzytkownika_attr AND identyfikator_listy_zakupow = id_listy_zakupow_attr AND identyfikator_czesci=id_czesci_attr
            RETURNING 1 
            )
        SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

        IF ilosc_usunietych_wierszy=0 THEN 
            RAISE 'Wskazany element nie znajduje sie na liscie zakupow';
        END IF;
    END;
$$ LANGUAGE plpgsql;

-- dodaje element do listy zakupow rzuca wyjatki jesli:
-- - koszt < 0
-- - ilosc do zamowienia <=0
-- - zadana czesc jest juz na tej liscie zakupow
CREATE OR REPLACE FUNCTION dodaj_do_listy_zakupow(id_uzytkownika_attr integer,id_listy_zakupow_attr integer,id_czesci_attr integer, ilosc_do_zamowienia_attr integer, koszt_attr decimal, czy_koszt_jednostkowy_attr boolean) RETURNS VOID AS $$
    BEGIN
        IF koszt_attr<0 THEN
            RAISE 'Koszt nie moze byc < 0 ';
        END IF;

        IF ilosc_do_zamowienia_attr<= 0 THEN
            RAISE 'Ilosc czesci do zamowienia nie moze byc <= 0 ';
        END IF;

        INSERT INTO definicje_elementow_listy_zakupow(id_uzytkownika,identyfikator_listy_zakupow,identyfikator_czesci,ilosc_do_zamowienia,koszt,czy_koszt_jednostkowy) 
        VALUES (id_uzytkownika_attr,id_listy_zakupow_attr,id_czesci_attr,ilosc_do_zamowienia_attr,koszt_attr,czy_koszt_jednostkowy_attr);
    EXCEPTION
        WHEN unique_violation THEN
            RAISE 'Element zostal juz dodany do tej listy zakupow';
    END;
$$ LANGUAGE plpgsql;


-- pobierz sumaryczny koszt elementów na liście zakupów
-- - jeżeli lista nie należy do użytkownika zwraca 0
CREATE OR REPLACE FUNCTION pobierzKosztZakupu(id_uzytkownika_attr INTEGER, lista_zakupow_id INTEGER) RETURNS DECIMAL AS $$
DECLARE
    lista_nalezy_do_usera INTEGER;
    koszt_jednostkowych DECIMAL;
    koszt_pozostalych DECIMAL;
BEGIN
    SELECT COUNT(*) FROM listy_zakupow WHERE identyfikator_listy_zakupow=lista_zakupow_id AND id_uzytkownika=id_uzytkownika_attr INTO lista_nalezy_do_usera;

    IF lista_nalezy_do_usera!=1 THEN
        RETURN 0;
    END IF;

    SELECT SUM(koszt*ilosc_do_zamowienia) FROM definicje_elementow_listy_zakupow WHERE identyfikator_listy_zakupow=lista_zakupow_id AND czy_koszt_jednostkowy=TRUE INTO koszt_jednostkowych;
    SELECT SUM(koszt) FROM definicje_elementow_listy_zakupow WHERE identyfikator_listy_zakupow=lista_zakupow_id AND czy_koszt_jednostkowy=FALSE INTO koszt_pozostalych;

    IF koszt_pozostalych IS NULL THEN
        koszt_pozostalych=0;
    END IF;

    IF koszt_jednostkowych IS NULL THEN
        koszt_jednostkowych=0;
    END IF;

    RETURN koszt_jednostkowych+koszt_pozostalych;
END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------
--
--                               schematy service
--
----------------------------------------------------------------------------------------------------




-- dodaje nowy schemat rzuca wyjątek jeśli:
-- - schemat nie istnieje
-- - nazwa  zawiera niedozwolone znaki
-- - nazwa  ma zla dlugosc
-- - brak uprawnien do tworzenia schematow
CREATE OR REPLACE FUNCTION dodaj_schemat(id_uzytkownika_attr integer, nazwa varchar(100),opis varchar(500),widoczny_publiczne BOOLEAN) RETURNS VOID AS $$
BEGIN
    IF sprawdz_uprawnienie(id_uzytkownika_attr,'TWORZENIE_SCHEMATOW') THEN
        IF length(nazwa)<5 OR length(nazwa)>100 THEN
            RAISE 'Nazwa schematu powinna miec dlugosc (5-100) znakow';
        END IF;

        IF nazwa  NOT SIMILAR TO  '[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ][a-z0-9A-Z\ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*[a-z0-9A-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]' THEN
            RAISE 'Dopuszczalne są tylko litery (polski i angielski alfabet) spacje i cyfry, nazwa nie może także kończyć i zaczynać się spacją';
        END IF;

        INSERT INTO schematy(nazwa_schematu, szczegulowy_opis, id_uzytkownika,widoczny_publicznie)  VALUES  (nazwa,opis,id_uzytkownika_attr,widoczny_publiczne);

    ELSE
        RAISE 'Brak uprawnien';
    END IF;

    EXCEPTION
    WHEN unique_violation THEN
        RAISE 'Schemat o podanej nazwie już istnieje';
END;
$$ LANGUAGE plpgsql;

-- zmienia opis lub widoczność schematu, rzuca wyjątek jeśli
-- - schemat nie istnieje lub brak uprawnień do upublicznienia
CREATE OR REPLACE FUNCTION zmien_schemat(id_uzytkownika_attr INTEGER,id_schematu_attr INTEGER,opis VARCHAR(500),widoczny_publicznie_attr BOOLEAN) RETURNS VOID AS $$
DECLARE
    zmienione_wiersze INTEGER;
BEGIN
    IF NOT sprawdz_uprawnienie(id_uzytkownika_attr,'UPUBLICZNIANIE_SCHEMATOW') AND widoczny_publicznie_attr THEN
        RAISE 'Nie masz uprawnień do edycji schematu - prawdopodobnie dostałeś bana, napisz do administratora';
    END IF;
    WITH rows AS (
        UPDATE schematy SET szczegulowy_opis=opis, widoczny_publicznie=widoczny_publicznie_attr WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_schematu=id_schematu_attr
        RETURNING 1
    ) SELECT COUNT(*) FROM rows INTO zmienione_wiersze;

    IF zmienione_wiersze=0 THEN
        RAISE 'Schemat nie istnieje lub nie jesteś jego właścicielem';
    END IF;
END;
$$ LANGUAGE plpgsql;

-- usuwa schemat rzuca wyjątek jeśli:
-- - schemat nie istnieje
CREATE OR REPLACE FUNCTION usun_schemat(id_uzytkownika_attr integer,id_schematu_attr integer) RETURNS VOID AS $$
DECLARE
    ilosc_usunietych_wierszy integer;
BEGIN
    WITH rows AS (
        DELETE FROM schematy WHERE id_uzytkownika = id_uzytkownika_attr AND identyfikator_schematu=id_schematu_attr
        RETURNING 1
    )
    SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

    IF ilosc_usunietych_wierszy=0 THEN
        RAISE 'Wskazany schemat nie istnieje';
    END IF;
END;
$$ LANGUAGE plpgsql;

-- usuwa schemat jako moderator rzuca wyjatek jesli:
-- - schemat nie istnieje
-- - brak uprawnien do usuwania
CREATE OR REPLACE FUNCTION usun_schemat_jako_moderator(id_uzytkownika_attr integer,id_schematu_attr integer) RETURNS VOID AS $$
DECLARE
    ilosc_usunietych_wierszy integer;
BEGIN
    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'USUWANIE_OBCYCH_SCHEMATOW') THEN
        RAISE 'Nie masz uprawnień do usuwania cudzych schematów';
    END IF;

    WITH rows AS (
        DELETE FROM schematy WHERE identyfikator_schematu=id_schematu_attr
        RETURNING 1
    )
    SELECT COUNT(*) INTO ilosc_usunietych_wierszy FROM rows;

    IF ilosc_usunietych_wierszy=0 THEN
        RAISE 'Wskazany schemat nie istnieje';
    END IF;
END;
$$ LANGUAGE plpgsql;


-- dodaj część do schematu, rzuca wyjątek jeśli
-- - ilosc <= 0
-- - część zablokowana
-- - uzytkownik nie jest właścicielem
CREATE OR REPLACE FUNCTION dodaj_do_schematu(id_uzytkownika_attr INTEGER,id_schematu_attr INTEGER, id_czesci INTEGER, ilosc_attr INTEGER) RETURNS VOID AS $$
DECLARE
    czesc_zablokowana BOOLEAN;
    schemat_uzytkownika INTEGER;
BEGIN
    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'MODYFIKACJA_SCHEMATOW') THEN
        RAISE 'Brak uprawnień do modyfikacji schematów';
    END IF;
    IF ilosc_attr<=0 THEN
        RAISE 'Ilość wymaganych części musi być większa od 0';
    END IF;

    SELECT zablokowana FROM czesci_elektroniczne WHERE identyfikator_czesci=id_czesci INTO czesc_zablokowana;

    IF czesc_zablokowana THEN
        RAISE 'Część jest zablokowana, nie możesz dodawać jej do nowych schematów';
    END IF;

    SELECT COUNT(id_uzytkownika) FROM schematy WHERE id_uzytkownika=id_uzytkownika_attr  AND  identyfikator_schematu=id_schematu_attr INTO schemat_uzytkownika;

    IF schemat_uzytkownika != 1 THEN
        RAISE 'Nie jesteś właścicielem tego schematu';
    END IF;


    INSERT INTO przypisanie_czesci_do_schematu(identyfikator_schematu,identyfikator_czesci,wymagana_ilosc) VALUES (id_schematu_attr,id_czesci,ilosc_attr);

    EXCEPTION
    WHEN UNIQUE_VIOLATION THEN RAISE 'Część jest już w schemacie';
    WHEN FOREIGN_KEY_VIOLATION THEN RAISE  'Część lub schemat nie istnieje';

END;
$$ LANGUAGE plpgsql;

-- usuwa część z schematu
CREATE OR REPLACE FUNCTION usun_czesc_z_schematu (id_uzytkownika_attr INTEGER,id_schematu_attr INTEGER, id_czesci INTEGER) RETURNS VOID AS $$
DECLARE
    ilosc_usunietych_wierszy INTEGER;
    schemat_uzytkownika INTEGER;
BEGIN
    SELECT COUNT(id_uzytkownika) FROM schematy WHERE id_uzytkownika=id_uzytkownika_attr AND identyfikator_schematu=id_schematu_attr INTO schemat_uzytkownika;

    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'MODYFIKACJA_SCHEMATOW') THEN
        RAISE 'Brak uprawnień do modyfikacji schematów';
    END IF;

    IF schemat_uzytkownika != 1 THEN
        RAISE 'Nie jesteś właścicielem tego schematu';
    END IF;

    WITH rows AS (
        DELETE FROM przypisanie_czesci_do_schematu WHERE identyfikator_schematu=id_schematu_attr AND identyfikator_czesci=id_czesci
        RETURNING 1
    ) SELECT COUNT(*) FROM rows INTO ilosc_usunietych_wierszy;

    IF ilosc_usunietych_wierszy=0 THEN
        RAISE 'Nie udało się usunąć części';
    END IF;

END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------
--
--                               komentarze service
--
----------------------------------------------------------------------------------------------------

-- dodaje komentarz do schematu rzuca wyjątek jeśli:
-- - user nie ma uprawnień do komentowania
-- - zła długość komentarza
-- - schemat nie istnieje
CREATE OR REPLACE FUNCTION dodaj_komentarz_do_schematu(id_uzytkownika_attr INTEGER,id_schematu_attr INTEGER, tresc_komentarza VARCHAR(500)) RETURNS VOID AS $$

BEGIN
    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'KOMENTOWANIE_SCHEMATU') THEN
        RAISE 'Nie masz uprawnień do komentowania schematu';
    END IF;

    IF length(tresc_komentarza)<3 OR length(tresc_komentarza)>500 THEN
        RAISE 'Komentarz ma niedozwoloną długość';
    END IF;

    INSERT INTO komentarze_do_schematow(id_uzytkownika,identyfikator_schematu,tresc,zgloszony_do_sprawdzenia) VALUES (id_uzytkownika_attr,id_schematu_attr,tresc_komentarza,FALSE);
EXCEPTION
    WHEN FOREIGN_KEY_VIOLATION THEN RAISE 'Podany schemat nie istnieje';
END;
$$ LANGUAGE plpgsql;


-- przekazuje komentarz do weryfikacji rzuca wyjątek jeśli:
-- - brak uprawnień
-- - komentarz nie istnieje
CREATE OR REPLACE FUNCTION przekaz_komentarz_do_weryfikacji(id_uzytkownika_attr INTEGER,id_komentarza_attr INTEGER) RETURNS VOID AS $$
DECLARE
    ilosc_zmienionych_wierszy INTEGER;
BEGIN
    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'PRZEKAZYWANIE_KOMENTARZA_DO_SPRAWDZENIA') THEN
        RAISE 'Nie masz uprawnień do przekazania komentarza do weryfikacji';
    END IF;

    WITH rows AS(
        UPDATE komentarze_do_schematow SET zgloszony_do_sprawdzenia=TRUE WHERE identyfikator_komentarza=id_komentarza_attr
        RETURNING 1
    ) SELECT COUNT(*) FROM rows INTO ilosc_zmienionych_wierszy;

    IF ilosc_zmienionych_wierszy = 0 THEN
        RAISE 'Komentarz nie istnieje';
    END IF;
END;
$$ LANGUAGE plpgsql;

-- usuwa komentarz z listy oczekujących na weryfikację, rzuca wyjątek jeśli:
-- - brak uprawnień
-- - komentarz nie istnieje
CREATE OR REPLACE FUNCTION cofnij_komentarz_z_weryfikacji(id_uzytkownika_attr INTEGER,id_komentarza_attr INTEGER) RETURNS VOID AS $$
DECLARE
    ilosc_zmienionych_wierszy INTEGER;
BEGIN
    IF not sprawdz_uprawnienie(id_uzytkownika_attr,'WERYFIKACJA_KOMENTARZY') THEN
        RAISE 'Nie masz uprawnień do weryfikacji komentarza';
    END IF;

    WITH rows AS(
        UPDATE komentarze_do_schematow SET zgloszony_do_sprawdzenia=FALSE WHERE identyfikator_komentarza=id_komentarza_attr
        RETURNING 1
    ) SELECT COUNT(*) FROM rows INTO ilosc_zmienionych_wierszy;

    IF ilosc_zmienionych_wierszy = 0 THEN
        RAISE 'Podany komentarz  nie istnieje';
    END IF;
END;
$$ LANGUAGE plpgsql;


-- usuwa komentarz rzuca wyjątek jeśli:
-- - brak uprawnień
-- - komentarz nie istnieje
CREATE OR REPLACE FUNCTION usun_komentarz(id_uzytkownika_attr INTEGER,id_komentarza_attr INTEGER) RETURNS VOID AS $$
DECLARE
    wlasciciel_komentarza INTEGER;
BEGIN
    SELECT id_uzytkownika FROM komentarze_do_schematow WHERE identyfikator_komentarza=id_komentarza_attr INTO wlasciciel_komentarza;

    IF wlasciciel_komentarza is NULL THEN
        RAISE 'Podany komentarz  nie istnieje';
    END IF;

    IF sprawdz_uprawnienie(id_uzytkownika_attr,'WERYFIKACJA_KOMENTARZY') or  wlasciciel_komentarza=id_uzytkownika_attr THEN
        DELETE FROM komentarze_do_schematow WHERE identyfikator_komentarza=id_komentarza_attr;
    ELSE
        RAISE 'Nie masz uprawnień do usunięcia komentarza';
    END IF;
END;
$$ LANGUAGE plpgsql;









