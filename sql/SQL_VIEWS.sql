----------------------------------------------------------------------------------------------------
--
--                              widoki
--
----------------------------------------------------------------------------------------------------

-- zwraca tabele zawierajaca informacje o uprawnieniach przyslugujacych uzytkownikom
-- przyklad:
-- id_uzytkownika | login |        nazwa_uprawnienia
------------------+-------+----------------------------------
--              1 | admin | DODAWANIE_CZESCI
--              1 | admin | EDYCJA_GRUP_UPRAWNIEN
--              1 | admin | EDYCJA_KATEGORI
--              1 | admin | MODYFIKACJA_CZESCI
CREATE OR REPLACE VIEW view_uzytkownicy_uprawnienia AS
SELECT id_uzytkownika,login,nazwa_uprawnienia
FROM definicje_uprawnien AS du
INNER JOIN grupy_uprawnien AS gu ON du.identyfikator_grupy=gu.identyfikator_grupy
INNER JOIN uzytkownicy AS uz ON uz.identyfikator_grupy = gu.identyfikator_grupy;

-- zwraca tabele zawierajaca grupy i ich uprawnienia
-- przyklad:
--  identyfikator_grupy | nazwa_grupy |        nazwa_uprawnienia
--  -------------------+-------------+----------------------------------
--                    2 | admin       | EDYCJA_GRUP_UPRAWNIEN
--                    2 | admin       | USUWANIE_UZYTKOWNIKOW
--                    2 | admin       | USUWANIE_CZESCI
--                    2 | admin       | PRZYPISYWANIE_CZESCI_DO_MAGAZYNU
CREATE OR REPLACE VIEW view_grupy_uprawnienia AS
SELECT gu.identyfikator_grupy,nazwa_grupy,nazwa_uprawnienia
FROM definicje_uprawnien AS du
INNER JOIN grupy_uprawnien AS gu ON du.identyfikator_grupy=gu.identyfikator_grupy;

-- zwraca liste schematow z iloscia wykorzystanych w nich elementow
CREATE OR REPLACE VIEW view_schematy_raport AS
select sch.identyfikator_schematu,nazwa_schematu, widoczny_publicznie, COUNT(wymagana_ilosc) as roznych_elementow, sch.id_uzytkownika, SUM(wymagana_ilosc) as wszystkich_czesci
from schematy as sch left join przypisanie_czesci_do_schematu as  pc on sch.identyfikator_schematu=pc.identyfikator_schematu GROUP BY sch.identyfikator_schematu,nazwa_schematu,sch.id_uzytkownika;


CREATE OR REPLACE VIEW view_uzytkownicy_grupy AS
SELECT id_uzytkownika,login,nazwa_grupy
FROM uzytkownicy AS uz
INNER JOIN grupy_uprawnien AS gu ON uz.identyfikator_grupy=gu.identyfikator_grupy;


-- przygotowuje zestawienie nazwa_grupy -> ilosc uzytkownikow
-- przyklad:
--   nazwa_grupy    | ilosc_uzytkownikow
--------------------+--------------------
-- domyslna         |                  1
-- grupka           |                  0
-- admin            |                  1
CREATE OR REPLACE VIEW view_grupy_raport AS
SELECT nazwa_grupy,COUNT(login) AS ilosc_uzytkownikow
FROM uzytkownicy AS uz
RIGHT JOIN grupy_uprawnien AS gu ON uz.identyfikator_grupy=gu.identyfikator_grupy GROUP BY nazwa_grupy;


-- przygotowuje zestawienie nazwa_kategori -> ilosc_czesci
-- przyklad:
--  nazwa_kategori | ilosc_czesci
-- ----------------+--------------
--  UART LEVEL     |           87
--  NOWA Nazwa     |          210
CREATE OR REPLACE VIEW view_kategorie_raport AS
select nazwa_kategori,COUNT(identyfikator_czesci) AS ilosc_czesci FROM czesci_elektroniczne AS ce RIGHT JOIN kategorie_czesci AS kt ON ce.id_kategori=kt.id_kategori GROUP BY nazwa_kategori;

-- przygotowuje zestawienie kategoria -> ilosc magazynow
-- przyklad:
-- nazwa_elementu |  nazwa_kategori  | ilosz_magazynow | zablokowana
------------------+------------------+-----------------+-------------
-- UART6          | UART LEVEL       |               0 | f
-- TTL67          | ADC przetworniki |               0 | f
-- TTL171         | ADC przetworniki |               0 | f
-- TTL51          | ADC przetworniki |               0 | f
-- TTL186         | ADC przetworniki |               0 | f
-- UART71         | UART LEVEL       |               0 | f
CREATE OR REPLACE VIEW view_czesci_kategorie_raport AS
select  ce.identyfikator_czesci,nazwa_elementu,nazwa_kategori,COUNT(ilosc_sztuk) AS ilosz_magazynow,zablokowana from czesci_elektroniczne AS ce INNER JOIN kategorie_czesci AS kc ON ce.id_kategori=kc.id_kategori LEFT JOIN czesci_elektroniczne_magazyn AS cm ON ce.identyfikator_czesci=cm.identyfikator_czesci GROUP BY ce.identyfikator_czesci,nazwa_elementu,zablokowana,nazwa_kategori;

CREATE OR REPLACE VIEW view_czesci_magazyn_raport AS
select  nazwa_elementu,id_uzytkownika,opis,nazwa_kategori,ilosc_sztuk,zablokowana , ce.identyfikator_czesci from czesci_elektroniczne AS ce INNER JOIN kategorie_czesci AS kc ON ce.id_kategori=kc.id_kategori INNER JOIN czesci_elektroniczne_magazyn AS cm ON ce.identyfikator_czesci=cm.identyfikator_czesci ;



-- zwraca tabele zawierajaca szczegolowe informacje o czesci razem z nazwa jej kategorii
-- przyklad:
-- identyfikator_czesci | nazwa_elementu |  nazwa_kategori  |         opis         | zablokowana
------------------------+----------------+------------------+----------------------+-------------
--                  311 | ABCDE          | ADC przetworniki |                      | f
--                  315 | dasdas         | ADC przetworniki | dasdasd\r           +| f
--                      |                |                  |                      |
--                  314 | dfddfdf        | UART LEVEL       |                      | f
--                   75 | UART73         | UART LEVEL       |                      | f
--                   76 | UART74         | UART LEVEL       |                      | f
CREATE OR REPLACE VIEW view_czesci_szczegoly AS
select  identyfikator_czesci,nazwa_elementu,nazwa_kategori,opis ,zablokowana from czesci_elektroniczne AS ce INNER JOIN kategorie_czesci AS kc ON ce.id_kategori=kc.id_kategori  ;


CREATE OR REPLACE VIEW view_czesci_dokumentacje AS
select identyfikator_dokumentacji,nazwa_elementu,nazwa_dokumentacji,link from dokumentacje AS dk INNER JOIN czesci_elektroniczne AS ce ON dk.identyfikator_czesci=ce.identyfikator_czesci;

CREATE OR REPLACE VIEW view_czesci_noty AS
select identyfikator_noty,nazwa_elementu,nazwa_noty,link from noty_aplikacyjne AS no INNER JOIN czesci_elektroniczne AS ce ON no.identyfikator_czesci=ce.identyfikator_czesci;


-- zwraca tabele z czesciami znajdujacymi sie w magazynie ich nazwami oraz wlascicielem
-- przyklad:
-- identyfikator_czesci | nazwa_elementu | id_uzytkownika
------------------------+----------------+----------------
--                  311 | ABCDE          |              1
--                  314 | dfddfdf        |              1
--                   97 | UART95         |              1
--                  111 | TTL10          |              1
--                   11 | UART9          |              1
CREATE OR REPLACE VIEW view_nazwa_magazyn_id_uzytkownika AS
select cm.identyfikator_czesci,nazwa_elementu,id_uzytkownika FROM czesci_elektroniczne_magazyn AS cm INNER JOIN czesci_elektroniczne AS ce ON cm.identyfikator_czesci=ce.identyfikator_czesci;

CREATE OR REPLACE VIEW view_czesci_magazyn_tagi AS
select  ce.identyfikator_czesci,nazwa_elementu,opis,nazwa_kategori,ilosc_sztuk,nazwa_tagu,cm.id_uzytkownika from czesci_elektroniczne AS ce INNER JOIN kategorie_czesci AS kc ON ce.id_kategori=kc.id_kategori INNER JOIN czesci_elektroniczne_magazyn AS cm ON ce.identyfikator_czesci=cm.identyfikator_czesci LEFT JOIN przypisanie_tagu AS pt ON cm.identyfikator_czesci=pt.identyfikator_czesci AND cm.id_uzytkownika=pt.id_uzytkownika ;

CREATE OR REPLACE VIEW view_listy_zakupow_elementy AS
SELECT identyfikator_listy_zakupow,cm.identyfikator_czesci,cm.id_uzytkownika,ilosc_do_zamowienia,koszt,czy_koszt_jednostkowy,ilosc_sztuk AS ilosc_sztuk_w_magazynie, nazwa_elementu FROM definicje_elementow_listy_zakupow AS de INNER JOIN czesci_elektroniczne_magazyn AS cm ON de.identyfikator_czesci=cm.identyfikator_czesci AND de.id_uzytkownika=cm.id_uzytkownika INNER JOIN czesci_elektroniczne AS ce ON cm.identyfikator_czesci=ce.identyfikator_czesci;

CREATE OR REPLACE VIEW view_schematy_czesci AS
SELECT pc.identyfikator_schematu, ce.identyfikator_czesci, nazwa_elementu, wymagana_ilosc FROM przypisanie_czesci_do_schematu AS pc INNER JOIN czesci_elektroniczne AS ce ON pc.identyfikator_czesci = ce.identyfikator_czesci;

CREATE OR REPLACE VIEW view_komentarze_uzytkownicy AS
SELECT  ko.identyfikator_komentarza, ko.identyfikator_schematu, uz.login, ko.tresc,ko.zgloszony_do_sprawdzenia FROM komentarze_do_schematow AS ko NATURAL JOIN uzytkownicy AS uz;

CREATE OR REPLACE VIEW view_schematy_komentarze_uzytkownicy AS
SELECT  ko.identyfikator_komentarza, sc.nazwa_schematu, uz.login, ko.tresc,ko.zgloszony_do_sprawdzenia FROM komentarze_do_schematow AS ko NATURAL JOIN uzytkownicy AS uz NATURAL JOIN schematy as sc;

