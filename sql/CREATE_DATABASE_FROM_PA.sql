
CREATE SEQUENCE public.kategorie_czesci_id_kategori_seq;

CREATE TABLE public.kategorie_czesci (
  id_kategori INTEGER NOT NULL DEFAULT nextval('public.kategorie_czesci_id_kategori_seq'),
  nazwa_kategori VARCHAR(50) NOT NULL,
  CONSTRAINT kategorie_czesci_pk PRIMARY KEY (id_kategori)
);


ALTER SEQUENCE public.kategorie_czesci_id_kategori_seq OWNED BY public.kategorie_czesci.id_kategori;

CREATE UNIQUE INDEX kategorie_czesci_idx
  ON public.kategorie_czesci
  ( nazwa_kategori );

CREATE SEQUENCE public.czesci_elektroniczne_identyfikator_czesci_seq_4;

CREATE TABLE public.czesci_elektroniczne (
  identyfikator_czesci INTEGER NOT NULL DEFAULT nextval('public.czesci_elektroniczne_identyfikator_czesci_seq_4'),
  id_kategori INTEGER NOT NULL,
  nazwa_elementu VARCHAR(100) NOT NULL,
  opis VARCHAR(500),
  zablokowana BOOLEAN NOT NULL,
  CONSTRAINT czesci_elektroniczne_pk PRIMARY KEY (identyfikator_czesci)
);


ALTER SEQUENCE public.czesci_elektroniczne_identyfikator_czesci_seq_4 OWNED BY public.czesci_elektroniczne.identyfikator_czesci;

CREATE UNIQUE INDEX czesci_elektroniczne_idx
  ON public.czesci_elektroniczne
  ( nazwa_elementu );

CREATE SEQUENCE public.noty_aplikacyjne_identyfikator_noty_seq;

CREATE TABLE public.noty_aplikacyjne (
  identyfikator_noty INTEGER NOT NULL DEFAULT nextval('public.noty_aplikacyjne_identyfikator_noty_seq'),
  identyfikator_czesci INTEGER NOT NULL,
  nazwa_noty VARCHAR(50) NOT NULL,
  link VARCHAR(250) NOT NULL,
  CONSTRAINT noty_aplikacyjne_pk PRIMARY KEY (identyfikator_noty)
);


ALTER SEQUENCE public.noty_aplikacyjne_identyfikator_noty_seq OWNED BY public.noty_aplikacyjne.identyfikator_noty;

CREATE UNIQUE INDEX noty_aplikacyjne_idx
  ON public.noty_aplikacyjne
  ( identyfikator_czesci, nazwa_noty );

CREATE SEQUENCE public.dokumentacje_identyfikator_dokumentacji_seq;

CREATE TABLE public.dokumentacje (
  identyfikator_dokumentacji INTEGER NOT NULL DEFAULT nextval('public.dokumentacje_identyfikator_dokumentacji_seq'),
  identyfikator_czesci INTEGER NOT NULL,
  nazwa_dokumentacji VARCHAR(50) NOT NULL,
  link VARCHAR(250) NOT NULL,
  CONSTRAINT dokumentacje_pk PRIMARY KEY (identyfikator_dokumentacji)
);


ALTER SEQUENCE public.dokumentacje_identyfikator_dokumentacji_seq OWNED BY public.dokumentacje.identyfikator_dokumentacji;

CREATE UNIQUE INDEX dokumentacje_idx
  ON public.dokumentacje
  ( identyfikator_czesci, nazwa_dokumentacji );

CREATE TABLE public.uprawnienia (
  nazwa_uprawnienia VARCHAR(100) NOT NULL,
  CONSTRAINT uprawnienia_pk PRIMARY KEY (nazwa_uprawnienia)
);
COMMENT ON COLUMN public.uprawnienia.nazwa_uprawnienia IS 'Encja słownikowa';


CREATE SEQUENCE public.grupy_uprawnien_identyfikator_grupy_seq_1;

CREATE TABLE public.grupy_uprawnien (
  identyfikator_grupy INTEGER NOT NULL DEFAULT nextval('public.grupy_uprawnien_identyfikator_grupy_seq_1'),
  nazwa_grupy VARCHAR(50) NOT NULL,
  CONSTRAINT grupy_uprawnien_pk PRIMARY KEY (identyfikator_grupy)
);


ALTER SEQUENCE public.grupy_uprawnien_identyfikator_grupy_seq_1 OWNED BY public.grupy_uprawnien.identyfikator_grupy;

CREATE UNIQUE INDEX grupy_uprawnien_idx
  ON public.grupy_uprawnien
  ( nazwa_grupy );

CREATE TABLE public.definicje_uprawnien (
  nazwa_uprawnienia VARCHAR(100) NOT NULL,
  identyfikator_grupy INTEGER NOT NULL,
  CONSTRAINT definicje_uprawnien_pk PRIMARY KEY (nazwa_uprawnienia, identyfikator_grupy)
);
COMMENT ON COLUMN public.definicje_uprawnien.nazwa_uprawnienia IS 'Encja słownikowa';


CREATE SEQUENCE public.uzytkownicy_id_uzytkownika_seq;

CREATE TABLE public.uzytkownicy (
  id_uzytkownika INTEGER NOT NULL DEFAULT nextval('public.uzytkownicy_id_uzytkownika_seq'),
  login VARCHAR(20) NOT NULL,
  haslo VARCHAR(40) NOT NULL,
  identyfikator_grupy INTEGER NOT NULL,
  CONSTRAINT uzytkownicy_pk PRIMARY KEY (id_uzytkownika)
);


ALTER SEQUENCE public.uzytkownicy_id_uzytkownika_seq OWNED BY public.uzytkownicy.id_uzytkownika;

CREATE UNIQUE INDEX uzytkownicy_idx
  ON public.uzytkownicy
  ( login );

CREATE TABLE public.tagi (
  id_uzytkownika INTEGER NOT NULL,
  nazwa_tagu VARCHAR(20) NOT NULL,
  CONSTRAINT tagi_pk PRIMARY KEY (id_uzytkownika, nazwa_tagu)
);


CREATE SEQUENCE public.listy_zakupow_identyfikator_listy_zakupow_seq;

CREATE TABLE public.listy_zakupow (
  identyfikator_listy_zakupow INTEGER NOT NULL DEFAULT nextval('public.listy_zakupow_identyfikator_listy_zakupow_seq'),
  id_uzytkownika INTEGER NOT NULL,
  nazwa_listy_zakupow VARCHAR(50) NOT NULL,
  czas_utworzenia DATE NOT NULL,
  termin_realizacji DATE,
  CONSTRAINT listy_zakupow_pk PRIMARY KEY (identyfikator_listy_zakupow)
);


ALTER SEQUENCE public.listy_zakupow_identyfikator_listy_zakupow_seq OWNED BY public.listy_zakupow.identyfikator_listy_zakupow;

CREATE UNIQUE INDEX listy_zakupow_idx
  ON public.listy_zakupow
  ( nazwa_listy_zakupow, id_uzytkownika );

CREATE TABLE public.czesci_elektroniczne_magazyn (
  identyfikator_czesci INTEGER NOT NULL,
  id_uzytkownika INTEGER NOT NULL,
  ilosc_sztuk INTEGER NOT NULL,
  CONSTRAINT czesci_elektroniczne_magazyn_pk PRIMARY KEY (identyfikator_czesci, id_uzytkownika)
);


CREATE TABLE public.przypisanie_tagu (
  identyfikator_czesci INTEGER NOT NULL,
  id_uzytkownika INTEGER NOT NULL,
  nazwa_tagu VARCHAR(20) NOT NULL,
  CONSTRAINT przypisanie_tagu_pk PRIMARY KEY (identyfikator_czesci, id_uzytkownika, nazwa_tagu)
);


CREATE TABLE public.definicje_elementow_listy_zakupow (
  identyfikator_listy_zakupow INTEGER NOT NULL,
  identyfikator_czesci INTEGER NOT NULL,
  id_uzytkownika INTEGER NOT NULL,
  ilosc_do_zamowienia INTEGER NOT NULL,
  koszt NUMERIC NOT NULL,
  czy_koszt_jednostkowy BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT definicje_elementow_listy_zakupow_pk PRIMARY KEY (identyfikator_listy_zakupow, identyfikator_czesci, id_uzytkownika)
);


CREATE SEQUENCE public.schematy_identyfikator_schematu_seq;

CREATE TABLE public.schematy (
  identyfikator_schematu INTEGER NOT NULL DEFAULT nextval('public.schematy_identyfikator_schematu_seq'),
  nazwa_schematu VARCHAR(100) NOT NULL,
  szczegulowy_opis VARCHAR(500) NOT NULL,
  widoczny_publicznie BOOLEAN DEFAULT False NOT NULL,
  id_uzytkownika INTEGER NOT NULL,
  CONSTRAINT schematy_pk PRIMARY KEY (identyfikator_schematu)
);


ALTER SEQUENCE public.schematy_identyfikator_schematu_seq OWNED BY public.schematy.identyfikator_schematu;

CREATE UNIQUE INDEX schematy_idx
  ON public.schematy
  ( nazwa_schematu, id_uzytkownika );

CREATE SEQUENCE public.komentarze_do_schematow_identyfikator_komentarza_seq;

CREATE TABLE public.komentarze_do_schematow (
  identyfikator_komentarza INTEGER NOT NULL DEFAULT nextval('public.komentarze_do_schematow_identyfikator_komentarza_seq'),
  identyfikator_schematu INTEGER NOT NULL,
  id_uzytkownika INTEGER NOT NULL,
  tresc VARCHAR(500) NOT NULL,
  zgloszony_do_sprawdzenia BOOLEAN NOT NULL,
  CONSTRAINT komentarze_do_schematow_pk PRIMARY KEY (identyfikator_komentarza)
);


ALTER SEQUENCE public.komentarze_do_schematow_identyfikator_komentarza_seq OWNED BY public.komentarze_do_schematow.identyfikator_komentarza;

CREATE TABLE public.przypisanie_czesci_do_schematu (
  identyfikator_czesci INTEGER NOT NULL,
  identyfikator_schematu INTEGER NOT NULL,
  wymagana_ilosc INTEGER NOT NULL,
  CONSTRAINT przypisanie_czesci_do_schematu_pk PRIMARY KEY (identyfikator_czesci, identyfikator_schematu)
);


ALTER TABLE public.czesci_elektroniczne ADD CONSTRAINT kategorie_czesci_czesci_elektroniczne_fk
FOREIGN KEY (id_kategori)
REFERENCES public.kategorie_czesci (id_kategori)
ON DELETE RESTRICT
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.czesci_elektroniczne_magazyn ADD CONSTRAINT czesci_elektroniczne_czesci_elektroniczne_magazyn_fk
FOREIGN KEY (identyfikator_czesci)
REFERENCES public.czesci_elektroniczne (identyfikator_czesci)
ON DELETE RESTRICT
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.przypisanie_czesci_do_schematu ADD CONSTRAINT czesci_elektroniczne_przypisanie_czesci_do_schematu_fk
FOREIGN KEY (identyfikator_czesci)
REFERENCES public.czesci_elektroniczne (identyfikator_czesci)
ON DELETE RESTRICT
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.dokumentacje ADD CONSTRAINT czesci_elektroniczne_dokumentacje_fk
FOREIGN KEY (identyfikator_czesci)
REFERENCES public.czesci_elektroniczne (identyfikator_czesci)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.noty_aplikacyjne ADD CONSTRAINT czesci_elektroniczne_noty_aplikacyjne_fk
FOREIGN KEY (identyfikator_czesci)
REFERENCES public.czesci_elektroniczne (identyfikator_czesci)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.definicje_uprawnien ADD CONSTRAINT uprawnienia_definicje_uprawnien_fk
FOREIGN KEY (nazwa_uprawnienia)
REFERENCES public.uprawnienia (nazwa_uprawnienia)
ON DELETE RESTRICT
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.definicje_uprawnien ADD CONSTRAINT grupy_uprawnien_definicje_uprawnien_fk
FOREIGN KEY (identyfikator_grupy)
REFERENCES public.grupy_uprawnien (identyfikator_grupy)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.uzytkownicy ADD CONSTRAINT grupy_uprawnien_uzytkownicy_fk
FOREIGN KEY (identyfikator_grupy)
REFERENCES public.grupy_uprawnien (identyfikator_grupy)
ON DELETE RESTRICT
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.schematy ADD CONSTRAINT uzytkownicy_schematy_fk
FOREIGN KEY (id_uzytkownika)
REFERENCES public.uzytkownicy (id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.czesci_elektroniczne_magazyn ADD CONSTRAINT uzytkownicy_czesci_elektroniczne_magazyn_fk
FOREIGN KEY (id_uzytkownika)
REFERENCES public.uzytkownicy (id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.listy_zakupow ADD CONSTRAINT uzytkownicy_listy_zakupow_fk
FOREIGN KEY (id_uzytkownika)
REFERENCES public.uzytkownicy (id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.komentarze_do_schematow ADD CONSTRAINT uzytkownicy_komentarze_do_schematow_fk
FOREIGN KEY (id_uzytkownika)
REFERENCES public.uzytkownicy (id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tagi ADD CONSTRAINT uzytkownicy_przypisanie_tagow_fk
FOREIGN KEY (id_uzytkownika)
REFERENCES public.uzytkownicy (id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.przypisanie_tagu ADD CONSTRAINT tagi_przypisanie_tagu_fk
FOREIGN KEY (nazwa_tagu, id_uzytkownika)
REFERENCES public.tagi (nazwa_tagu, id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.definicje_elementow_listy_zakupow ADD CONSTRAINT listy_zakupow_definicje_elementow_listy_zakupow_fk
FOREIGN KEY (identyfikator_listy_zakupow)
REFERENCES public.listy_zakupow (identyfikator_listy_zakupow)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.definicje_elementow_listy_zakupow ADD CONSTRAINT czesci_elektroniczne_magazyn_definicje_elementow_listy_zakup616
FOREIGN KEY (identyfikator_czesci, id_uzytkownika)
REFERENCES public.czesci_elektroniczne_magazyn (identyfikator_czesci, id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.przypisanie_tagu ADD CONSTRAINT czesci_elektroniczne_magazyn_przypisanie_tagu_fk
FOREIGN KEY (identyfikator_czesci, id_uzytkownika)
REFERENCES public.czesci_elektroniczne_magazyn (identyfikator_czesci, id_uzytkownika)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.przypisanie_czesci_do_schematu ADD CONSTRAINT schematy_przypisanie_czesci_do_schematu_fk
FOREIGN KEY (identyfikator_schematu)
REFERENCES public.schematy (identyfikator_schematu)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.komentarze_do_schematow ADD CONSTRAINT schematy_komentarze_do_schematow_fk
FOREIGN KEY (identyfikator_schematu)
REFERENCES public.schematy (identyfikator_schematu)
ON DELETE CASCADE
ON UPDATE NO ACTION
NOT DEFERRABLE;