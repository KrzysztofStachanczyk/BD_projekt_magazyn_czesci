<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 15:34
 */

require_once("../CzesciService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;


if(!isset($_POST['nazwa_czesci']) or !isset($_POST['opis']) or !isset($_POST['kategoria'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        CzesciService::dodajCzesc($_SESSION['user_id'],
            htmlspecialchars($_POST['nazwa_czesci']),
            htmlspecialchars($_POST['opis']),
            htmlspecialchars($_POST['kategoria'])
            );
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);