<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 23:44
 */

require_once("../NotyService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;


if(!isset($_POST['nazwa_czesci']) or !isset($_POST['nazwa_noty']) or  !isset($_POST['link'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        NotyService::dodajNote($_SESSION['user_id'],
            htmlspecialchars($_POST['nazwa_czesci']),
            htmlspecialchars($_POST['nazwa_noty']),
            htmlspecialchars($_POST['link'])
        );
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);