<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 23:22
 */

require_once("../KategorieService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledach=null;


if(!isset($_POST['nazwa_kategorii'])){
    $komunikat_o_bledach = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledach = "Użytkownik nie jest zalogowany";
}
else{
    try{
        KategorieService::usunKategorie($_SESSION['user_id'],$_POST['nazwa_kategorii']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledach=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledach);

header('Content-Type: application/json');
echo json_encode($result_array);