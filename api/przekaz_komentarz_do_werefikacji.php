<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 08.01.17
 * Time: 22:15
 */

require_once("../KomentarzeService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;

if(!isset($_POST['id_komentarza'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        KomentarzeService::przekazKomentarzDoWeryfikacji($_SESSION['user_id'],$_POST['id_komentarza']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);