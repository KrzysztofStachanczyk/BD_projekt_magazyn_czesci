<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 17:51
 */

require_once("../CzesciMagazynService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;

if(!isset($_POST['nazwa_czesci'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        CzesciMagazynService::dodajCzescDoMagazynu($_SESSION['user_id'],$_POST['nazwa_czesci']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);