<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 03.01.17
 * Time: 00:53
 */

require_once("../ListyZakupowDefinicjeService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;


if(!isset($_POST['list_id'])  or !isset($_POST['id_czesci']) ){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        ListyZakupowDefinicjeService::usunElementZListyZakupow($_SESSION['user_id'],$_POST['list_id'],$_POST['id_czesci']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);