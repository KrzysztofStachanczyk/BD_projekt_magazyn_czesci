<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 19:05
 */

require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;


if(!isset($_POST['user_login'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if(!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else if($_POST['user_login']==$_SESSION['user_name']){
    $komunikat_o_bledzie = "Nie możesz usunąć sam siebie";
}
else{
    try{
        UzytkownicyService::usunUzytkownika($_SESSION['user_id'],$_POST['user_login']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);