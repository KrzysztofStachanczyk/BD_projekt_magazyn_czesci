<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 09.01.17
 * Time: 13:26
 */

require_once("../ListyZakupowService.php");
require_once ("../UzytkownicyService.php");

$koszt=-1;

if(UzytkownicyService::czyZalogowany() and isset($_POST['id_listy_zakupow'])){
    try{
        $koszt=ListyZakupowService::pobierzKosztZakupu($_SESSION['user_id'],$_POST['id_listy_zakupow']);
    }
    catch (Exception $e){}
}

$result_array=array('koszt'=>$koszt);
header('Content-Type: application/json');
echo json_encode($result_array);
