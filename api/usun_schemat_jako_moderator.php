<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 07.01.17
 * Time: 21:29
 */

require_once("../UzytkownicyService.php");
require_once ("../SchematyService.php");

$status=false;
$komunikat_o_bledzie=null;


if(!isset($_POST['id_schematu'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{

    try{
        SchematyService::usunSchematJakoModerator($_SESSION['user_id'],
            $_POST['id_schematu']
        );
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);