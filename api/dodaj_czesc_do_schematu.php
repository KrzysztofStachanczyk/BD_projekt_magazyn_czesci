<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 08.01.17
 * Time: 18:29
 */

require_once("../SchematyService.php");
require_once ("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;

if(!isset($_POST['id_czesci']) or !isset($_POST['ilosc_czesci']) or !isset($_POST['id_schematu'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else if (!UzytkownicyService::czyZalogowany()){
    $komunikat_o_bledzie = "Użytkownik nie jest zalogowany";
}
else{
    try{
        SchematyService::dodajCzescDoSchematu($_SESSION['user_id'],$_POST['id_schematu'],$_POST['id_czesci'],$_POST['ilosc_czesci']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);