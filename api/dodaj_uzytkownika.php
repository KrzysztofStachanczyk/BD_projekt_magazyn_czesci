<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 21:27
 */

require_once("../UzytkownicyService.php");

$status=false;
$komunikat_o_bledzie=null;

if(!isset($_POST['login']) or !isset($_POST['haslo']) or !isset($_POST['haslo_retype'])){
    $komunikat_o_bledzie = "Żądanie niekompletne";
}
else{
    try{
        UzytkownicyService::utworzUzytkowanika($_POST['login'],$_POST['haslo'],$_POST['haslo_retype']);
        UzytkownicyService::zalogujUzytkownika($_POST['login'],$_POST['haslo']);
        $status=true;
    }catch (Exception $e) {
        $komunikat_o_bledzie=$e->getMessage();
    }
}

$result_array=array('status'=>$status,'komunikat_o_bledzie'=>$komunikat_o_bledzie);

header('Content-Type: application/json');
echo json_encode($result_array);
