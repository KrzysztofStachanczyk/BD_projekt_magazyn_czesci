<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 12:04
 *
 * Nu Html Checker - OK
 */


require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "KategorieService.php";
require_once "DashBoardRendererService.php";
require_once "CzesciService.php";
require_once "CzesciMagazynService.php";
// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];

if (!in_array("DODAWANIE_CZESCI", $uprawnienia_uzytkownika) and !in_array("USUWANIE_CZESCI", $uprawnienia_uzytkownika) and !in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia_uzytkownika) and !in_array("MODYFIKACJA_CZESCI", $uprawnienia_uzytkownika)) {
    header("Location: index.php");
    die();
}

$kategorie = null;
try {
    $kategorie = KategorieService::pobierzKategorie();
} catch (Exception $e) {
    die();
}

$wybrana_kategoria = null;

if (isset($_GET['wybrana_kategoria']) and $_GET['wybrana_kategoria'] != null) {
    $wybrana_kategoria = $_GET['wybrana_kategoria'];
}

$limit = 20;
$strona = 1;
$ilosc_czesci = null;
try {
    $ilosc_czesci = CzesciService::pobierzIloscCzesci($wybrana_kategoria);
} catch (Exception $e) {
    die();
}

$ilosc_stron = (int)($ilosc_czesci / $limit) + 1;

// przypadek brzegowy
if (($ilosc_stron - 1) * $limit == $ilosc_czesci) {
    $ilosc_stron = $ilosc_stron - 1;
}

if (isset($_GET['numer_strony']) and is_numeric($_GET['numer_strony'])) {
    $strona = (int)$_GET['numer_strony'];
}

if ($strona > $ilosc_stron) {
    $strona = 1;
}

$offset = $limit * ($strona - 1);

$czesci = null;
try {
    $czesci = CzesciService::pobierzCzesci($wybrana_kategoria, $offset, $limit);
} catch (Exception $e) {
    die();
}

$czesci_w_magazynie = null;
try {
    $czesci_w_magazynie = CzesciMagazynService::pobierzNazwyCzesciUzytkownika($_SESSION['user_id']);
} catch (Exception $e) {
    die();
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Czesci</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header"><span class="glyphicon glyphicon-cog"></span> Zarządzanie częściami</h1>
            <p>W poniższych formularzach możesz dodawać nowe części , modyfikować i usuwać już istniejące oraz dodawać
                je do swojego magazynu.</p>

            <form method="get" action="zarzadzanieCzesciami.php" id="group_select_form">
                <label for="category_select_combo">Kategoria:</label>
                <select id="category_select_combo" name="wybrana_kategoria">
                    <?php if ($wybrana_kategoria == null) {
                        echo "<option value='' selected='selected'>-- wszystkie --</option>";
                    } else {
                        echo "<option value=''>-- wszystkie --</option>";
                    }

                    foreach ($kategorie as $kategoria) {
                        echo '<option value="' . $kategoria['nazwa_kategori'] . '" ';
                        if ($wybrana_kategoria == $kategoria['nazwa_kategori']) {
                            echo 'selected="selected"';
                        }
                        echo '>' . $kategoria['nazwa_kategori'] . '</option>';
                    }
                    ?>
                </select>
                <input type="submit" value="pobierz">
            </form>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Części</h2>

            <ul>
            <?php if ($wybrana_kategoria != null) {
                echo '<li> <strong>Kategoria:</strong> ' . $_GET['wybrana_kategoria'].'</li>';
            }
            echo '<li><strong>Liczba pasujących części:</strong> '.$ilosc_czesci.'</li>';

            ?>
            </ul>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa części</th>
                        <?php if ($wybrana_kategoria == null) {
                            echo '<th>Kategoria</th>';
                        } ?>

                        <th>Ilość magazynów</th>

                        <?php if (in_array("USUWANIE_CZESCI", $uprawnienia_uzytkownika)) {
                            echo "<th>Usuń</th>";
                        } ?>

                        <?php if (in_array("MODYFIKACJA_CZESCI", $uprawnienia_uzytkownika)) {
                            echo "<th> Zablokuj/odblokuj </th >";
                        } else {
                            echo "<th>Status</th>";
                        }
                        ?>

                        <?php if (in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia_uzytkownika)) {
                            echo "<th > Dodaj/usuń z magazynu </th >";
                        } ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($czesci as $czesc) {
                    ?>
                    <tr>
                        <td>
                            <a href="modyfikacjaIPodgladCzesci.php?selected_part=<?php echo $czesc['nazwa_elementu']; ?>">
                            <?php echo $czesc['nazwa_elementu']; ?>
                            </a>
                        </td>

                        <?php if ($wybrana_kategoria == null) {
                            echo '<td>' . $czesc['nazwa_kategorii'] . '</td>';
                        } ?>

                        <td>
                            <?php echo $czesc['ilosz_magazynow']; ?>
                        </td>

                        <?php if (in_array("USUWANIE_CZESCI", $uprawnienia_uzytkownika)) {
                            ?>
                            <td>

                            <?php if (((int)$czesc['ilosz_magazynow']) == 0) { ?>
                                <form class="remove-part">
                                    <input type="hidden" name="nazwa_czesci" value="<?php echo $czesc['nazwa_elementu'] ?>">
                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            <?php } ?>
                            </td><?php
                        } ?>


                        <?php if (in_array("MODYFIKACJA_CZESCI", $uprawnienia_uzytkownika)) {

                            ?>
                            <td>
                                <form>
                                    <input type="hidden" name="nazwa_czesci" value="<?php echo $czesc['nazwa_elementu'] ?>">

                                    <?php if ($czesc['zablokowana'] == 'f') { ?>
                                        <button type="submit" class="btn btn-default block_part"
                                                aria-label="Left Align">

                                            <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
                                        </button>
                                        <?php
                                    } else {
                                        ?>

                                        <button type="submit" class="btn btn-default unlock_part"
                                                aria-label="Left Align">
                                            <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                                        </button>

                                    <?php } ?>
                                </form>
                            </td>

                            <?php
                        } else {
                            echo "<td>";

                            if ($czesc['zablokowana'] == 'f') {
                                echo "dostępna";
                            } else {
                                echo "zablokowana";
                            }
                            echo "</td>";
                        }

                        ?>


                        <?php if (in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia_uzytkownika)) {
                            ?>
                            <td>

                                <form>
                                    <input type="hidden" name="nazwa_czesci" value="<?php echo $czesc['nazwa_elementu'] ?>">

                                    <?php if (in_array($czesc['nazwa_elementu'], $czesci_w_magazynie)) { ?>
                                        <button type="submit" class="btn btn-default remove_from_store"
                                                aria-label="Left Align">


                                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                        </button>

                                    <?php } else { ?>

                                        <button type="submit" class="btn btn-default add_to_store"
                                                aria-label="Left Align">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>

                                    <?php } ?>
                                </form>

                            </td>
                            <?php
                        }

                        }
                        ?>
                    </tbody>
                </table>

                <ul class="pagination">

                    <?php
                    $indeksStrony = 1;
                    while ($indeksStrony <= $ilosc_stron) {
                        if ($indeksStrony == $strona) {
                            echo "<li class='active'>";
                        } else {
                            echo "<li>";
                        }
                        echo "<a href='zarzadzanieCzesciami.php?numer_strony=" . $indeksStrony;
                        if ($wybrana_kategoria != null) {
                            echo "&wybrana_kategoria=" . $wybrana_kategoria;

                        }
                        echo "'>" . $indeksStrony . "</a></li>";
                        $indeksStrony = $indeksStrony + 1;
                    }
                    ?>

                </ul>

            </div>

            <?php if(in_array("DODAWANIE_CZESCI",$uprawnienia_uzytkownika)){?>
            <h2 class="sub-header"><span class="glyphicon glyphicon-plus"></span> Dodaj część</h2>
            <form id="add_part">

                <div class="form-group">
                    <label for="part_name">Nazwa części</label>
                    <input class="form-control" id="part_name" placeholder="Nazwa części" name="nazwa_czesci" required>
                </div>

                <div class="form-group">
                    <label for="part_desc">Opis</label>
                    <textarea class="form-control" id="part_desc" name="opis">

                    </textarea>
                </div>

                <div class="form-group">
                    <label for="part_category">Kategoria:</label>
                    <select class="form-control" id="part_category" name="kategoria">
                        <?php
                        foreach ($kategorie as $czesc) {
                            echo '<option value="' . $czesc['nazwa_kategori'] . '" ';
                            echo '>' . $czesc['nazwa_kategori'] . '</option>';
                        }
                        ?>
                    </select>
                </div>


                <button type="submit" class="btn btn-default " aria-label="Left Align">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                </button>

            </form>
            <?php } ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $("#partsEditLink").addClass("active");

        $("#add_part").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_czesc.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania części");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania części");
                }

            })
        });

        $(".add_to_store").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_czesc_do_magazynu.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania do magazynu");
                    }
                },
                error: function (r) {
                    toastr.error("Błąd dodawania do magazynu");
                }

            })
        });

        $(".remove_from_store").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_czesc_z_magazynu.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania z magazynu");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania z magazynu");
                }

            })
        });

        $(".block_part").click(function (e) {
            e.preventDefault();

            var that = this;

            $.ajax({
                method: "POST",
                url: "api/zablokuj_czesc.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd blokowania części");
                    }
                },
                error: function () {
                    toastr.error("Błąd blokowania części");
                }

            })
        });

        $(".unlock_part").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/odblokuj_czesc.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd odblokowania części");
                    }
                },
                error: function () {
                    toastr.error("Błąd odblokowania części");
                }

            })
        });

        $(".remove-part").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_czesc.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania cześci");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania cześci");
                }

            })
        });
    });


</script>
</body>
</html>