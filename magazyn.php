<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 02.01.17
 * Time: 17:03
 *
 * Nu Html Checker - OK
 */


require_once 'DBConnector.php';
require_once "UzytkownicyService.php";
require_once "DashBoardRendererService.php";
require_once "CzesciMagazynService.php";
require_once "KategorieService.php";
require_once "TagiService.php";
require_once "ListyZakupowService.php";
// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}


$magazyn_lista = null;
$wybrana_kategoria = null;
$lista_tagow = null;
$lista_kategori = null;
$wybrane_tagi = null;
$listy_zakupow = null;

if (isset($_GET['wybrana_kategoria'])) {
    $wybrana_kategoria = $_GET['wybrana_kategoria'];
}


if (isset($_GET['selected_tags'])) {
    $wybrane_tagi = $_GET['selected_tags'];
}

try {
    $magazyn_lista = CzesciMagazynService::pobierzListePozycjiWMagazynie($_SESSION['user_id'], $wybrana_kategoria, $wybrane_tagi);
    $lista_tagow = TagiService::pobierzTagiUzytkownika($_SESSION['user_id']);
    $lista_kategori = KategorieService::pobierzKategorie();
    $listy_zakupow = ListyZakupowService::pobierzListyZakupowUzytkownika($_SESSION['user_id']);
} catch (Exception $e) {
    die();
}

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Magazyn</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"><span class="glyphicon glyphicon-compressed" aria-hidden="true"></span> Magazyn</h1>
            <p>Tutaj znajdują się wszystkie twoje części. </p>


            <h2 class="sub-header"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filtry:</h2>

            <form method="get" action="magazyn.php" id="filter_form">
                <label for="category_select_combo">Kategoria:</label>
                <select id="category_select_combo" name="wybrana_kategoria">
                    <?php if ($wybrana_kategoria == null) {
                        echo "<option value='' selected='selected'>-- wszystkie --</option>";
                    } else {
                        echo "<option value=''>-- wszystkie --</option>";
                    }

                    foreach ($lista_kategori as $kategoria) {
                        echo '<option value="' . $kategoria['nazwa_kategori'] . '" ';
                        if ($wybrana_kategoria == $kategoria['nazwa_kategori']) {
                            echo 'selected="selected"';
                        }
                        echo '>' . $kategoria['nazwa_kategori'] . '</option>';
                    }
                    ?>
                </select>

                <br/>
                <strong> Tagi: </strong><br/>

                <?php foreach ($lista_tagow as $tag) {

                    echo "<input type=\"checkbox\" name=\"selected_tags[]\" value=\"" . $tag . "\" ";
                    if ($wybrane_tagi != null and in_array($tag, $wybrane_tagi)) {
                        echo "checked";
                    }
                    echo ">" . $tag . "<br/>";
                }
                ?>


                <input type="submit" value="zastosuj">
            </form>


            <h2 class="sub-header"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Tabela:</h2>

            <?php if (sizeof($magazyn_lista) == 0) {
                echo "W magazynie brak części spełniających podane kryteria";
            } else { ?>
                <div class="table-with-scroll">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nazwa czesci</th>
                            <th>Ilość</th>
                            <th>Usuń z magazynu</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($magazyn_lista as $czesc) {
                            echo '<tr><td><a href="modyfikacjaIPodgladCzesci.php?selected_part=' . $czesc['nazwa_elementu'] . '">' . $czesc['nazwa_elementu'] . '</a></td>';


                            ?>
                            <td>
                                <form class="change_amount">
                                    <input type="hidden" name="id_czesci" value="<?php
                                    echo $czesc['identyfikator_czesci'];
                                    ?>">

                                    <input type="hidden" class="form-control" name="stara_ilosc"
                                           value="<?php echo $czesc['ilosc_sztuk'] ?>"
                                            >

                                    <div class="input-group">
                                        <input type="number" class="form-control" name="nowa_ilosc"
                                               value="<?php echo $czesc['ilosc_sztuk'] ?>" required>

                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-check" aria-hidden="true"> </span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                            <td>


                                <form class="remove_part">
                                    <input type="hidden" name="id_czesci" value="<?php
                                    echo $czesc['identyfikator_czesci'];
                                    ?>">

                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"> </span>
                                    </button>

                                </form>

                            </td>
                            <?php

                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }
            ?>


            <h2 class="sub-header"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Listy zakupów:</h2>

            <?php if (sizeof($listy_zakupow) == 0) {
                echo "Brak list zakupów";
            } else {
                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nazwa listy</th>
                        <th>Czas utworzenia</th>
                        <th>Termin realizacji</th>
                        <th>Usuń</th>
                        <th>Wprowadź</th>
                    </tr>
                    </thead>


                    <tbody>
                    <?php
                    foreach ($listy_zakupow as $listaZakupow) {
                        ?>
                        <tr>
                            <td>
                                <a href="listaZakupow.php?nazwa_listy=<?php echo $listaZakupow['nazwa_listy_zakupow'];?>"><?php echo $listaZakupow['nazwa_listy_zakupow']; ?></a>
                            </td>

                            <td>
                                <?php echo $listaZakupow['czas_utworzenia']; ?>
                            </td>

                            <td>
                                <?php
                                    if($listaZakupow['termin_realizacji']==null){
                                        echo "brak";
                                    }
                                    else {
                                        echo $listaZakupow['termin_realizacji'];
                                    }?>
                            </td>

                            <td>
                                <form class="remove_list">
                                    <input type="hidden" name="id_listy" value="<?php
                                    echo $listaZakupow['identyfikator_listy_zakupow'];
                                    ?>">

                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
                                    </button>

                                </form>
                            </td>

                            <td>
                                <form class="accept_list">
                                    <input type="hidden" name="id_listy_zakupow" value="<?php
                                    echo $listaZakupow['identyfikator_listy_zakupow'];
                                    ?>">

                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"> </span>
                                    </button>

                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>

                </table>
                <?php
            }
            ?>

            <h2 class="sub-header"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Dodaj listę zakupów:</h2>
            <form id="add_list">

                <div class="input-group">
                    <input class="form-control" placeholder="Nazwa listy zakupow" name="nazwa_listy" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                        </button>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $("#magazynLink").addClass("active");
        $(".change_amount").submit(function (e) {
            e.preventDefault();

            var thatForm = this;
            $.ajax({
                method: "POST",
                url: "api/zmian_ilosc_czesci.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        toastr.success("Pomyślnie zmieniono ilość elementów");
                        $(thatForm).find('input[name="stara_ilosc"]').val($(thatForm).find('input[name="nowa_ilosc"]').val());
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd wprowadzania nowej ilości części");
                        $(thatForm).find('input[name="nowa_ilosc"]').val($(thatForm).find('input[name="stara_ilosc"]').val());
                    }
                },
                error: function () {
                    toastr.error("Błąd wprowadzania nowej ilości części");
                }

            })
        });

        $("#add_list").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/dodaj_liste_zakupow.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania listy zakupów ");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania listy zakupów ");
                }

            })
        });

        $(".remove_part").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_czesc_z_magazynu_by_id.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania  części");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania  części");
                }

            })
        });

        $(".remove_list").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_liste_zakupow.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania listy zakupów");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania listy zakupów");
                }

            })
        });

        $(".accept_list").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/wprowadz_liste_zakupow.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd wprowadzania listy zakupów");
                    }
                },
                error: function () {
                    toastr.error("Błąd wprowadzania listy zakupów");
                }

            })
        });
    });

</script>
</body>
</html>
