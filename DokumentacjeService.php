<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 21:38
 */

require_once "DBConnector.php";

class DokumentacjeService
{
    /**
     * Pobiera dokumentacje przypisane części o zadanym id
     * @param $id_czesci
     * @return array - każdy wiersz zawiera definicje jednej dokumentacji (tabela z kluczami 'id_dokumentacji', 'nazwa_dokumentacji' ,'link'
     * @throws Exception - błąd komunikacji z bd (KOD 200)
     */
    public static function pobierzDokumentacjeByID($id_czesci){
        $polaczenie=DBConnector::connect();

        $wynik_zapytania=pg_query_params($polaczenie,"select identyfikator_dokumentacji,nazwa_dokumentacji,link from dokumentacje where identyfikator_czesci=$1;",array($id_czesci));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $dokumentacje=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $dokumentacje[]=array(
                "id_dokumentacji"=>$row[0],
                "nazwa_dokumentacji"=>$row[1],
                "link"=>$row[2]);
        }

        DBConnector::closeConnection();
        return $dokumentacje;
    }

    public static function dodajDokumentacje($uzid,$nazwa_czesci,$nazwa_dokumentacji,$link)
    {
        $connection = DBConnector::connect();

        if (filter_var($link, FILTER_VALIDATE_URL) === FALSE) {
            throw new Exception("Błędny format linku", 201);
        }

        pg_send_query_params($connection, 'select dodaj_dokumentacje($1,$2,$3,$4);', array($uzid, $nazwa_czesci, $nazwa_dokumentacji, $link));
        $result = pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage = "Nieobsługiwany wyjątek";
            $exceptionMessage=pg_result_error($result);
            if (pg_result_error_field($result, PGSQL_DIAG_SQLSTATE) == "P0001") {
                $exceptionMessage = pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage, 202);
        }
        DBConnector::closeConnection();

    }

    /**
     * Usuwa dokumentację o zadanym ID
     * @param $uzid - identyfikator usuwającego
     * @param $id_dokumentacji
     * @throws Exception - błędy pochodzące z bazy danych - (KOD 202)
     */
    public static function usunDokumentacje($uzid,$id_dokumentacji)
    {
        $polaczenie = DBConnector::connect();

        pg_send_query_params($polaczenie, 'select usun_dokumentacje($1,$2);', array($uzid, $id_dokumentacji));
        $wynik_zapytania = pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie = "Nieobsługiwany wyjątek";
            if (pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE) == "P0001") {
                $komunikat_o_bledzie = pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie, 202);
        }
        DBConnector::closeConnection();
    }
}