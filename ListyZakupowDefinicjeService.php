<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 02.01.17
 * Time: 23:05
 */

require_once "DBConnector.php";

class ListyZakupowDefinicjeService
{
    /**
     * Pobiera elementy listy zakupów o podanym ID
     * @param $id_uzytkownika
     * @param $id_listy
     * @return array|null - tablica zawierająca wiersze o kluczach (identyfikator_czesci,ilosc_do_zamowienia,koszt,czy_koszt_jednostkowy,ilosc_sztuk_w_magazynie,nazwa_elementu)
     * @throws Exception - błąd komunikacji z bazą danych (KOD 200)
     */
    public static function pobierzElementyListyZakupow($id_uzytkownika,$id_listy){
        $connection=DBConnector::connect();
        $resultSet=pg_query_params($connection,"select identyfikator_czesci,ilosc_do_zamowienia,koszt,czy_koszt_jednostkowy,ilosc_sztuk_w_magazynie,nazwa_elementu from view_listy_zakupow_elementy WHERE id_uzytkownika=$1 AND identyfikator_listy_zakupow=$2",array($id_uzytkownika,$id_listy));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $elementyListyZakupow=null;

        while ($row=pg_fetch_row($resultSet)){
            $elementyListyZakupow[]=array(
                "identyfikator_czesci"=> $row[0],
                "ilosc_do_zamowienia" => $row[1],
                "koszt" => $row[2],
                "czy_koszt_jednostkowy" => $row[3],
                "ilosc_sztuk_w_magazynie" => $row[4],
                "nazwa_elementu" => $row[5]
            );
        }

        DBConnector::closeConnection();
        return $elementyListyZakupow;
    }

    /**
     * Dodaje wskazany element do listy zakupów
     * @param $id_uzytkownika
     * @param $id_listy_zakupow
     * @param $id_czesci
     * @param $ilosc_do_zamowiania
     * @param $koszt
     * @param $czy_kosz_jednostkowy
     * @throws Exception - błąd pochodzący z bd (KOD 202)
     */
    public static function dodajElementDoListyZakupow($id_uzytkownika,$id_listy_zakupow,$id_czesci,$ilosc_do_zamowiania,$koszt,$czy_kosz_jednostkowy){
        $polaczenie=DBConnector::connect();

        if($czy_kosz_jednostkowy){
            $czy_kosz_jednostkowy="T";
        }
        else{
            $czy_kosz_jednostkowy="F";
        }
        pg_send_query_params($polaczenie,'select dodaj_do_listy_zakupow($1,$2,$3,$4,$5,$6);',array($id_uzytkownika,$id_listy_zakupow,$id_czesci,$ilosc_do_zamowiania,$koszt,$czy_kosz_jednostkowy));
        $result=pg_get_result($polaczenie);

        if (pg_result_error($result)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Aktualizuje element listy zakupów
     * @param $id_uzytkownika
     * @param $id_listy_zakupow
     * @param $id_czesci
     * @param $ilosc_do_zamowiania
     * @param $koszt
     * @param $czy_kosz_jednostkowy
     * @throws Exception - błąd pochodzący z bd (KOD 202)
     */
    public static function zmienElementZListyZakupow($id_uzytkownika,$id_listy_zakupow,$id_czesci,$ilosc_do_zamowiania,$koszt,$czy_kosz_jednostkowy){
        $polaczenie=DBConnector::connect();

        if($czy_kosz_jednostkowy){
            $czy_kosz_jednostkowy="T";
        }
        else{
            $czy_kosz_jednostkowy="F";
        }
        pg_send_query_params($polaczenie,'select zmien_na_liscie_zakupow($1,$2,$3,$4,$5,$6);',array($id_uzytkownika,$id_listy_zakupow,$id_czesci,$ilosc_do_zamowiania,$koszt,$czy_kosz_jednostkowy));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa element o wybranym ID z listy zakupów
     * @param $id_uzytkownika
     * @param $id_listy_zakupow
     * @param $id_czesci
     * @throws Exception - błąd pochodzący z BD (KOD 202)
     */
    public static function usunElementZListyZakupow($id_uzytkownika,$id_listy_zakupow,$id_czesci){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select usun_element_z_listy_zakupow($1,$2,$3);',array($id_uzytkownika,$id_listy_zakupow,$id_czesci));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }


}


