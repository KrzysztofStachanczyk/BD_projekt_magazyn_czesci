<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 11:56
 */

require_once "DBConnector.php";

class UprawnieniaService
{
    /**
     * Usuwa zadane uprawnienie z grupy
     * @param $uzid
     * @param $nazwa_grupy
     * @param $nazwa_uprawnienia
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunUprawnienie($uzid, $nazwa_grupy, $nazwa_uprawnienia){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_uprawnienie($1,$2,$3);',array($uzid,$nazwa_grupy,$nazwa_uprawnienia));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie=null;
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }
            else{
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }

        DBConnector::closeConnection();

    }

    /**
     * Dodaje uprawnienie do grupy
     * @param $uzid
     * @param $nazwa_grupy
     * @param $nazwa_uprawnienia
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function dodajUprawnienie($uzid, $nazwa_grupy, $nazwa_uprawnienia){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_uprawnienie($1,$2,$3);',array($uzid,$nazwa_grupy,$nazwa_uprawnienia));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie=null;
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }
            else{
                $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Pobiera listę dostępnych w systemie uprawnień
     * @return array - kolejne nazwy uprawnień
     * @throws Exception - błąd komunikacji z db (kod 200)
     */
    public static function pobierzWszystkieUprawnienia(){
        $polaczenie=DBConnector::connect();

        $zapytanie="SELECT nazwa_uprawnienia FROM uprawnienia";

        $wynik_zapytania=pg_query($polaczenie,$zapytanie);

        if (!$wynik_zapytania) {
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        $nazwy_uprawnien=array();
        while($row = pg_fetch_row($wynik_zapytania)){
            $nazwy_uprawnien[]=$row[0];
        }

        DBConnector::closeConnection();

        return $nazwy_uprawnien;
    }


    public static function pobierzGrupeUzytkownika($userID){
        $connection=DBConnector::connect();
        $group=null;

        $resultSet=pg_query_params($connection,"SELECT nazwa_grupy FROM view_uzytkownicy_grupy WHERE id_uzytkownika=$1",array($userID));

        if($resultSet){
            while($row = pg_fetch_row($resultSet)){
                $group=$row[0];
            }
        }

        DBConnector::closeConnection();

        if(!$group){
            throw new Exception("Błąd pobierania danych",200);
        }

        return $group;
    }

    public static function pobierzUprawnieniaUzytkownika($userID){
        $connection=DBConnector::connect();

        pg_prepare($connection,"query","SELECT nazwa_uprawnienia FROM view_uzytkownicy_uprawnienia WHERE id_uzytkownika=$1");
        $resultSet=pg_execute($connection,"query",array($userID));

        if (!$resultSet) {
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        $resultArray=array();
        while($row = pg_fetch_row($resultSet)){
            $resultArray[]=$row[0];
        }

        DBConnector::closeConnection();

        return $resultArray;
    }

    public static function pobierzDefinicjeUprawnienDlaWszystkichUzytkownikow(){
        $connection=DBConnector::connect();

        $query="SELECT id_uzytkownika,nazwa_uprawnienia FROM view_uzytkownicy_uprawnienia";

        $resultSet=pg_query($connection,$query);

        if (!$resultSet) {
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }


        $resultMatrix=array(array());
        while($row = pg_fetch_row($resultSet)){
            $resultMatrix[$row[0]][]=$row[1];
        }

        DBConnector::closeConnection();

        return $resultMatrix;
    }

    /**
     * Pobiera liste uprawnien przynależna do grupy o podanej nazwie
     * @param $nazwa_grupy
     * @return array - kolejne nazwy uprawnień
     * @throws Exception - błąd komunikacji z bazą danych (KOD 200)
     */
    public static function pobierzUprawnieniaGrupyByName($nazwa_grupy){
        $polaczenie=DBConnector::connect();

        pg_prepare($polaczenie,"query","SELECT nazwa_uprawnienia FROM view_grupy_uprawnienia WHERE nazwa_grupy=$1");
        $wynik_zapytania=pg_execute($polaczenie,"query",array($nazwa_grupy));

        if (!$wynik_zapytania) {
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        $uprawnienia=array();
        while($row = pg_fetch_row($wynik_zapytania)){
            $uprawnienia[]=$row[0];
        }

        DBConnector::closeConnection();

        return $uprawnienia;

    }

    /**
    public static function pobierzUprawnieniaGrupyByID($groupId){
        $connection=DBConnector::connect();

        pg_prepare($connection,"query","SELECT nazwa_uprawnienia FROM view_grupy_uprawnienia WHERE identyfikator_grupy=$1");
        $resultSet=pg_execute($connection,"query",array($groupId));

        if (!$resultSet) {
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        $resultArray=array();
        while($row = pg_fetch_row($resultSet)){
            $resultArray[]=$row[0];
        }

        DBConnector::closeConnection();

        return $resultArray;
    }**/
}

?>