<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 14:58
 * CHECKED AND IMPROVED
 */

require_once ("DBConnector.php");
require_once ("UprawnieniaService.php");

/**
 * Class UzytkownicyService
 * Service odpowiedzialny za przeprowadzanie procedur logowania, wylogowywania oraz manipulacje uzytkownikami
 */
class UzytkownicyService{

    /**Przeprowadza procedure logowania uzytkownika
     * @param $login -
     * @param $haslo -
     * @throws Exception - blad logowania (kod 203) lub blad komunikacji z baza danych (kod 200)
     */
    public static function zalogujUzytkownika($login,$haslo){

        if (session_id() === '') {
            session_start();
        }

        UzytkownicyService::wyloguj();


        $polaczenie=DBConnector::connect();

        $zapytanie="SELECT login($1,$2);";

        $wynik_zapytania=pg_query_params($polaczenie,$zapytanie,array($login,$haslo));

        if (!$wynik_zapytania) {
            DBConnector::closeConnection();
            throw new Exception("Błąd logowania w komunikacji z bazą danych",200);
        }

        if($row = pg_fetch_row($wynik_zapytania)){
            if($row[0]!= null){
                $_SESSION['user_id']=$row[0];
                $_SESSION['user_name']=$login;
                $_SESSION['user_uprawnienia']=UprawnieniaService::pobierzUprawnieniaUzytkownika($_SESSION['user_id']);
                $_SESSION['user_group']=UprawnieniaService::pobierzGrupeUzytkownika($_SESSION['user_id']);
            }
            else{
                throw new Exception("Błąd logowania",203);
            }
        }

        DBConnector::closeConnection();
    }

    /**
     * Sprawdza czy uzytkownik jest zalogowany + odswieza informacje o uprawnieniach i grupie
     * @return bool
     */
    public static function czyZalogowany(){
        if (session_id() === '') {
            session_start();
        }

        if(isset($_SESSION['user_id'])){
            try {
                $_SESSION['user_uprawnienia'] = UprawnieniaService::pobierzUprawnieniaUzytkownika($_SESSION['user_id']);
                $_SESSION['user_group'] = UprawnieniaService::pobierzGrupeUzytkownika($_SESSION['user_id']);
            }catch(Exception $ex){

            }
        }
        return isset($_SESSION['user_id']);
    }

    /**
     * Wylogowuje użytkownika - jeśli nie jest zalogowany to brak akcji
     */
    public static function wyloguj(){
        if(session_id()=== ''){
            session_start();
        }
        if(UzytkownicyService::czyZalogowany()){
            unset($_SESSION['user_id']);
            unset($_SESSION['user_name']);
            unset($_SESSION['user_uprawnienia']);
            unset($_SESSION['user_group']);
        }
    }

    /**
     * Usuwa użytkownika
     * @param $uzid - id użytkownika zalogowanego w systemie (np administratora)
     * @param $login_do_usuniecia - login użytkownika do usunięcia
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunUzytkownika($uzid,$login_do_usuniecia){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_uzytkownika($1,$2);',array($uzid,$login_do_usuniecia));

        $wynik_zapytania=pg_get_result($polaczenie);


        if (pg_result_error($wynik_zapytania)) {
            $tresc_komunikatu_bledu=null;

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $tresc_komunikatu_bledu=pg_result_error($wynik_zapytania);
            }
            else{
                $tresc_komunikatu_bledu=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();

            throw new Exception($tresc_komunikatu_bledu,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Tworzy nowego użytkownika
     * @param $login
     * @param $haslo
     * @param $haslo_retype
     * @return integer - id_uzytkownika - jeśli dodawanie udało się
     * @throws Exception -błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych lub walidacji po stronie serwera (kod 201)
     *
     */
    public static function utworzUzytkowanika($login, $haslo, $haslo_retype){

        $login=utf8_encode($login);
        $haslo=utf8_encode($haslo);
        $haslo_retype=utf8_encode($haslo_retype);

        // walidacja poprawnosci po stronie serwera PHP
        if(preg_match('/[^A-Za-z0-9]/', $haslo)){
            throw new Exception("Hasło zawiera niedozwolone znaki",201);
        }

        if(preg_match('/[^A-Za-z0-9]/', $login)){
            throw new Exception("Login zawiera niedozwolone znaki",201);
        }

        if(strlen($login)<3 || strlen($login)>50){
            throw new Exception("Login ma niepoprawną długość",201);
        }

        if(strlen($haslo)<5 || strlen($haslo)>20){
            throw new Exception("Hasło ma błędną długość",201);
        }

        if($haslo_retype!==$haslo){
            throw new Exception("Hasła nie są zgodne",201);
        }




        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select utworz_uzytkownika($1,$2);',array($login,$haslo));

        $wynik_zapytania=pg_get_result($polaczenie);


        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie=null;

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }
            else{
                $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            }
            DBConnector::closeConnection();

            throw new Exception($komunikat_o_bledzie,202);
        }

        $id_uzytkownika=null;
        if($row = pg_fetch_row($wynik_zapytania)){
            $id_uzytkownika=$row[0];
        }

        DBConnector::closeConnection();
        return $id_uzytkownika;
    }

}

