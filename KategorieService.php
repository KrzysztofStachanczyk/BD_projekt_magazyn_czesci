<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 22:39
 */

require_once "DBConnector.php";
class KategorieService
{
    /**
     * Dodaje nową kategorie
     * @param $uzid
     * @param $nazwa_kategori
     * @throws Exception -  błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function dodajKategorie($uzid, $nazwa_kategori){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_kategorie($1,$2);',array($uzid,$nazwa_kategori));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledach="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledach=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledach,202);
        }
        DBConnector::closeConnection();

    }

    /**
     * Zmienia nazwę kategorii
     * @param $uzid
     * @param $nazwa_kategori
     * @param $nowa_nazwa_kategori
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function zmienNazweKategori($uzid, $nazwa_kategori, $nowa_nazwa_kategori){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select zmien_nazwe_kategori($1,$2,$3);',array($uzid,$nazwa_kategori,$nowa_nazwa_kategori));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa kategorie o podanej nazwie
     * @param $uzid
     * @param $nazwa_kategori
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunKategorie($uzid, $nazwa_kategori){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_kategorie($1,$2);',array($uzid,$nazwa_kategori));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledach="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledach=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledach,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Pobiera informacje o kategoriach
     * @return array - każdy wiersz zawiera klucze ('nazwa_kategori','ilosc_czesci')
     * @throws Exception - błąd komunikacji z bazą danych (KOD 200)
     */
    public static function pobierzKategorie(){
        $polaczenie=DBConnector::connect();
        $wynik_zapytania=pg_query($polaczenie,"select nazwa_kategori,ilosc_czesci from view_kategorie_raport ORDER BY nazwa_kategori;");

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $kategorie=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $kategorie[]=array("nazwa_kategori"=> $row[0],"ilosc_czesci"=>$row[1]);
        }

        DBConnector::closeConnection();
        return $kategorie;
    }
}