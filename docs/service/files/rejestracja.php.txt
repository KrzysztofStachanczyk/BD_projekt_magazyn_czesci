<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 21:19
 *
 * Nu Html Checker - OK
 */
require_once "UzytkownicyService.php";

if(UzytkownicyService::czyZalogowany()){
    header("Location: index.php");
    die();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Rejestracja</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />

    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



    <link rel="stylesheet" href="login_styles.css">
</head>

<body>

<div class="container">
    <div class="login-container">
        <form class="form-signin" id="registerForm">
            <h2 class="form-signin-heading">Rejestracja</h2>

            <div class="form-group">
                <label for="login">Login:</label>
                <input id="login" type="text" name="login" class="form-control" placeholder="Login" required autofocus>
            </div>

            <div class="form-group">
                <label for="haslo">Hasło:</label>
                <input id="haslo" type="password" name="haslo" class="form-control" placeholder="Hasło" required >
            </div>


            <div class="form-group">
                <label for="haslo_retype">Hasło retype:</label>
                <input id="haslo_retype" type="password" name="haslo_retype" class="form-control" placeholder="Hasło retype" required >
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Zarejestruj</button>

        </form>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#registerForm").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_uzytkownika.php",
                data: $(this).serialize(),
                success: function(json){
                    if(json['status']){
                        window.location.href="index.php";
                    }
                    else{
                        toastr.error(json['komunikat_o_bledzie'].replace("ERROR:", ""));
                    }

                },
                error: function () {
                    toastr.error("Błąd rejestracji");
                }
            });
        });
    });
</script>
</body>
</html>

