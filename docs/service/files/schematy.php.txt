<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 07.01.17
 * Time: 20:28
 */

require_once 'DBConnector.php';
require_once "UzytkownicyService.php";
require_once "DashBoardRendererService.php";
require_once "CzesciMagazynService.php";
require_once "KategorieService.php";
require_once "TagiService.php";
require_once "ListyZakupowService.php";
require_once "SchematyService.php";
// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];
$lista_schematow_uzytkownika=null;
$lista_schematow_publicznych=null;

try {
    $lista_schematow_publicznych=SchematyService::pobierzListeSchematowPublicznych();
    $lista_schematow_uzytkownika=SchematyService::pobierzListeSchematowUzytkownika($_SESSION['user_id']);
} catch (Exception $e) {
    die();
}

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Schematy</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


            <h1 class="page-header"><span class="glyphicon glyphicon-book"></span> Schematy</h1>
            <p>Tutaj możesz oglądać i współtworzyć schematy - czyli zestawy części służące do budowy popularnych układów  </p>


            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Twoje schematy:</h2>

            <?php if (sizeof($lista_schematow_uzytkownika) == 0) {
                echo "Brak schematów użytkownika";
            } else {
                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nazwa schematu</th>
                        <th>Ilość różnych elementów</th>
                        <th>Łączna ilość części</th>
                        <th>Usuń</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($lista_schematow_uzytkownika as $schemat) {
                        ?>
                        <tr>
                            <td>
                                <a href="schemat.php?nazwa_schematu=<?php echo $schemat['nazwa_schematu'];?>"><?php echo $schemat['nazwa_schematu']; ?></a>
                            </td>

                            <td>
                                <?php echo $schemat['roznych_elementow']; ?>
                            </td>

                            <td>
                                <?php echo $schemat['wszystkich_czesci']; ?>
                            </td>


                            <td>
                                <form class="remove_schema">
                                    <input type="hidden" name="id_schematu" value="<?php
                                    echo $schemat['identyfikator_schematu'];
                                    ?>">

                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
                                    </button>

                                </form>
                            </td>

                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>

                </table>
                <?php
            }
            ?>

            <?php if(in_array('TWORZENIE_SCHEMATOW',$uprawnienia_uzytkownika)){ ?>
            <h2 class="sub-header"><span class="glyphicon glyphicon-plus"></span> Dodaj schemat:</h2>
            <form id="add_schema">

                <div class="form-group">
                    <label for="schema_name">Nazwa schematu</label>
                    <input class="form-control" id="schema_name" placeholder="Nazwa schematu" name="nazwa_schematu" required>
                </div>

                <div class="form-group">
                    <label for="schema_desc">Opis</label>
                    <textarea class="form-control" id="schema_desc" name="schemat_opis">

                    </textarea>
                </div>

                <label for="schema_public_checkbox">Widoczny publicznie</label>
                <input  id="schema_public_checkbox" type="checkbox" name="schemat_publiczny">
                <br />
                <button type="submit" class="btn btn-default " aria-label="Left Align">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                </button>
            </form>
            <?php } ?>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Schematy widoczne publicznie:</h2>

            <?php if (sizeof($lista_schematow_publicznych) == 0) {
                echo "Brak schematów publicznych";
            } else {
                ?>
                <div class="table-with-scroll">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nazwa schematu</th>
                        <th>Ilość  różnych elementów</th>
                        <th>Łączna ilość części</th>
                        <?php
                        if(in_array("USUWANIE_OBCYCH_SCHEMATOW",$uprawnienia_uzytkownika)){
                            echo "<th>Usuń</th>";
                        }
                        ?>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($lista_schematow_publicznych as $schemat) {
                        ?>
                        <tr>
                            <td>
                                <a href="schemat.php?nazwa_schematu=<?php echo $schemat['nazwa_schematu'];?>"><?php echo $schemat['nazwa_schematu']; ?></a>
                            </td>

                            <td>
                                <?php echo $schemat['roznych_elementow']; ?>
                            </td>

                            <td>
                                <?php echo $schemat['wszystkich_czesci']; ?>
                            </td>


                            <?php
                            if(in_array("USUWANIE_OBCYCH_SCHEMATOW",$uprawnienia_uzytkownika)) {
                                ?>
                                <td>
                                    <form class="remove_schema">
                                        <input type="hidden" name="id_schematu" value="<?php
                                        echo $schemat['identyfikator_schematu'];
                                        ?>">

                                        <button type="submit" class="btn btn-default" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
                                        </button>

                                    </form>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>

                </table>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $("#schematLink").addClass("active");
        $("#add_schema").submit(function (e) {
            e.preventDefault();

            var thatForm = this;
            $.ajax({
                method: "POST",
                url: "api/dodaj_schemat.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                       location.reload();
                    }
                    else {
                        toastr.error(json['komunikat_o_bledzie'].replace("ERROR:", ""), "Błąd tworzenia schematu");
                    }
                },
                error: function () {
                    toastr.error("Błąd tworzenia schematu");
                }

            })
        });

        $(".remove_schema").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_schemat.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        toastr.error(json['komunikat_o_bledzie'].replace("ERROR:", ""), "Błąd usuwania schematu ");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania schematu ");
                }

            })
        });

        <?php
        if(in_array("USUWANIE_OBCYCH_SCHEMATOW",$uprawnienia_uzytkownika)){ ?>
        $(".remove_schema_as_moderator").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_schemat_jako_moderator.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        toastr.error(json['komunikat_o_bledzie'].replace("ERROR:", ""), "Błąd usuwania schematu ");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania schematu ");
                }

            })
        });
        <?php
        }
        ?>


    });

</script>
</body>
</html>
