<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 08.01.17
 * Time: 22:30
 */


require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "DashBoardRendererService.php";
require_once "KomentarzeService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];

if (!in_array("WERYFIKACJA_KOMENTARZY", $uprawnienia_uzytkownika)) {
    header("Location: index.php");
    die();
}

$komentarze_do_weryfikacji=null;
try {
    $komentarze_do_weryfikacji = KomentarzeService::pobierzKomentarzeDoWeryfikacji();
} catch (Exception $e) {
    die();
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weryfikacja komentarzy</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header"><span class="glyphicon glyphicon-question-sign" ></span> Weryfikacja komentarzy</h1>
            <p>Tutaj możesz przeprowadzić sprawdzenie komentarzy, które zostały oznaczone do weryfikacji.</p>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Lista komentarzy</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa użytkownika</th>
                        <th>Komentarz</th>
                        <th>Schemat</th>

                        <th>
                            Akceptuj/Usuń
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($komentarze_do_weryfikacji as $komentarz) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $komentarz['login'];?>
                            </td>
                            <td>
                                <p>
                                    <?php echo $komentarz['tresc'];?>
                                </p>
                            </td>
                            <td>
                                <a href="<?php echo "schemat.php?nazwa_schematu=".$komentarz['nazwa_schematu']; ?>">
                                    <?php echo $komentarz['nazwa_schematu']; ?>
                                </a>
                            </td>
                            <td>
                                <form method="post" >
                                    <input type="hidden" name="id_komentarza" value="<?php echo $komentarz['identyfikator_komentarza']; ?>">
                                    <button  class="btn btn-default accept-comment-button" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </button>
                                    <button  class="btn btn-default remove-comment-button" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                                </form>

                            </td>
                        </tr>

                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#commentsCheckLink").addClass("active");
        $(".accept-comment-button").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/cofnij_komentarz_z_weryfikacji.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania uzytkownika");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.error("Błąd usuwania użytkownika");
                }

            })
        });

        $(".remove-comment-button").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_komentarz_schematu.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania uzytkownika");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.error("Błąd usuwania użytkownika");
                }

            })
        });

    });


</script>
</body>
</html>
