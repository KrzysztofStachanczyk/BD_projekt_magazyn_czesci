<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 11:04
 *
 * Nu Html Checker - OK
 */

require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "DashBoardRendererService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];

if (!in_array("EDYCJA_GRUP_UPRAWNIEN", $uprawnienia_uzytkownika)) {
    header("Location: index.php");
    die();
}

$grupa_uzytkownika = $_SESSION['user_group'];

$grupy = null;
try {
    $grupy = GrupyUprawnienService::pobierzListeGrup();

} catch (Exception $e) {
    die();
}

$uprawnienia_wybranej_grupy = null;
$uprawnienia = null;

if (isset($_GET['wybrana_grupa'])) {
    $match = false;
    foreach ($grupy as $grupa) {
        if ($grupa['nazwa_grupy'] == $_GET['wybrana_grupa']) {
            $match = true;
            break;
        }
    }
    if ($match) {
        try {
            $uprawnienia = UprawnieniaService::pobierzWszystkieUprawnienia();
            $uprawnienia_wybranej_grupy = UprawnieniaService::pobierzUprawnieniaGrupyByName($_GET['wybrana_grupa']);
        } catch (Exception $e) {
            die();
        }
    } else {
        unset($_GET['wybrana_grupa']);
    }
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edycja grup uprawnień</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"><span class="glyphicon glyphicon-cog"></span> Edycja grup uprawnień</h1>
            <p>Tutaj możesz zarządzać grupami uprawnień użytkowników strony. <strong> Uwaga !!!</strong> Niektóre zmiany
                mogą pozbawić Cię praw do ich cofnięcia.</p>


            <h2 class="sub-header"><span class="glyphicon glyphicon-map-marker"></span> Twoja grupa</h2>
            <p> Nazwa: <?php echo $grupa_uzytkownika; ?>

            </p>
            Uprawnienia:
            <ul>
                <?php
                foreach ($uprawnienia_uzytkownika as $uprawnienie) {
                    echo '<li>' . $uprawnienie . '</li>';
                }
                ?>
            </ul>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Grupy</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>Ilość członków</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($grupy as $grupa) {
                        echo '<tr><td>' . $grupa['nazwa_grupy'] . '</td><td>' . $grupa['ilosc_uzytkownikow'] . '</td><td>';

                        if ($grupa['ilosc_uzytkownikow'] == 0 or true) {
                            ?>
                            <form method="post" class="remove_group">
                                <input type="hidden" name="nazwa_grupy" value="<?php echo $grupa['nazwa_grupy']; ?>">
                                <button type="submit" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>
                            <?php
                        }

                        echo '</td></tr>';
                    }

                    ?>
                    <tr>
                        <td colspan="2">

                        <form method="post" id="add_group">


                            <div class="input-group" style="width: 100%;">
                                <input class="form-control" placeholder="Nazwa grupy" name="nazwa_grupy" required>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                                    </button>
                                </div>
                            </div>


                        </form>
                    </td>
                    <td></td>


                    </tr>
                    </tbody>
                </table>
            </div>

            <h2 class="sub-header"><span class="glyphicon glyphicon-eye-open" ></span> Uprawnienia</h2>

            <form method="get" action="edycjaGrup.php" id="group_select_form">
                <div class="form-group">
                    <label for="group_select_combo">Nazwa grupy:</label>
                    <select class="form-control" id="group_select_combo" name="wybrana_grupa">

                        <?php
                        foreach ($grupy as $grupa) {
                            echo '<option value="' . $grupa['nazwa_grupy'] . '" ';
                            if (isset($_GET['wybrana_grupa']) and $_GET['wybrana_grupa'] == $grupa['nazwa_grupy']) {
                                echo 'selected="selected"';
                            }
                            echo '>' . $grupa['nazwa_grupy'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <input type="submit" class="form-control" value="pobierz">
            </form>

            <?php
            if (isset($_GET['wybrana_grupa'])) {
                ?>
                <div class="table-responsive" id="right_table">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th> Nazwa uprawnienia</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($uprawnienia as $uprawnienie) {
                            echo "<tr><td>" . $uprawnienie . "</td><td>";
                                ?>
                                <form>
                                    <input type="hidden" name="nazwa_grupy" value="<?php echo $_GET['wybrana_grupa']; ?>">
                                    <input type="hidden" name="nazwa_uprawnienia" value="<?php echo $uprawnienie; ?>">

                                    <button type="submit" class="btn btn-default remove_right"  aria-label="Left Align"
                                            <?php if(!in_array($uprawnienie, $uprawnienia_wybranej_grupy)) {
                                                echo "style='display:none;'";
                                            }?>
                                    >
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                    </button>

                                    <button type="submit" class="btn btn-default add_right" aria-label="Left Align"
                                        <?php if(in_array($uprawnienie, $uprawnienia_wybranej_grupy)) {
                                            echo "style='display:none;'";
                                        }?>

                                    >
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                </form>
                                <?php
                            echo "</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#groupEditLink").addClass("active");
        $(".remove_right").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_uprawnienia.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }

                        toastr.error(error_string, "Błąd usuwania uprawnienia");
                    }

                },
                error: function () {
                    toastr.error("Błąd usuwania uprawnienia");
                }
            });

        });


        $(".add_right").click(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_uprawnienia.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }

                        toastr.error(error_string, "Błąd dodawania uprawnienia");
                    }

                },
                error: function () {
                    toastr.error("Błąd dodawania uprawnienia");
                }
            });

        });


        $("#group_select_combo").change(function () {
            $("#group_select_form").submit();
        });

        $('#add_group').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/dodaj_grupe_uprawnien.php",
                data: $("#add_group").serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }

                        toastr.error(error_string, "Błąd dodawania grupy");
                    }

                },
                error: function () {
                    toastr.error("Błąd dodawania grupy");
                }
            });
        });

        $('.remove_group').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_grupe_uprawnien.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania grupy");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania grupy");
                }
            });
        });
    });
</script>
</body>
</html>

