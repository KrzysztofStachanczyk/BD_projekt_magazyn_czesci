<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 17:21
 */

require_once "DBConnector.php";

class CzesciMagazynService
{
    public static  function  pobierzIloscCzesciWMagazynie($id_uzytkownika,$id_czesci){
        $connection=DBConnector::connect();
        $resultSet=pg_query_params($connection,"select ilosc_sztuk from czesci_elektroniczne_magazyn  WHERE id_uzytkownika=$1 AND identyfikator_czesci=$2",array($id_uzytkownika,$id_czesci));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $ilosc=null;

        if ($row=pg_fetch_row($resultSet)){
            $ilosc= $row[0];
        }

        DBConnector::closeConnection();
        return $ilosc;
    }

    public static function zmienIloscCzesci($idUzytkownika,$idCzesci,$nowaIlosc){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select zmien_ilosc_czesci($1,$2,$3);',array($idUzytkownika,$idCzesci,$nowaIlosc));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Pobiera liste pozycji w magazynie na podstawie filtrów
     * @param $id_uzytkownika
     * @param $nazwa_kategorii
     * @param $lista_tagow
     * @return array - lista zawierająca wiersze o indeksach "identyfikator_czesci","nazwa_elementu","nazwa_kategori","ilosc_sztuk"
     * @throws Exception - blad komunikacji z bd
     */
    public static function pobierzListePozycjiWMagazynie($id_uzytkownika, $nazwa_kategorii, $lista_tagow){
        $polaczenie=DBConnector::connect();

        $zapytanie="select DISTINCT identyfikator_czesci,nazwa_elementu, nazwa_kategori , ilosc_sztuk  from view_czesci_magazyn_tagi ";
        $indexParametru=1;
        $parametry=array();

            $zapytanie=$zapytanie."WHERE ";

        if ($nazwa_kategorii!=null){
            $parametry[]=$nazwa_kategorii;
            $zapytanie=$zapytanie." nazwa_kategori=$".$indexParametru." ";
            $indexParametru=$indexParametru+1;
        }

        if($nazwa_kategorii!=null and $lista_tagow!=null){
            $zapytanie=$zapytanie . " AND ";
        }

        if($lista_tagow!=null) {
            $pierwszyTag = true;
            $zapytanie=$zapytanie . " ( ";
            foreach ($lista_tagow as $tag) {
                if($pierwszyTag==false){
                    $zapytanie=$zapytanie . " OR ";
                }
                $pierwszyTag=False;
                $zapytanie=$zapytanie." nazwa_tagu=$".$indexParametru;
                $indexParametru=$indexParametru+1;
                $parametry[]=$tag;
            }
            $zapytanie=$zapytanie." ) ";
        }

        if($nazwa_kategorii!=null or $lista_tagow!=null){
            $zapytanie=$zapytanie." AND  ";
        }

        $zapytanie=$zapytanie." id_uzytkownika=$".$indexParametru;
        $parametry[]=$id_uzytkownika;

        $zapytanie=$zapytanie."  ORDER BY nazwa_elementu ";
        $zapytanie=$zapytanie." ;";

        $wynik_zapytania=pg_query_params($polaczenie,$zapytanie,$parametry);

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $czesci=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $czesci[]=array(
                "identyfikator_czesci" => $row[0],
                "nazwa_elementu" => $row[1],
                "nazwa_kategori" => $row[2],
                "ilosc_sztuk" => $row[3]);
        }

        DBConnector::closeConnection();
        return $czesci;
    }

    /**
     * Zwraca nazwy części znajdujących się w magazynie użytkownika
     * @param $id_uzytkownika
     * @return array
     * @throws Exception - błąd komunikacji z bazą danych (kod 200)
     */
    public static function pobierzNazwyCzesciUzytkownika($id_uzytkownika){
        $polaczenie=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenie,"select nazwa_elementu from view_nazwa_magazyn_id_uzytkownika WHERE id_uzytkownika=$1",array($id_uzytkownika));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $czesci=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $czesci[]=$row[0];
        }

        DBConnector::closeConnection();
        return $czesci;
    }

    public static function pobierzListeCzesciUzytkownika($id_uzytkownika){
        $connection=DBConnector::connect();
        $resultSet=pg_query_params($connection,"select identyfikator_czesci,nazwa_elementu from view_nazwa_magazyn_id_uzytkownika WHERE id_uzytkownika=$1",array($id_uzytkownika));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $czesci=array();

        while ($row=pg_fetch_row($resultSet)){
            $czesci[]=array('identyfikator_czesci'=>$row[0],'nazwa_elementu'=>$row[1]);
        }

        DBConnector::closeConnection();
        return $czesci;
    }

    public static function usunCzescZMagazynuById($id_uzytkownika,$id_czesci){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select usun_czesc_z_magazynu_by_id($1,$2);',array($id_uzytkownika,$id_czesci));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Usuwa część o podanej nazwie z magazynu
     * @param $id_uzytkownika
     * @param $nazwa_czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunCzescZMagazynu($id_uzytkownika,$nazwa_czesci){
        $connection=DBConnector::connect();

        pg_send_query_params($connection,'select usun_czesc_z_magazynu($1,$2);',array($id_uzytkownika,$nazwa_czesci));
        $result=pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage="Nieobsługiwany wyjątek";
            if(pg_result_error_field($result, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $exceptionMessage=pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Dodaje część o podanej nazwie do magazynu
     * @param $id_uzytkownika
     * @param $nazwa_czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function dodajCzescDoMagazynu($id_uzytkownika,$nazwa_czesci){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_czesc_do_magazynu($1,$2);',array($id_uzytkownika,$nazwa_czesci));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }
}