<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 07.01.17
 * Time: 21:55
 */


require_once 'DBConnector.php';
require_once "UzytkownicyService.php";
require_once "DashBoardRendererService.php";
require_once "CzesciMagazynService.php";
require_once "KategorieService.php";
require_once "TagiService.php";
require_once "ListyZakupowService.php";
require_once "SchematyService.php";
require_once "CzesciService.php";
require_once "KomentarzeService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}


$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];
$schemat = null;
$nazwa_schematu = null;
$lista_czesci = null;
$dostepne_czesci = null;
$komentarze = null;

if (isset($_GET['nazwa_schematu'])) {
    $nazwa_schematu = $_GET['nazwa_schematu'];
}

try {
    $schemat = SchematyService::pobierzSchematByName($_SESSION['user_id'], $nazwa_schematu);
    $lista_czesci = SchematyService::pobierzCzesciSchematuById($schemat['identyfikator_schematu']);
    $dostepne_czesci = CzesciService::pobierzCzesci(null, null, null);
    $komentarze = KomentarzeService::pobierzKomentarzeSchematu($schemat['identyfikator_schematu']);
} catch (Exception $e) {
    die();
}

if ($schemat == null) {
    die();
}

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Schematy</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


            <h1 class="page-header"><span class="glyphicon glyphicon-zoom-in" ></span> Schemat szczegóły</h1>
            <?php if ($schemat['id_uzytkownika'] != $_SESSION['user_id']) {
                ?>
                <ul>
                    <li><strong>Nazwa:</strong><?php echo $schemat['nazwa_schematu']; ?></li>
                    <li><strong>Opis:</strong>
                        <p><?php echo $schemat['szczegolowy_opis']; ?></p></li>
                </ul>
                <?php
            } else { ?>
                <form id="modify-schematic">
                    <input type="hidden" value="<?php echo $schemat['identyfikator_schematu']; ?>" name="id_schematu">
                    <strong>Nazwa: </strong><?php echo $schemat['nazwa_schematu']; ?><br/>
                    <strong>Opis: </strong> <br/> <textarea class="form-control" rows="7"
                                                            name="schemat_opis"><?php echo $schemat['szczegolowy_opis']; ?></textarea>
                    <strong>Widoczny publicznie:</strong> <input type="checkbox"
                                                                 name="widoczny_publicznie" <?php if ($schemat['widoczny_publicznie'] == 't') {
                        echo 'checked';
                    } ?>>
                    <br/>

                    <button type="submit" class="btn btn-default " aria-label="Left Align">
                        <span class="glyphicon glyphicon-save" aria-hidden="true"> Zapisz </span>
                    </button>
                </form>
                <?php
            } ?>
            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Lista potrzebnych części</h2>
            <?php if (sizeof($lista_czesci) == 0) {
                echo "Brak części";
            } else { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th> Nazwa</th>
                        <th> Ilość</th>
                        <?php
                        if ($schemat['id_uzytkownika'] == $_SESSION['user_id'] and in_array("MODYFIKACJA_SCHEMATOW", $uprawnienia_uzytkownika)) {
                            echo "<th>Usuń</th>";
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($lista_czesci as $czesc) {
                        ?>
                        <tr>
                            <td><?php echo $czesc['nazwa_elementu']; ?></td>
                            <td><?php echo $czesc['wymagana_ilosc']; ?></td>
                            <?php
                            if ($schemat['id_uzytkownika'] == $_SESSION['user_id'] and in_array("MODYFIKACJA_SCHEMATOW", $uprawnienia_uzytkownika)) {
                                ?>
                                <td>
                                    <form class="remove_part">
                                        <input type="hidden" name="id_schematu" value="<?php
                                        echo $schemat['identyfikator_schematu'];
                                        ?>">

                                        <input type="hidden" name="id_czesci" value="<?php
                                        echo $czesc['identyfikator_czesci'];
                                        ?>">

                                        <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                    } ?>
                    </tbody>

                </table>
                <?php
            }
            ?>

            <?php if (in_array("MODYFIKACJA_SCHEMATOW", $uprawnienia_uzytkownika)) { ?>
                <h2 class="sub-header"><span class="glyphicon glyphicon-plus"></span> Dodaj część do schematu</h2>
                <form id="add_part">

                    <input type="hidden" name="id_schematu" value="<?php
                    echo $schemat['identyfikator_schematu'];
                    ?>">

                    <div class="form-group">
                        <label for="id_czesci">Część </label>
                        <select class="form-control" id="id_czesci" name="id_czesci">
                            <?php
                            foreach ($dostepne_czesci as $czesc_do_wstawienia) {
                                if ($czesc_do_wstawienia['zablokowana'] != 't') {
                                    echo "<option value='" . $czesc_do_wstawienia['identyfikator_czesci'] . "'>" . $czesc_do_wstawienia['id_czesci'] . $czesc_do_wstawienia['nazwa_elementu'] . "</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ilosc_czesci">Ilość</label>
                        <input type="number" class="form-control" id="ilosc_czesci" name="ilosc_czesci" required>
                    </div>


                    <button type="submit" class="btn btn-default " aria-label="Left Align">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                    </button>

                </form>
            <?php } ?>


            <?php if (in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia_uzytkownika)) { ?>
                <h2 class="sub-header"><span class="glyphicon glyphicon-import"></span> Wygeneruj listę zakupów na podstawie schematu</h2>
                <form id="create_list">
                    <input type="hidden" name="id_schematu" value="<?php
                    echo $schemat['identyfikator_schematu'];
                    ?>">

                    <div class="form-group">
                        <label for="nazwa_listy">Nazwa listy</label>
                        <input type="text" class="form-control" id="nazwa_listy" name="nazwa_listy" required>
                    </div>

                    <button type="submit" class="btn btn-default " aria-label="Left Align">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"> Wykonaj </span>
                    </button>
                </form>
            <?php } ?>


            <h2 class="sub-header"><span class="glyphicon glyphicon-comment"></span> Komentarze:</h2>

            <?php if (sizeof($komentarze) == 0) {
                echo "Brak komentarzy";
            } else {
                foreach ($komentarze as $komentarz) {
                    ?>

                    <div class="comment">
                        <div class="comment-head">
                            <?php
                            if (in_array("PRZEKAZYWANIE_KOMENTARZA_DO_SPRAWDZENIA", $uprawnienia_uzytkownika)) {
                                if ($komentarz['zgloszony_do_sprawdzenia']=='t') {
                                    echo $komentarz['login'];
                                    ?>
                                    <span class="glyphicon glyphicon-flash" aria-hidden="true">  </span>
                                    <?php
                                } else {
                                    ?>

                                    <form class="add-to-comment-to-check">
                                        <?php echo $komentarz['login']; ?>
                                        <input type="hidden"
                                               value="<?php echo $komentarz['identyfikator_komentarza']; ?>"
                                               name="id_komentarza">
                                        <button type="submit" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-flash" aria-hidden="true">  </span>
                                        </button>
                                    </form>

                                    <?php
                                }
                            } else {
                                echo $komentarz['login'];
                            }
                            ?>

                        </div>
                        <div class="comment-body">
                            <?php echo $komentarz['tresc']; ?>
                        </div>
                    </div>

                    <?php
                }
            }

            if (in_array("KOMENTOWANIE_SCHEMATU", $uprawnienia_uzytkownika)) {
                ?>
                <form id="add-comment">
                    <input type="hidden" name="id_schematu" value="<?php
                    echo $schemat['identyfikator_schematu'];
                    ?>">
                    <div class="comment">
                        <div class="comment-head">
                            <?php echo $_SESSION['user_name']; ?>
                        </div>
                        <div class="comment-body">
                            <textarea class="form-control" name="tresc_komentarza" maxlength="500" rows="5" required>


                            </textarea>
                            <button type="submit" class="btn btn-default " aria-label="Left Align">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                            </button>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>


        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $("#schematLink").addClass("active");
        $("#modify-schematic").submit(function (e) {
            e.preventDefault();

            var thatForm = this;
            $.ajax({
                method: "POST",
                url: "api/zmien_schemat.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd zmiany schematu");
                    }
                },
                error: function () {
                    toastr.error("Błąd zmiany schematu");
                }

            })
        });

        <?php
        if ($schemat['id_uzytkownika'] == $_SESSION['user_id'] and in_array("MODYFIKACJA_SCHEMATOW", $uprawnienia_uzytkownika)) {
        ?>
        $(".remove_part").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/usun_czesc_z_schematu.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania części z schematu ");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania części z schematu ");
                }

            })
        });

        $("#add_part").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/dodaj_czesc_do_schematu.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania części do schematu ");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania części do schematu ");
                }

            })
        });


        <?php } ?>

        <?php
        if (in_array("KOMENTOWANIE_SCHEMATU", $uprawnienia_uzytkownika)) {
        ?>
        $("#add-comment").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/dodaj_komentarz_do_schematu.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania komentarza ");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania komentarza ");
                }

            })
        });
        <?php
        }
        ?>


        <?php
        if(in_array("PRZEKAZYWANIE_KOMENTARZA_DO_SPRAWDZENIA", $uprawnienia_uzytkownika)){
        ?>
        $(".add-to-comment-to-check").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/przekaz_komentarz_do_werefikacji.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd zgłaszania komentarza do weryfikacji ");
                    }
                },
                error: function () {
                    toastr.error("Błąd zgłaszania komentarza do weryfikacji");
                }

            })
        });
        <?php
        }?>

        <?php if (in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia_uzytkownika)) { ?>

        $("#create_list").submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/utworz_liste_zakupow_na_podstawie_schematu.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        toastr.success("Pomyślnie wygenerowano listę zakupów");
                    }
                    else {
                        var string_to_show=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        string_to_show=string_to_show.substring(0,string_to_show.indexOf("CONTEXT"));
                        toastr.error(string_to_show, "Błąd generowania listy zakupów ");
                    }
                },
                error: function () {
                    toastr.error("Błąd generowania listy zakupów");
                }

            })
        });

        <?php } ?>

    });

</script>
</body>
</html>