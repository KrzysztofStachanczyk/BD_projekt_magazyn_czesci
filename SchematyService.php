<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 07.01.17
 * Time: 19:10
 */

require_once "DBConnector.php";

class SchematyService
{
    /**
     * Pobiera listę schematów należących do użytkownika
     * @param $id_uzytkownika
     * @return array - lista zawierająca wiersze z kluczami (identyfikator_schematu,nazwa_schematu,roznych_elementow,wszystkich_czesci)
     * @throws Exception
     */
    public static function pobierzListeSchematowUzytkownika($id_uzytkownika){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenia,"select identyfikator_schematu,nazwa_schematu,roznych_elementow,wszystkich_czesci from view_schematy_raport where id_uzytkownika=$1;",array($id_uzytkownika));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $lista_schematow=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $lista_schematow[]=array("identyfikator_schematu"=> $row[0], "nazwa_schematu" => $row[1],"roznych_elementow" => $row[2], "wszystkich_czesci" => $row[3]);
        }

        DBConnector::closeConnection();
        return $lista_schematow;
    }


    /**
     * Pobiera schemat na podstawie nazwy
     * @param $id_uzytkownika
     * @param $nazwa_schematu
     * @return array|null - tablica z kluczami (identyfikator_schematu,id_uzytkownika,nazwa_schematu,szczegolowy_opis,widoczny_publicznie)
     * @throws Exception
     */
    public static function pobierzSchematByName($id_uzytkownika,$nazwa_schematu){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenia,"select identyfikator_schematu,id_uzytkownika,nazwa_schematu,szczegulowy_opis,widoczny_publicznie from schematy where (id_uzytkownika=$1 or widoczny_publicznie=TRUE) and nazwa_schematu=$2; ",array($id_uzytkownika,$nazwa_schematu));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $schemat=null;

        if ($row=pg_fetch_row($wynik_zapytania)){
            $schemat=array("identyfikator_schematu"=> $row[0], "id_uzytkownika" => $row[1],"nazwa_schematu" => $row[2], "szczegolowy_opis" => $row[3],"widoczny_publicznie"=>$row[4]);
        }

        DBConnector::closeConnection();
        return $schemat;
    }


    /**
     * Pobiera listę schematów widocznych publicznie
     * @return array - lista zawierająca wiersze z kluczami
     * @throws Exception
     */
    public static function pobierzListeSchematowPublicznych(){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query($polaczenia,"select identyfikator_schematu,nazwa_schematu,roznych_elementow,wszystkich_czesci from view_schematy_raport where widoczny_publicznie=TRUE;");


        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $lista_schematow=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $lista_schematow[]=array("identyfikator_schematu"=> $row[0], "nazwa_schematu" => $row[1],"roznych_elementow" => $row[2], "wszystkich_czesci" => $row[3]);
        }

        DBConnector::closeConnection();
        return $lista_schematow;
    }

    /**
     * Dodaje schemat do bazy danych
     * @param $id_uzytkownika
     * @param $nazwa
     * @param $opis
     * @param $widocznosc
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function dodajSchemat($id_uzytkownika,$nazwa,$opis,$widocznosc){
        $polaczenie=DBConnector::connect();

        if($widocznosc){
            $widocznosc="T";
        }else{
            $widocznosc="F";
        }

        pg_send_query_params($polaczenie,'select dodaj_schemat($1, $2,$3,$4);',array($id_uzytkownika,$nazwa,$opis,$widocznosc));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Usuwa schemat o podanym ID
     * @param $id_uzytkownika
     * @param $id_schematu
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function usunSchemat($id_uzytkownika,$id_schematu){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_schemat($1, $2);',array($id_uzytkownika,$id_schematu));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa schemat o podanym ID jako moderator
     * @param $id_uzytkownika
     * @param $id_schematu
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function usunSchematJakoModerator($id_uzytkownika,$id_schematu){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_schemat_jako_moderator($1, $2);',array($id_uzytkownika,$id_schematu));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa schemat o podanym ID jako moderator
     * @param $id_uzytkownika
     * @param $id_schematu
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function zmienSchemat($id_uzytkownika,$id_schematu,$nowy_opis,$widocznosc){
        $polaczenie=DBConnector::connect();

        if($widocznosc){
            $widocznosc="T";
        }else{
            $widocznosc="F";
        }

        pg_send_query_params($polaczenie,'select zmien_schemat($1, $2, $3, $4);',array($id_uzytkownika,$id_schematu,$nowy_opis,$widocznosc));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Dodaje część o podanym ID do schematu
     * @param $id_uzytkownika
     * @param $id_schematu
     * @param $id_czesci
     * @param $ilosc_wymaganych_elementow
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function dodajCzescDoSchematu($id_uzytkownika,$id_schematu,$id_czesci,$ilosc_wymaganych_elementow){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_do_schematu($1, $2, $3, $4);',array($id_uzytkownika,$id_schematu,$id_czesci,$ilosc_wymaganych_elementow));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Usuwa część o podanym ID z schematu
     * @param $id_uzytkownika
     * @param $id_schematu
     * @param $id_czesci
     * @throws Exception - błędy pochodzące z BD (kod 202)
     */
    public static function usunCzescZSchematu($id_uzytkownika,$id_schematu,$id_czesci){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_czesc_z_schematu($1, $2, $3);',array($id_uzytkownika,$id_schematu,$id_czesci));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Pobiera czesci nalezace do schematu
     * @param $id_schematu
     * @return array - lista z wierszami o kluczach (nazwa_elementu,identyfikator_czesci,wymagana_ilosc)
     * @throws Exception
     */
    public static function pobierzCzesciSchematuById($id_schematu){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenia,"SELECT nazwa_elementu,identyfikator_czesci,wymagana_ilosc FROM view_schematy_czesci WHERE identyfikator_schematu=$1;",array($id_schematu));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $lista_czesci=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $lista_czesci[]=array("nazwa_elementu"=> $row[0], "identyfikator_czesci" => $row[1],"wymagana_ilosc" => $row[2]);
        }

        DBConnector::closeConnection();
        return $lista_czesci;
    }

}

