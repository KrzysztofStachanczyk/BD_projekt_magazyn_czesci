<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 22:13
 *
 * Nu Html Checker - OK
 */

require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "KategorieService.php";
require_once "DashBoardRendererService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];

if (!in_array("EDYCJA_KATEGORI", $uprawnienia_uzytkownika) ) {
    header("Location: index.php");
    die();
}


$kategorie_raport = null;
try {
    $kategorie_raport = KategorieService::pobierzKategorie();
} catch (Exception $e) {
    die();
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <title>Kategorie</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header"><span class="glyphicon glyphicon-inbox"></span> Zarządzanie kategoriami</h1>
            <p>Tutaj możesz dodawać nowe kategorie i zmieniać ich nazwy dla sprzętu elektronicznego.</p>

            <div id="kategorie_chart" style="width: 100%; height: 500px;"></div>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Kategorie</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa kategorii</th>
                        <th>Ilość części</th>
                        <th>Nowa nazwa</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($kategorie_raport as $kategoria) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $kategoria['nazwa_kategori']; ?>
                        </td>
                        <td>
                            <?php echo $kategoria['ilosc_czesci']; ?>

                        </td>
                        <td>
                            <form class="rename_category">
                                <input type="hidden" name="nazwa_kategorii"  value="<?php
                                echo $kategoria['nazwa_kategori'];
                                ?>">


                                <div class="input-group">
                                    <input class="form-control" placeholder="Nazwa kategorii" name="nowa_nazwa_kategorii" required>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-check" aria-hidden="true"> </span>
                                        </button>
                                    </div>
                                </div>
                            </form>


                        </td>
                        <td>
                            <form class="remove_category">
                                <input type="hidden" name="nazwa_kategorii"  value="<?php
                                    echo $kategoria['nazwa_kategori'];
                                ?>">

                                <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>
                        </td>

                        <?php


                        }
                        ?>
                    </tbody>
                </table>

                <h2 class="sub-header"><span class="glyphicon glyphicon-plus"></span> Dodaj kategorie</h2>
                <form id="add_category">

                    <div class="input-group">
                        <input class="form-control" placeholder="Nazwa kategorii" name="nazwa_kategorii" required>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<script>
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Kategoria', 'Ilość części'],
            <?php
            foreach ($kategorie_raport as  $kategoria){
                echo "['".$kategoria['nazwa_kategori']."',".$kategoria['ilosc_czesci']."],";
            }
            ?>
        ]);

        var options = {
            title: '',
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('kategorie_chart'));
        chart.draw(data, options);
    }


    $(document).ready(function () {

        $("#categoryEditLink").addClass("active");
        $("#add_category").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_kategorie.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        toastr.error(json['komunikat_o_bledzie'].replace("ERROR:", ""), "Błąd dodawania kategorii");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania kategorii");
                }

            })
        });


        $(".remove_category").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_kategorie.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania kategorii");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania kategorii");
                }

            })
        });

        $(".rename_category").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/zmien_nazwe_kategori.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd zmiany nazwy");
                    }
                },
                error: function () {
                    toastr.error("Błąd zmiany nazwy");
                }

            })
        });
    });


</script>
</body>
</html>