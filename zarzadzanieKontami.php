<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 19:54
 *
 * Nu Html Checker OK
 */


require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "DashBoardRendererService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];

if (!in_array("USUWANIE_UZYTKOWNIKOW", $uprawnienia_uzytkownika) and !in_array("PRZYPISYWANIE_DO_UPRAWNIEN", $uprawnienia_uzytkownika)) {
    header("Location: index.php");
    die();
}

$uzytkownicy_grupy = null;
$wszystkie_grupy = null;
try {
    $wszystkie_grupy = GrupyUprawnienService::pobierzListeGrup();
    $uzytkownicy_grupy = GrupyUprawnienService::pobierzGrupyWszystkichUzytkownikow();
} catch (Exception $e) {
    die();
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Zarządzanie użytkownikami</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header"><span class="glyphicon glyphicon-cog" ></span> Zarządzanie użytkownikami</h1>
            <p>W poniższej tabeli możesz modyfikować konta użytkowników. <strong> UWAGA !!! </strong> pewnych zmian nie
                da się cofnąć.</p>

            <h2 class="sub-header"><span class="glyphicon glyphicon-user"></span> Użytkownicy</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa użytkownika</th>
                        <th>Grupa</th>
                        <?php
                        if (in_array("USUWANIE_UZYTKOWNIKOW", $uprawnienia_uzytkownika)) {
                            echo "<th></th>";
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($uzytkownicy_grupy as $przypisanie_do_grupy) {
                        echo "<tr><td>" . $przypisanie_do_grupy['login'] . "</td>";
                        if (!in_array("PRZYPISYWANIE_DO_UPRAWNIEN", $uprawnienia_uzytkownika)) {
                            echo "<td>" . $przypisanie_do_grupy['nazwa_grupy'] . "</td>";
                        } else {
                            ?>
                            <td>
                                <form method="post">
                                    <input type="hidden" name="login" value="<?php echo $przypisanie_do_grupy['login']; ?>">

                                    <select name="nazwa_grupy" class="change_group_combo">
                                        <?php
                                            foreach ($wszystkie_grupy as $grupa){
                                                echo '<option value="'.$grupa['nazwa_grupy'].'"';
                                                if($grupa['nazwa_grupy']==$przypisanie_do_grupy['nazwa_grupy']){
                                                    echo ' selected="selected"';
                                                }
                                                echo '>'.$grupa['nazwa_grupy'].'</option>';
                                            }
                                        ?>

                                    </select>

                                </form>
                            </td>
                            <?php
                        }

                        if (in_array("USUWANIE_UZYTKOWNIKOW", $uprawnienia_uzytkownika)) {

                            ?>
                            <td>
                                <form method="post" class="remove_user">
                                    <input type="hidden" name="user_login" value="<?php echo $przypisanie_do_grupy['login']; ?>">
                                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>
                            <?php
                        }
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#usersEditLink").addClass("active");
       $(".remove_user").submit(function (e) {
          e.preventDefault();

          $.ajax({
              method: "POST",
              url: "api/usun_uzytkownika.php",
              data: $(this).serialize(),
              success: function (json) {
                  if (json['status']) {
                      location.reload();
                  }
                  else {
                      var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                      if(error_string.indexOf("CONTEXT")>0){
                          error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                      }
                      toastr.error(error_string, "Błąd usuwania uzytkownika");
                  }
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  toastr.error("Błąd usuwania użytkownika");
              }

          })
       });


        $(".change_group_combo").change(function() {

            $.ajax({
                method: "POST",
                url: "api/zmien_grupe.php",
                data: $(this).parent().serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd zmieniania grupy");
                    }
                },
                error: function () {
                    toastr.error("Błąd zmieniania grupy");
                }

            })
        });
    });


</script>
</body>
</html>
