<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 12:17
 */
require_once "DBConnector.php";

class CzesciService
{
    /**
     * Usuwa cześć o podanej nazwie
     * @param $uzid
     * @param $nazwa_czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunCzesc($uzid,$nazwa_czesci){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_czesc($1,$2);',array($uzid,$nazwa_czesci));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Blokuje część o podanej nazwie ( nie może być dodawana do magazynów )
     * @param $uzid
     * @param $nazwa_czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function zablokujCzesc($uzid,$nazwa_czesci){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select zablokuj_czesc($1,$2);',array($uzid,$nazwa_czesci));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledach="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledach=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledach,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Odblokuje część o podanej nazwie (
     * @param $uzid
     * @param $nazwa_czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function odblokujCzesc($uzid,$nazwa_czesci){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select odblokuj_czesc($1,$2);',array($uzid,$nazwa_czesci));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledach="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledach=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledach,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Dodaje nową część do bazy danych
     * @param $uzid
     * @param $nazwa_czesci
     * @param $opis_czesci
     * @param $kategoria
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function dodajCzesc($uzid,$nazwa_czesci,$opis_czesci,$kategoria){
        $polaczenie=DBConnector::connect();

        if($opis_czesci==null){
            $opis_czesci="";
        }

        pg_send_query_params($polaczenie,'select dodaj_czesz($1,$2,$3,$4);',array($uzid,$nazwa_czesci,$opis_czesci,$kategoria));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();

    }


    /**
     * Zwraca ilość części spełniających kryteria
     * @param $nazwa_kategorii  - nazwa kategorii, jeśli null uwzględniane są wszystkie
     * @return int - ilosc czesci
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function pobierzIloscCzesci($nazwa_kategorii){
        $polaczenie=DBConnector::connect();

        $zapytanie="select count(nazwa_elementu) from view_czesci_kategorie_raport";
        $parametry_zapytania=array();

        if ($nazwa_kategorii!=null){
            $parametry_zapytania[]=$nazwa_kategorii;
            $zapytanie=$zapytanie." WHERE nazwa_kategori=$1 ";
        }

        $zapytanie=$zapytanie.";";

        $wynik_zapytania=pg_query_params($polaczenie,$zapytanie,$parametry_zapytania);


        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $ilosc_czesci=0;

        while ($row=pg_fetch_row($wynik_zapytania)){
            $ilosc_czesci=(int) $row[0];
        }

        DBConnector::closeConnection();
        return $ilosc_czesci;
    }

    /**
     * Pobiera szczegółowe dane o cześci
     * @param $nazwa_czesci - nazwa części do odszukania
     * @return array|null - tablica zawierajaca klucze (id_czesci,nazwa_elementu,nazwa_kategorii,ilosz_magazynow,zablokowana)
     * @throws Exception - błąd komunikacji z bd (kod 200) i część nie istnieje (KOD 202)
     */
    public static function pobierzCzesc($nazwa_czesci){
        $polaczenia=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenia,"select identyfikator_czesci,nazwa_elementu,nazwa_kategori,opis ,zablokowana from view_czesci_szczegoly WHERE nazwa_elementu=$1;",array($nazwa_czesci));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $czesc=null;

        if ($row=pg_fetch_row($wynik_zapytania)){
            $czesc=array(
                'id_czesci' => $row[0],
                'nazwa_elementu' => $row[1],
                'nazwa_kategori' => $row[2],
                'opis' => $row[3],
                'zablokowana' => $row[4]);
        }

        DBConnector::closeConnection();

        if($czesc==null){
            throw new Exception("Część nie istnieje",202);
        }
        return $czesc;

    }

    /**
     * Pobiera czesci spelniajace kryteria posortowane po nazwie
     * @param $nazwa_kategorii - nazwa kategorii, jeśli null uwzględniane są wszystkie
     * @param $offset - indeks pierwszego zwracanego wiersza (przydatne do paginacji)
     * @param $limit - maksymalna ilosc zwracanych wierszy
     * @return array - każdy wiersz jest tablica zawierajaca klucze (nazwa_elementu,nazwa_kategorii,ilosz_magazynow,zablokowana)
     * @throws Exception - błąd komunikacji z bazą danych (KOD 200)
     */
    public static function pobierzCzesci($nazwa_kategorii, $offset, $limit){
        $polaczenie=DBConnector::connect();

        $zapytanie="select  identyfikator_czesci,nazwa_elementu,nazwa_kategori,ilosz_magazynow,zablokowana  from view_czesci_kategorie_raport ";
        $indeks_parametru=1;
        $parametry=array();

        if ($nazwa_kategorii!=null){
            $parametry[]=$nazwa_kategorii;
            $zapytanie=$zapytanie."WHERE nazwa_kategori=$".$indeks_parametru." ";
            $indeks_parametru=$indeks_parametru+1;
        }

        $zapytanie=$zapytanie." ORDER BY nazwa_elementu ";

        if($limit!=null){
            $parametry[]=$limit;
            $zapytanie=$zapytanie." LIMIT $".$indeks_parametru." ";
            $indeks_parametru++;
        }
        if($offset!=null){
            $parametry[]=$offset;
            $zapytanie=$zapytanie." OFFSET $".$indeks_parametru;
        }

        $zapytanie=$zapytanie.";";

        $wynik_zapytania=pg_query_params($polaczenie,$zapytanie,$parametry);

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $czesci=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $czesci[]=array(
                "identyfikator_czesci" => $row[0],
                "nazwa_elementu" => $row[1],
                "nazwa_kategorii" => $row[2],
                "ilosz_magazynow" => $row[3],
                "zablokowana" => $row[4]);
        }

        DBConnector::closeConnection();
        return $czesci;
    }
}