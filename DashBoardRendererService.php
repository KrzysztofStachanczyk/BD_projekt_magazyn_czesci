<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 29.12.16
 * Time: 14:37
 */
require_once "UprawnieniaService.php";

class DashBoardRendererService
{
    public static function renderDashBoardHeader()
    {
        ?>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css"/>

        <script
                src="https://code.jquery.com/jquery-3.1.1.min.js"
                integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
                crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
              crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous"></script>


        <link href="dashboard_styles.css" rel="stylesheet">

        <?php
    }

    public static function renderDashBoardNavBar()
    {
        ?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid" style="background-color: #428bca;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand navbar-link" href="index.php" >Magazyn części</a>

                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="api/wyloguj.php" class="navbar-link">Wyloguj</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php

    }

    public static function renderDashBoardAside()
    {
        $uprawnienia = $_SESSION['user_uprawnienia'];
        ?>
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            if (in_array("EDYCJA_GRUP_UPRAWNIEN", $uprawnienia) or in_array("PRZYPISYWANIE_DO_UPRAWNIEN", $uprawnienia) or in_array("USUWANIE_UZYTKOWNIKOW", $uprawnienia)) {
                ?>
                <ul class="nav nav-sidebar">
                    <?php
                    if (in_array("EDYCJA_GRUP_UPRAWNIEN", $uprawnienia)) {
                        ?>
                        <li id="groupEditLink"><a href="edycjaGrup.php">Edycja grup uprawnień</a></li> <?php
                    }
                    if (in_array("USUWANIE_UZYTKOWNIKOW", $uprawnienia) or in_array("PRZYPISYWANIE_DO_UPRAWNIEN", $uprawnienia)) {
                        ?>
                        <li id="usersEditLink"><a href="zarzadzanieKontami.php">Zarządzanie użytkownikami</a></li>
                        <?php
                    }
                    ?>
                </ul>
                <hr/>
                <?php
            }

            if (in_array("WERYFIKACJA_KOMENTARZY", $uprawnienia) or in_array("EDYCJA_KATEGORI", $uprawnienia) or in_array("DODAWANIE_CZESCI", $uprawnienia) or in_array("USUWANIE_CZESCI", $uprawnienia) or in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia) or in_array("MODYFIKACJA_CZESCI", $uprawnienia)) {
                ?>
                <ul class="nav nav-sidebar">

                    <?php
                    if (in_array("EDYCJA_KATEGORI", $uprawnienia)) {
                        ?>
                        <li id="categoryEditLink"><a href="kategorie.php">Kategorie</a></li>
                        <?php
                    }

                    if (in_array("DODAWANIE_CZESCI", $uprawnienia) or in_array("USUWANIE_CZESCI", $uprawnienia) or in_array("PRZYPISYWANIE_CZESCI_DO_MAGAZYNU", $uprawnienia) or in_array("MODYFIKACJA_CZESCI", $uprawnienia)) {
                        ?>
                        <li id="partsEditLink"><a href="zarzadzanieCzesciami.php">Części</a></li>
                        <?php
                    }

                    if (in_array("WERYFIKACJA_KOMENTARZY", $uprawnienia)){?>
                        <li id="commentsCheckLink"><a href="weryfikacjaKomentarzy.php">Weryfikacja komentarzy</a></li>
                        <?php
                        }
                        ?>
                </ul>

                <?php
            }
            ?>
            <hr />
            <ul class="nav nav-sidebar">
                <li id="magazynLink"><a href="magazyn.php">Magazyn</a></li>
                <li id="schematLink"><a href="schematy.php">Schematy</a></li>

            </ul>
        </div>
        <?php
    }
}

?>
