<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 20:05
 *
 * Nu Html Checker - OK
 */


require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "KategorieService.php";
require_once "DashBoardRendererService.php";
require_once "CzesciService.php";
require_once "DokumentacjeService.php";
require_once "CzesciMagazynService.php";
require_once "NotyService.php";
require_once "TagiService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

$uprawnienia_uzytkownika = $_SESSION['user_uprawnienia'];


if (!isset($_GET['selected_part'])) {
    die();
}

$opis_czesci = null;
$dokumentacje = null;
$noty = null;
$ilosc_czesci_w_magazynie = null;
$lista_tagow = null;
$id_czesci = null;

try {
    $opis_czesci = CzesciService::pobierzCzesc($_GET['selected_part']);
    $id_czesci = $opis_czesci['id_czesci'];

    $noty = NotyService::pobierzNotyByID($id_czesci);
    $dokumentacje = DokumentacjeService::pobierzDokumentacjeByID($id_czesci);
    $ilosc_czesci_w_magazynie = CzesciMagazynService::pobierzIloscCzesciWMagazynie($_SESSION['user_id'], $id_czesci);

    if ($ilosc_czesci_w_magazynie != null) {
        $lista_tagow = TagiService::pobierzTagi($_SESSION['user_id'], $id_czesci);
    }

} catch (Exception $e) {
    die();
}

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cześć elektroniczna - szczegóły</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"><span class="glyphicon glyphicon-zoom-in"></span> Szczegóły elementu</h1>
            <p><strong>Nazwa: </strong><?php echo $opis_czesci['nazwa_elementu']; ?></p>
            <p><strong> Kategoria: </strong> <?php echo $opis_czesci['nazwa_kategori']; ?></p>
            <p>
                <strong>
                    Opis:
                </strong>
                <?php
                echo $opis_czesci['opis'];
                ?>

            </p>
            <p><strong>Status:</strong><?php
                if ($opis_czesci['zablokowana'] == "t") {
                    echo "zablokowana";
                } else {
                    echo "dostępna";
                }
                ?></p>


            <h2 class="sub-header"><span class="glyphicon glyphicon-book" ></span> Dokumentacje</h2>
            <?php if (sizeof($dokumentacje) == 0) {
                echo "Brak dokumentacji";
            } else {
                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nazwa</th>
                        <?php
                        if (in_array("MODYFIKACJA_DOKUMENTACJI", $uprawnienia_uzytkownika)) {
                            echo "<th> Usuń </th>";
                        }
                        ?>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($dokumentacje as $indeks => $dokumentacja) {
                        echo "<tr><td>" . ($indeks + 1) . "</td><td><a href='" . $dokumentacja['link'] . "' target='blank'>" . $dokumentacja['nazwa_dokumentacji'] . "</a></td>";
                        if (in_array("MODYFIKACJA_DOKUMENTACJI", $uprawnienia_uzytkownika)) {
                            ?>
                            <td>
                                <form class="remove_doc">
                                    <input type="hidden" name="id_dokumentacji" value="<?php
                                    echo $dokumentacja['id_dokumentacji'];
                                    ?>">

                                    <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>
                            <?php
                        }
                        echo "</tr>";
                    }
                    ?>

                    </tbody>
                </table>

            <?php }
            if (in_array("MODYFIKACJA_DOKUMENTACJI", $uprawnienia_uzytkownika)) { ?>
                <form class="form-inline" id="add_doc">
                    <input type="hidden" value="<?php echo $opis_czesci['nazwa_elementu']; ?>" name="nazwa_czesci">
                    <div class="form-group">
                        <label for="doc_name">Nazwa:</label>
                        <input type="text" name="nazwa_dokumentacji" class="form-control" id="doc_name" required>
                    </div>
                    <div class="form-group">
                        <label for="doc_link">Link:</label>
                        <input type="text" name="link" class="form-control" id="doc_link" required>
                    </div>

                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </form>
                <?php
            }
            ?>

            <h2 class="sub-header"><span class="glyphicon glyphicon-bookmark"></span> Noty aplikacyjne</h2>


            <?php if (sizeof($noty) == 0) {
                echo "Brak not aplikacyjnych";
            } else {
                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nazwa</th>
                        <?php
                        if (in_array("MODYFIKACJA_NOTY", $uprawnienia_uzytkownika)) {
                            echo "<th> Usuń </th>";
                        }
                        ?>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($noty as $indeks => $nota) {
                        echo "<tr><td>" . ($indeks + 1) . "</td><td><a href='" . $nota['link'] . "' target='blank'>" . $nota['nazwa_noty'] . "</a></td>";
                        if (in_array("MODYFIKACJA_NOTY", $uprawnienia_uzytkownika)) {
                            ?>
                            <td>
                                <form class="remove_note">
                                    <input type="hidden" name="id_noty" value="<?php
                                    echo $nota['id_noty'];
                                    ?>">

                                    <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>
                            <?php
                        }
                        echo "</tr>";
                    }
                    ?>

                    </tbody>
                </table>


                <?php
            }
            if (in_array("MODYFIKACJA_NOTY", $uprawnienia_uzytkownika)) {
                ?>
                <form class="form-inline" id="add_note">
                    <input type="hidden" value="<?php echo $opis_czesci['nazwa_elementu']; ?>" name="nazwa_czesci">
                    <div class="form-group">
                        <label for="note_name">Nazwa:</label>
                        <input type="text" name="nazwa_noty" class="form-control" id="note_name" required>
                    </div>
                    <div class="form-group">
                        <label for="note_link">Link:</label>
                        <input type="text" name="link" class="form-control" id="note_link" required>
                    </div>

                    <button type="submit" class="btn btn-default" aria-label="Left Align">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </form>
                <?php
            }
            ?>


            <?php if ($ilosc_czesci_w_magazynie != null) { ?>
                <h2 class="sub-header"><span class="glyphicon glyphicon-tags"></span> Twoje tagi:</h2>
                <?php if (sizeof($lista_tagow) == 0) {
                    echo "Brak tagów";
                } else {
                    ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                Nazwa
                            </th>

                            <th>
                                Usuń
                            </th>
                        </tr>

                        </thead>
                        <tbody>
                        <?php
                        foreach ($lista_tagow as $tag) {
                            ?>
                            <tr>
                                <?php
                                echo "<td>" . $tag[1] . "</td>";
                                ?>
                                <td>
                                    <form class="remove_tag">
                                        <input type="hidden" name="id_czesci" value="<?php echo $tag[0]; ?>">
                                        <input type="hidden" name="nazwa_tagu" value="<?php echo $tag[1]; ?>">

                                        <button type="submit" class="btn btn-default" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>

                                    </form>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>

                        </tbody>

                    </table>

                    <?php } ?>
                    <form id="add-tag">
                        <input type="hidden" name="id_czesci" value="<?php echo $id_czesci; ?>">

                        <div class="input-group">
                            <input class="form-control" placeholder="Nazwa tagu" name="nazwa_tagu" required>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"> Dodaj </span>
                                </button>
                            </div>
                        </div>

                    </form>

                    <?php
            }
            ?>


        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#partsEditLink").addClass("active");
        $("#add-tag").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_tag.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania tag-u");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania tagu");
                }

            })
        });

        $(".remove_tag").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_tag.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania tag-u");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania tagu");
                }

            })
        });


        $(".remove_doc").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_dokumentacje.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania dokumentacji");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania dokumentacji");
                }

            })
        });

        $(".remove_note").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_note.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania noty aplikacyjnej");
                    }
                },
                error: function () {
                    toastr.error("Błąd usuwania noty aplikacyjnej");
                }

            })
        });

        $("#add_doc").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_dokumentacje.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania dokumentacji");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania dokumentacji");
                }

            })
        });

        $("#add_note").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_note.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania noty aplikacyjnej");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania noty aplikacyjnej");
                }

            })
        });
    });


</script>
</body>
</html>