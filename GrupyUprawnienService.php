<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 21:10
 */

class GrupyUprawnienService{
    /**
     * Przypisuje użytkownika o podanym loginie do grupy
     * @param $uzid
     * @param $login
     * @param $nazwa_grupy
     * @throws Exception - błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function przypiszUzytkownikaDoGrupy($uzid, $login, $nazwa_grupy){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select przypisz_uzytkownika_do_grupy($1,$2,$3);',array($uzid,$login,$nazwa_grupy));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";

            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Dodaje grupe uprawnien do bazy danych
     * @param $uzid
     * @param $nazwa_grupy
     * @throws Exception -błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function dodajGrupeUprawnien($uzid, $nazwa_grupy){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_grupe_uprawnien($1,$2);',array($uzid,$nazwa_grupy));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa wskazaną grupę uprawnień z bazy danych
     * @param $uzid
     * @param $nazwa_grupy
     * @throws Exception -błędy wykonywania operacji (kod 202) - szczegółowy komunikat pochodzi z bazy danych
     */
    public static function usunGrupeUprawnien($uzid, $nazwa_grupy){
        $poloczenie=DBConnector::connect();

        pg_send_query_params($poloczenie,'select usun_grupe_uprawnien($1,$2);',array($uzid,$nazwa_grupy));
        $wynik_zapytania=pg_get_result($poloczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }


    /**
     * Pobiera listę grup znajdujących się w systemie i ilość użytkowników do nich przypisanych
     * @return array - lista grup  - każdy wiersz posiada 2 klucze('nazwa_grupy','ilosc_uzytkownikow')
     * @throws Exception
     */
    public static function pobierzListeGrup(){
        $polaczenie=DBConnector::connect();

        $wynik_zapytania=pg_query($polaczenie,"select nazwa_grupy, ilosc_uzytkownikow from view_grupy_raport;");

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        $lista_grup=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $lista_grup[]=array("nazwa_grupy"=>$row[0],"ilosc_uzytkownikow"=>$row[1]);
        }

        return $lista_grup;
    }

    /**
     * Pobiera listę użytkowników z przypisanymi do nich grupami
     * @return array -  każdy wiersz zawiera klucze('login','nazwa_grupy')
     * @throws Exception
     */
    public static function pobierzGrupyWszystkichUzytkownikow(){
        $polaczenie=DBConnector::connect();
        $grupy=array();

        $wynik_zapytania=pg_query($polaczenie,"SELECT login,nazwa_grupy FROM view_uzytkownicy_grupy");

        if($wynik_zapytania){
            while($row = pg_fetch_row($wynik_zapytania)){
                $grupy[]=array("login" => $row[0],"nazwa_grupy" => $row[1]);
            }
        }
        else{
            DBConnector::closeConnection();
            throw new Exception("Błąd pobierania danych",200);
        }

        DBConnector::closeConnection();
        return $grupy;
    }
}

?>