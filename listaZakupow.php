<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 02.01.17
 * Time: 22:38
 *
 * Nu Html Checker - OK
 */

require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once "GrupyUprawnienService.php";
require_once "KategorieService.php";
require_once "DashBoardRendererService.php";
require_once "ListyZakupowService.php";
require_once "ListyZakupowDefinicjeService.php";
require_once "CzesciMagazynService.php";

// check if have permission to be here
if (!UzytkownicyService::czyZalogowany()) {
    header("Location: login.php");
    die();
}

if (!isset($_GET['nazwa_listy'])) {
    die();
}

$lista_zakupow = null;
$elementy_listy_zakupow = null;
$czesci_uzytkownika=null;
try {
    $lista_zakupow = ListyZakupowService::pobierzListeZakupow($_SESSION['user_id'], $_GET['nazwa_listy']);
    $czesci_uzytkownika=CzesciMagazynService::pobierzListeCzesciUzytkownika($_SESSION['user_id']);
    if($lista_zakupow==null){
        die();
    }
    $elementy_listy_zakupow=ListyZakupowDefinicjeService::pobierzElementyListyZakupow($_SESSION['user_id'],$lista_zakupow['identyfikator_listy_zakupow']);
} catch (Exception $e) {
    die();
}



?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lista zakupów</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header"><span class="glyphicon glyphicon-shopping-cart"></span> Szczegóły listy zakupów</h1>
            <ul>
                <li>
                    <strong> Nazwa: </strong><?php echo $lista_zakupow["nazwa_listy_zakupow"]; ?>
                </li>
                <li>
                    <strong> Czas utworzenia: </strong> <?php echo $lista_zakupow["czas_utworzenia"]; ?>
                </li>

                <li>
                    <strong> Termin realizacji: </strong>

                    <?php if ($lista_zakupow['termin_realizacji'] == null) {
                        echo "niezdefiniowany";
                    } else {
                        echo $lista_zakupow['termin_realizacji'];
                    } ?>
                </li>
            </ul>

            <h2 class="sub-header"><span class="glyphicon glyphicon-list"></span> Elementy listy zakupów</h2>
            <?php if (sizeof($elementy_listy_zakupow) == 0) {
                echo "Lista zakupów jest pusta";
            } else { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th> Nazwa elementu</th>
                        <th> Ilość w magazynie</th>
                        <th> Liczba do zamówienia</th>
                        <th> Koszt</th>
                        <th> Czy koszt jednostkowy</th>
                        <th> Usuń z listy </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($elementy_listy_zakupow as $elementListyZakupow) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $elementListyZakupow["nazwa_elementu"]; ?>
                        </td>
                        <td>
                            <?php echo $elementListyZakupow["ilosc_sztuk_w_magazynie"]; ?>

                        </td>


                        <td colspan="3">

                            <form class="change_on_list">
                                <input type="hidden" name="list_id" value="<?php echo $lista_zakupow['identyfikator_listy_zakupow'];?>" >
                                <input  type="hidden" name="id_czesci" value="<?php echo $elementListyZakupow['identyfikator_czesci'];?>" >


                                <input type="hidden" name="ilosc_old" value="<?php echo $elementListyZakupow['ilosc_do_zamowienia'];?>">
                                <input type="hidden" name="koszt_old" value="<?php echo $elementListyZakupow['koszt'];?>">
                                <input type="hidden" name="jednostkowy_old" value="<?php echo $elementListyZakupow['czy_koszt_jednostkowy']=='t';?>">


                                <table style="width: 100%;">

                                    <tr>

                                        <td style="width: 30%;">
                                            <input title="Ilość" type="number" name="ilosc" value="<?php echo $elementListyZakupow['ilosc_do_zamowienia'];?>" required>
                                        </td>
                                        <td style="width: 30%;">
                                            <input title="Koszt"  type="number" name="koszt" value="<?php echo $elementListyZakupow['koszt']?>" step="0.01" required>
                                        </td>
                                        <td style="width: 20%;">
                                            <input  title="Jednostkowy" type="checkbox" name="jednostkowy" <?php if($elementListyZakupow['czy_koszt_jednostkowy']=='t'){ echo "checked"; }?>>
                                        </td>
                                        <td style="width: 20%;">
                                            <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>

                        <td>
                            <form class="remove_from_list">
                                <input type="hidden" name="list_id" value="<?php echo $lista_zakupow['identyfikator_listy_zakupow'];?>" >
                                <input  type="hidden" name="id_czesci" value="<?php echo $elementListyZakupow['identyfikator_czesci'];?>" >

                                <button type="submit" class="btn btn-default add_right" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </form>
                        </td>

                        <?php


                        }
                        ?>
                    </tbody>
                </table>

                <?php
            }
            ?>
            <strong>Koszt całkowity: </strong><p id="total-cost"></p>

            <h2 class="sub-header"><span class="glyphicon glyphicon-plus"></span> Dodaj do listy zakupów</h2>

            <form  id="add_to_list">
                <div class="form-group">
                    <label for="id_czesci">Nazwa części:</label>
                    <select class="form-control" id="id_czesci" name="id_czesci" >

                        <?php
                        foreach ($czesci_uzytkownika as $czesc) {
                            echo '<option value="' . $czesc["identyfikator_czesci"] . '"';
                            echo '>' . $czesc["nazwa_elementu"] . '</option>';
                        }
                        ?>
                    </select>

                    <input type="hidden" name="list_id" value="<?php echo $lista_zakupow['identyfikator_listy_zakupow'];?>" >


                    <label for="ilosc">Ilość:</label>
                    <input class="form-control" type="number" name="ilosc" id="ilosc" >

                    <label for="koszt"> Koszt: </label>
                    <input class="form-control" type="number" name="koszt" id="koszt" step="0.01" >

                    <label> Jednostkowy:</label>
                    <input  type="checkbox" name="jednostkowy">

                </div>
                <input type="submit" class="form-control" value="Dodaj">
            </form>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#add_to_list").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/dodaj_do_listy_zakupow.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd dodawania części do listy");
                    }
                },
                error: function () {
                    toastr.error("Błąd dodawania części do listy");
                }

            })
        });


        $(".remove_from_list").submit(function (e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "api/usun_element_z_listy.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        location.reload();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd usuwania kategorii");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.error("Błąd usuwania kategorii");
                }

            })
        });

        $(".change_on_list").submit(function (e) {
            e.preventDefault();

            var thatForm=this;

            $.ajax({
                method: "POST",
                url: "api/zmien_element_listy.php",
                data: $(this).serialize(),
                success: function (json) {
                    if (json['status']) {
                        toastr.success("Pomyślnie zmieniono element listy");
                        $(thatForm).find('input[name="ilosc_old"]').val($(thatForm).find('input[name="ilosc"]').val());
                        $(thatForm).find('input[name="koszt_old"]').val($(thatForm).find('input[name="koszt"]').val());
                        $(thatForm).find('input[name="jednostkowy_old"]').val($(thatForm).find('input[name="jednostkowy"]').prop('checked'));
                        aktualizujKoszt();
                    }
                    else {
                        var error_string=json['komunikat_o_bledzie'].replace("ERROR:", "");
                        if(error_string.indexOf("CONTEXT")>0){
                            error_string=error_string.substring(0,error_string.indexOf("CONTEXT"));
                        }
                        toastr.error(error_string, "Błąd zmiany elementu listy");
                        $(thatForm).find('input[name="ilosc"]').val($(thatForm).find('input[name="ilosc_old"]').val());
                        $(thatForm).find('input[name="koszt"]').val($(thatForm).find('input[name="koszt_old"]').val());
                        $(thatForm).find('input[name="jednostkowy"]').prop('checked',$(thatForm).find('input[name="jednostkowy_old"]').val());
                    }
                },
                error: function () {
                    toastr.error("Błąd zmiany elementu listy");
                }

            })
        });
        aktualizujKoszt();
    });

    function aktualizujKoszt(){
        $.ajax({
            method: "POST",
            url: "api/pobierz_koszt_zakupu.php",
            data: {id_listy_zakupow: "<?php echo $lista_zakupow['identyfikator_listy_zakupow'];?>"},
            success: function (json) {
                $('#total-cost').text(json.koszt);
            }
        });
    }

</script>
</body>
</html>