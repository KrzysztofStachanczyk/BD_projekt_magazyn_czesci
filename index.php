<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 11:04
 */

require_once 'DBConnector.php';
require_once "UprawnieniaService.php";
require_once "UzytkownicyService.php";
require_once  "GrupyUprawnienService.php";
require_once "DashBoardRendererService.php";

// check if have permission to be here
if(!UzytkownicyService::czyZalogowany()){
    header("Location: login.php");
    die();
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Strona Główna</title>

    <?php
    DashBoardRendererService::renderDashBoardHeader();
    ?>

</head>

<body>

<?php
DashBoardRendererService::renderDashBoardNavBar();
?>

<div class="container-fluid">
    <div class="row">
        <?php
        DashBoardRendererService::renderDashBoardAside();
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"><span class="glyphicon glyphicon-home"></span> Strona główna</h1>
            <strong>Witamy</strong> w witrynie "Magazyn części" tutaj możesz zapisywać posiadane przez Ciebie podzespoły tworzyć listy zakupów, a także schematy współdzielone z innymi użytkownikami.


        </div>
    </div>
</div>

</body>
</html>

