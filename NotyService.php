<?php
/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 30.12.16
 * Time: 23:28
 */

require_once "DBConnector.php";

class NotyService
{

    /**
     * Pobiera noty przypisane do danej części
     * @param $id_czesci
     * @return array - każdy wiersz zawiera tablicę z kluczami ('id_noty','nazwa_noty','link')
     * @throws Exception - błąd komunikacji z BD (KOD 200)
     */
    public static function pobierzNotyByID($id_czesci){
        $polaczenie=DBConnector::connect();

        $wynik_zapytania=pg_query_params($polaczenie,"select identyfikator_noty,nazwa_noty,link from noty_aplikacyjne where identyfikator_czesci=$1;",array($id_czesci));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $noty=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $noty[]=array(
                "id_noty" => $row[0],
                "nazwa_noty" => $row[1],
                "link" => $row[2]);
        }

        DBConnector::closeConnection();
        return $noty;
    }


    public static function dodajNote($uzid,$nazwa_czesci,$nazwa_noty,$link)
    {
        $connection = DBConnector::connect();

        if (filter_var($link, FILTER_VALIDATE_URL) === FALSE) {
            throw new Exception("Błędny format linku", 201);
        }

        pg_send_query_params($connection, 'select dodaj_note($1,$2,$3,$4);', array($uzid, $nazwa_czesci, $nazwa_noty, $link));
        $result = pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage = "Nieobsługiwany wyjątek";
            $exceptionMessage=pg_result_error($result);
            if (pg_result_error_field($result, PGSQL_DIAG_SQLSTATE) == "P0001") {
                $exceptionMessage = pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage, 202);
        }
        DBConnector::closeConnection();

    }

    public static function usunNote($uzid,$id_noty)
    {
        $connection = DBConnector::connect();

        pg_send_query_params($connection, 'select usun_note($1,$2);', array($uzid, $id_noty));
        $result = pg_get_result($connection);

        if (pg_result_error($result)) {
            $exceptionMessage = "Nieobsługiwany wyjątek";
            if (pg_result_error_field($result, PGSQL_DIAG_SQLSTATE) == "P0001") {
                $exceptionMessage = pg_result_error($result);
            }

            DBConnector::closeConnection();
            throw new Exception($exceptionMessage, 202);
        }
        DBConnector::closeConnection();
    }
}