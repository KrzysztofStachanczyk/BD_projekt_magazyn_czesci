<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 08.01.17
 * Time: 20:44
 */

require_once "DBConnector.php";
class KomentarzeService
{
    /**
     * Pobiera komentarze przynależne do danego schematu
     * @param $id_schematu
     * @return array - lista wierszy z kluczami (identyfikator_komentarza,login,tresc,zgloszony_do_sprawdzenia)
     * @throws Exception
     */
    public static function pobierzKomentarzeSchematu($id_schematu){
        $polaczenie=DBConnector::connect();
        $wynik_zapytania=pg_query_params($polaczenie,"select identyfikator_komentarza,login,tresc,zgloszony_do_sprawdzenia from view_komentarze_uzytkownicy WHERE identyfikator_schematu = $1 ORDER BY identyfikator_komentarza;",array($id_schematu));

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $komentarze=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $komentarze[]=array("identyfikator_komentarza"=> $row[0],
                "login"=>$row[1],
                "tresc"=>$row[2],
                "zgloszony_do_sprawdzenia"=>$row[3]
            );
        }

        DBConnector::closeConnection();
        return $komentarze;
    }

    /**
     * Pobiera wszystkie komentarze oznaczone do weryfikacji
     * @return array -lista z wierszami o indeksach (identyfikator_komentarza, nazwa_schematu, login, tresc)
     * @throws Exception
     */
    public static function pobierzKomentarzeDoWeryfikacji(){
        $polaczenie=DBConnector::connect();
        $wynik_zapytania=pg_query($polaczenie,"select identyfikator_komentarza, nazwa_schematu, login, tresc from view_schematy_komentarze_uzytkownicy WHERE zgloszony_do_sprawdzenia=TRUE ORDER BY login");

        if(!$wynik_zapytania){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $komentarze=array();

        while ($row=pg_fetch_row($wynik_zapytania)){
            $komentarze[]=array("identyfikator_komentarza"=> $row[0],
                "nazwa_schematu"=>$row[1],
                "login"=>$row[2],
                "tresc"=>$row[3]
            );
        }

        DBConnector::closeConnection();
        return $komentarze;

    }



    /**
     * Dodaje komentarz do schematu
     * @param $id_uzytkownika
     * @param $id_schematu
     * @param $tresc
     * @throws Exception
     */
    public static function dodajKomentarzDoSchematu($id_uzytkownika,$id_schematu,$tresc){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select dodaj_komentarz_do_schematu($1, $2, $3);',array($id_uzytkownika,$id_schematu,$tresc));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Przekazuje podany komentarz do weryfikacji
     * @param $id_uzytkownika
     * @param $id_komentarza
     * @throws Exception
     */
    public static function przekazKomentarzDoWeryfikacji($id_uzytkownika,$id_komentarza){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select przekaz_komentarz_do_weryfikacji($1, $2);',array($id_uzytkownika,$id_komentarza));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Przekazuje podany komentarz do weryfikacji
     * @param $id_uzytkownika
     * @param $id_komentarza
     * @throws Exception
     */
    public static function cofnijKomentarzZWeryfikacji($id_uzytkownika,$id_komentarza){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select cofnij_komentarz_z_weryfikacji($1, $2);',array($id_uzytkownika,$id_komentarza));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa komentarz o zadanym ID
     * @param $id_uzytkownika
     * @param $id_komentarza
     * @throws Exception
     */
    public static function usunKomentarz($id_uzytkownika,$id_komentarza){
        $polaczenie=DBConnector::connect();

        pg_send_query_params($polaczenie,'select usun_komentarz($1, $2);',array($id_uzytkownika,$id_komentarza));
        $wynik_zapytania=pg_get_result($polaczenie);

        if (pg_result_error($wynik_zapytania)) {
            $komunikat_o_bledzie="Nieobsługiwany wyjątek";
            if(pg_result_error_field($wynik_zapytania, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikat_o_bledzie=pg_result_error($wynik_zapytania);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikat_o_bledzie,202);
        }
        DBConnector::closeConnection();
    }
}