<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 02.01.17
 * Time: 13:38
 */

require_once "DBConnector.php";

class TagiService
{

    /**
     * Pobiera wszytkie taki należące do użytkownika
     * @param $id_uzytkownika
     * @return array - lista nazw tagów
     * @throws Exception
     */
    public static function pobierzTagiUzytkownika($id_uzytkownika){
        $polaczenie=DBConnector::connect();
        $resultSet=pg_query_params($polaczenie,"select DISTINCT nazwa_tagu from przypisanie_tagu WHERE  id_uzytkownika=$1  ;",array($id_uzytkownika));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $tagi=array();

        while ($wiersz=pg_fetch_row($resultSet)){
            $tagi[]=$wiersz[0];
        }

        DBConnector::closeConnection();

        return $tagi;
    }

    /**
     * Pobiera tagi opisujące daną część
     * @param $id_uzytkownika
     * @param $id_czesci
     * @return array - wiersz jest tablica o indeksach 0 - identyfikator_czesci , 1 -nazwa_tagu
     * @throws Exception
     */
    public static function pobierzTagi($id_uzytkownika,$id_czesci){
        $polaczenie=DBConnector::connect();
        $resultSet=pg_query_params($polaczenie,"select identyfikator_czesci,nazwa_tagu from przypisanie_tagu WHERE identyfikator_czesci=$2 AND id_uzytkownika=$1;",array($id_uzytkownika,$id_czesci));

        if(!$resultSet){
            DBConnector::closeConnection();
            throw new Exception("Błąd komunikacji z bazą danych",200);
        }

        $tagi=array();

        while ($wiersz=pg_fetch_row($resultSet)){
            $tagi[]=array($wiersz[0],$wiersz[1]);
        }

        DBConnector::closeConnection();

        return $tagi;
    }

    /**
     * Dodaje tag do wskazanej części dla użytkownika o podanym ID
     * @param $id_uzytkownika
     * @param $id_czesci
     * @param $nazwa_tagu
     * @throws Exception - błędy pochodzące z bd (KOD 202)
     */
    public static function dodajTag($id_uzytkownika,$id_czesci,$nazwa_tagu){
        $polaczenie=DBConnector::connect();


        pg_send_query_params($polaczenie,'select dodaj_tag($1,$2,$3);',array($id_uzytkownika,$id_czesci,$nazwa_tagu));
        $wynik=pg_get_result($polaczenie);

        if (pg_result_error($wynik)) {
            $komunikatOBledzie=pg_result_error($wynik);
            if(pg_result_error_field($wynik, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikatOBledzie=pg_result_error($wynik);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikatOBledzie,202);
        }
        DBConnector::closeConnection();
    }

    /**
     * Usuwa tag z części u użytkownika
     * @param $id_uzytkownika
     * @param $id_czesci
     * @param $nazwa_tagu
     * @throws Exception
     */
    public static function usunTag($id_uzytkownika,$id_czesci,$nazwa_tagu){
        $polaczenie=DBConnector::connect();


        pg_send_query_params($polaczenie,'select usun_tag($1,$2,$3);',array($id_uzytkownika,$id_czesci,$nazwa_tagu));
        $wynik=pg_get_result($polaczenie);

        if (pg_result_error($wynik)) {
            $komunikatOBledzie=pg_result_error($wynik);
            if(pg_result_error_field($wynik, PGSQL_DIAG_SQLSTATE)=="P0001"){
                $komunikatOBledzie=pg_result_error($wynik);
            }

            DBConnector::closeConnection();
            throw new Exception($komunikatOBledzie,202);
        }
        DBConnector::closeConnection();
    }


}