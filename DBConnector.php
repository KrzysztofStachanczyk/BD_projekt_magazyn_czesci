<?php

/**
 * Created by PhpStorm.
 * User: krzysztof
 * Date: 28.12.16
 * Time: 11:43
 */
class DBConnector
{
    private static $handler=null;

    public static function connect(){
        self::$handler= pg_connect("host=localhost dbname=projekt user=postgres password=postgres") or die('Błąd połączenia z bazą danych');
        return self::getConnector();
    }

    public static function getConnector(){
        return self::$handler;
    }

    public static function closeConnection(){
        if(self::$handler) {
            pg_close(self::$handler);
            self::$handler = null;
        }
    }
}